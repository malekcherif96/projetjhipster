"use strict";
(self["webpackChunkassurance"] = self["webpackChunkassurance"] || []).push([["common"],{

/***/ 1726:
/*!*******************************************************!*\
  !*** ./src/main/webapp/app/config/input.constants.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DATE_FORMAT": () => (/* binding */ DATE_FORMAT),
/* harmony export */   "DATE_TIME_FORMAT": () => (/* binding */ DATE_TIME_FORMAT)
/* harmony export */ });
const DATE_FORMAT = 'YYYY-MM-DD';
const DATE_TIME_FORMAT = 'YYYY-MM-DDTHH:mm';


/***/ }),

/***/ 4218:
/*!************************************************************!*\
  !*** ./src/main/webapp/app/config/pagination.constants.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ITEMS_PER_PAGE": () => (/* binding */ ITEMS_PER_PAGE),
/* harmony export */   "ASC": () => (/* binding */ ASC),
/* harmony export */   "DESC": () => (/* binding */ DESC),
/* harmony export */   "SORT": () => (/* binding */ SORT)
/* harmony export */ });
const ITEMS_PER_PAGE = 20;
const ASC = 'asc';
const DESC = 'desc';
const SORT = 'sort';


/***/ }),

/***/ 5929:
/*!**********************************************************!*\
  !*** ./src/main/webapp/app/core/request/request-util.ts ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "createRequestOption": () => (/* binding */ createRequestOption)
/* harmony export */ });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ 1841);

const createRequestOption = (req) => {
    let options = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__.HttpParams();
    if (req) {
        Object.keys(req).forEach(key => {
            if (key !== 'sort') {
                options = options.set(key, req[key]);
            }
        });
        if (req.sort) {
            req.sort.forEach((val) => {
                options = options.append('sort', val);
            });
        }
    }
    return options;
};


/***/ }),

/***/ 6037:
/*!****************************************************!*\
  !*** ./src/main/webapp/app/core/util/operators.ts ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "isPresent": () => (/* binding */ isPresent),
/* harmony export */   "filterNaN": () => (/* binding */ filterNaN)
/* harmony export */ });
/*
 * Function used to workaround https://github.com/microsoft/TypeScript/issues/16069
 * es2019 alternative `const filteredArr = myArr.flatMap((x) => x ? x : []);`
 */
function isPresent(t) {
    return t !== undefined && t !== null;
}
const filterNaN = (input) => (isNaN(input) ? 0 : input);


/***/ }),

/***/ 9827:
/*!**************************************************************!*\
  !*** ./src/main/webapp/app/core/util/parse-links.service.ts ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ParseLinks": () => (/* binding */ ParseLinks)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 7716);

/**
 * An utility service for link parsing.
 */
class ParseLinks {
    /**
     * Method to parse the links
     */
    parse(header) {
        if (header.length === 0) {
            throw new Error('input must not be of zero length');
        }
        // Split parts by comma
        const parts = header.split(',');
        const links = {};
        // Parse each part into a named link
        parts.forEach(p => {
            const section = p.split(';');
            if (section.length !== 2) {
                throw new Error('section could not be split on ";"');
            }
            const url = section[0].replace(/<(.*)>/, '$1').trim();
            const queryString = {};
            url.replace(new RegExp('([^?=&]+)(=([^&]*))?', 'g'), ($0, $1, $2, $3) => {
                if ($1 !== undefined) {
                    queryString[$1] = $3;
                }
                return $3 !== null && $3 !== void 0 ? $3 : '';
            });
            if (queryString.page !== undefined) {
                const name = section[1].replace(/rel="(.*)"/, '$1').trim();
                links[name] = parseInt(queryString.page, 10);
            }
        });
        return links;
    }
}
ParseLinks.ɵfac = function ParseLinks_Factory(t) { return new (t || ParseLinks)(); };
ParseLinks.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: ParseLinks, factory: ParseLinks.ɵfac, providedIn: 'root' });


/***/ }),

/***/ 585:
/*!*********************************************************!*\
  !*** ./src/main/webapp/app/entities/role/role.model.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Role": () => (/* binding */ Role),
/* harmony export */   "getRoleIdentifier": () => (/* binding */ getRoleIdentifier)
/* harmony export */ });
class Role {
    constructor(id, name, utilisateurs) {
        this.id = id;
        this.name = name;
        this.utilisateurs = utilisateurs;
    }
}
function getRoleIdentifier(role) {
    return role.id;
}


/***/ }),

/***/ 9885:
/*!*******************************************************************!*\
  !*** ./src/main/webapp/app/entities/role/service/role.service.ts ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RoleService": () => (/* binding */ RoleService)
/* harmony export */ });
/* harmony import */ var app_core_util_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! app/core/util/operators */ 6037);
/* harmony import */ var app_core_request_request_util__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/core/request/request-util */ 5929);
/* harmony import */ var _role_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../role.model */ 585);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ 1841);
/* harmony import */ var app_core_config_application_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/core/config/application-config.service */ 1082);






class RoleService {
    constructor(http, applicationConfigService) {
        this.http = http;
        this.applicationConfigService = applicationConfigService;
        this.resourceUrl = this.applicationConfigService.getEndpointFor('api/roles');
        this.resourceSearchUrl = this.applicationConfigService.getEndpointFor('api/_search/roles');
    }
    create(role) {
        return this.http.post(this.resourceUrl, role, { observe: 'response' });
    }
    update(role) {
        return this.http.put(`${this.resourceUrl}/${(0,_role_model__WEBPACK_IMPORTED_MODULE_2__.getRoleIdentifier)(role)}`, role, { observe: 'response' });
    }
    partialUpdate(role) {
        return this.http.patch(`${this.resourceUrl}/${(0,_role_model__WEBPACK_IMPORTED_MODULE_2__.getRoleIdentifier)(role)}`, role, { observe: 'response' });
    }
    find(id) {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    query(req) {
        const options = (0,app_core_request_request_util__WEBPACK_IMPORTED_MODULE_1__.createRequestOption)(req);
        return this.http.get(this.resourceUrl, { params: options, observe: 'response' });
    }
    delete(id) {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    search(req) {
        const options = (0,app_core_request_request_util__WEBPACK_IMPORTED_MODULE_1__.createRequestOption)(req);
        return this.http.get(this.resourceSearchUrl, { params: options, observe: 'response' });
    }
    addRoleToCollectionIfMissing(roleCollection, ...rolesToCheck) {
        const roles = rolesToCheck.filter(app_core_util_operators__WEBPACK_IMPORTED_MODULE_0__.isPresent);
        if (roles.length > 0) {
            const roleCollectionIdentifiers = roleCollection.map(roleItem => (0,_role_model__WEBPACK_IMPORTED_MODULE_2__.getRoleIdentifier)(roleItem));
            const rolesToAdd = roles.filter(roleItem => {
                const roleIdentifier = (0,_role_model__WEBPACK_IMPORTED_MODULE_2__.getRoleIdentifier)(roleItem);
                if (roleIdentifier == null || roleCollectionIdentifiers.includes(roleIdentifier)) {
                    return false;
                }
                roleCollectionIdentifiers.push(roleIdentifier);
                return true;
            });
            return [...rolesToAdd, ...roleCollection];
        }
        return roleCollection;
    }
}
RoleService.ɵfac = function RoleService_Factory(t) { return new (t || RoleService)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpClient), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](app_core_config_application_config_service__WEBPACK_IMPORTED_MODULE_3__.ApplicationConfigService)); };
RoleService.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjectable"]({ token: RoleService, factory: RoleService.ɵfac, providedIn: 'root' });


/***/ })

}]);
//# sourceMappingURL=common.js.map