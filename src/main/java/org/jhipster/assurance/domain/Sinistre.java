package org.jhipster.assurance.domain;

import java.io.Serializable;
import java.time.LocalDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Sinistre.
 */
@Table("sinistre")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "sinistre")
public class Sinistre implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @Column("type_sinistre")
    private String typeSinistre;

    @Column("date_sinistre")
    private LocalDate dateSinistre;

    @Column("lieu_sinistre")
    private String lieuSinistre;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Sinistre id(Long id) {
        this.id = id;
        return this;
    }

    public String getTypeSinistre() {
        return this.typeSinistre;
    }

    public Sinistre typeSinistre(String typeSinistre) {
        this.typeSinistre = typeSinistre;
        return this;
    }

    public void setTypeSinistre(String typeSinistre) {
        this.typeSinistre = typeSinistre;
    }

    public LocalDate getDateSinistre() {
        return this.dateSinistre;
    }

    public Sinistre dateSinistre(LocalDate dateSinistre) {
        this.dateSinistre = dateSinistre;
        return this;
    }

    public void setDateSinistre(LocalDate dateSinistre) {
        this.dateSinistre = dateSinistre;
    }

    public String getLieuSinistre() {
        return this.lieuSinistre;
    }

    public Sinistre lieuSinistre(String lieuSinistre) {
        this.lieuSinistre = lieuSinistre;
        return this;
    }

    public void setLieuSinistre(String lieuSinistre) {
        this.lieuSinistre = lieuSinistre;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Sinistre)) {
            return false;
        }
        return id != null && id.equals(((Sinistre) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Sinistre{" +
            "id=" + getId() +
            ", typeSinistre='" + getTypeSinistre() + "'" +
            ", dateSinistre='" + getDateSinistre() + "'" +
            ", lieuSinistre='" + getLieuSinistre() + "'" +
            "}";
    }
}
