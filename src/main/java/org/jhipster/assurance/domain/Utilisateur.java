package org.jhipster.assurance.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Utilisateur.
 */
@Table("utilisateur")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "utilisateur")
public class Utilisateur implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @Column("num_contrat")
    private String numContrat;

    @Column("cin")
    private Integer cin;

    @Column("situation_social")
    private String situationSocial;

    @Column("numero_telephone")
    private Integer numeroTelephone;

    @Column("nom")
    private String nom;

    @Column("prenom")
    private String prenom;

    @Column("login")
    private String login;

    @Column("password")
    private String password;

    @JsonIgnoreProperties(value = { "utilisateurs" }, allowSetters = true)
    @Transient
    private Set<Role> roles = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Utilisateur id(Long id) {
        this.id = id;
        return this;
    }

    public String getNumContrat() {
        return this.numContrat;
    }

    public Utilisateur numContrat(String numContrat) {
        this.numContrat = numContrat;
        return this;
    }

    public void setNumContrat(String numContrat) {
        this.numContrat = numContrat;
    }

    public Integer getCin() {
        return this.cin;
    }

    public Utilisateur cin(Integer cin) {
        this.cin = cin;
        return this;
    }

    public void setCin(Integer cin) {
        this.cin = cin;
    }

    public String getSituationSocial() {
        return this.situationSocial;
    }

    public Utilisateur situationSocial(String situationSocial) {
        this.situationSocial = situationSocial;
        return this;
    }

    public void setSituationSocial(String situationSocial) {
        this.situationSocial = situationSocial;
    }

    public Integer getNumeroTelephone() {
        return this.numeroTelephone;
    }

    public Utilisateur numeroTelephone(Integer numeroTelephone) {
        this.numeroTelephone = numeroTelephone;
        return this;
    }

    public void setNumeroTelephone(Integer numeroTelephone) {
        this.numeroTelephone = numeroTelephone;
    }

    public String getNom() {
        return this.nom;
    }

    public Utilisateur nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public Utilisateur prenom(String prenom) {
        this.prenom = prenom;
        return this;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getLogin() {
        return this.login;
    }

    public Utilisateur login(String login) {
        this.login = login;
        return this;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return this.password;
    }

    public Utilisateur password(String password) {
        this.password = password;
        return this;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role> getRoles() {
        return this.roles;
    }

    public Utilisateur roles(Set<Role> roles) {
        this.setRoles(roles);
        return this;
    }

    public Utilisateur addRoles(Role role) {
        this.roles.add(role);
        role.getUtilisateurs().add(this);
        return this;
    }

    public Utilisateur removeRoles(Role role) {
        this.roles.remove(role);
        role.getUtilisateurs().remove(this);
        return this;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Utilisateur)) {
            return false;
        }
        return id != null && id.equals(((Utilisateur) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Utilisateur{" +
            "id=" + getId() +
            ", numContrat='" + getNumContrat() + "'" +
            ", cin=" + getCin() +
            ", situationSocial='" + getSituationSocial() + "'" +
            ", numeroTelephone=" + getNumeroTelephone() +
            ", nom='" + getNom() + "'" +
            ", prenom='" + getPrenom() + "'" +
            ", login='" + getLogin() + "'" +
            ", password='" + getPassword() + "'" +
            "}";
    }
}
