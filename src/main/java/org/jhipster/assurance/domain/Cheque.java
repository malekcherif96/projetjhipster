package org.jhipster.assurance.domain;

import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Cheque.
 */
@Table("cheque")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "cheque")
public class Cheque implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @Column("nom_cheque")
    private String nomCheque;

    @Column("montnant")
    private Double montnant;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Cheque id(Long id) {
        this.id = id;
        return this;
    }

    public String getNomCheque() {
        return this.nomCheque;
    }

    public Cheque nomCheque(String nomCheque) {
        this.nomCheque = nomCheque;
        return this;
    }

    public void setNomCheque(String nomCheque) {
        this.nomCheque = nomCheque;
    }

    public Double getMontnant() {
        return this.montnant;
    }

    public Cheque montnant(Double montnant) {
        this.montnant = montnant;
        return this;
    }

    public void setMontnant(Double montnant) {
        this.montnant = montnant;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Cheque)) {
            return false;
        }
        return id != null && id.equals(((Cheque) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Cheque{" +
            "id=" + getId() +
            ", nomCheque='" + getNomCheque() + "'" +
            ", montnant=" + getMontnant() +
            "}";
    }
}
