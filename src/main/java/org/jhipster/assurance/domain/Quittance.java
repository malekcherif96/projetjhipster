package org.jhipster.assurance.domain;

import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Quittance.
 */
@Table("quittance")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "quittance")
public class Quittance implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @Column("num_quittance")
    private String numQuittance;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Quittance id(Long id) {
        this.id = id;
        return this;
    }

    public String getNumQuittance() {
        return this.numQuittance;
    }

    public Quittance numQuittance(String numQuittance) {
        this.numQuittance = numQuittance;
        return this;
    }

    public void setNumQuittance(String numQuittance) {
        this.numQuittance = numQuittance;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Quittance)) {
            return false;
        }
        return id != null && id.equals(((Quittance) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Quittance{" +
            "id=" + getId() +
            ", numQuittance='" + getNumQuittance() + "'" +
            "}";
    }
}
