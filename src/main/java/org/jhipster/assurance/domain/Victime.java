package org.jhipster.assurance.domain;

import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Victime.
 */
@Table("victime")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "victime")
public class Victime implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @Column("num_contrat")
    private String numContrat;

    @Column("cin")
    private Integer cin;

    @Column("situation_social")
    private String situationSocial;

    @Column("numero_telephone")
    private Integer numeroTelephone;

    @Column("nom")
    private String nom;

    @Column("prenom")
    private String prenom;

    @Column("login")
    private String login;

    @Column("password")
    private String password;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Victime id(Long id) {
        this.id = id;
        return this;
    }

    public String getNumContrat() {
        return this.numContrat;
    }

    public Victime numContrat(String numContrat) {
        this.numContrat = numContrat;
        return this;
    }

    public void setNumContrat(String numContrat) {
        this.numContrat = numContrat;
    }

    public Integer getCin() {
        return this.cin;
    }

    public Victime cin(Integer cin) {
        this.cin = cin;
        return this;
    }

    public void setCin(Integer cin) {
        this.cin = cin;
    }

    public String getSituationSocial() {
        return this.situationSocial;
    }

    public Victime situationSocial(String situationSocial) {
        this.situationSocial = situationSocial;
        return this;
    }

    public void setSituationSocial(String situationSocial) {
        this.situationSocial = situationSocial;
    }

    public Integer getNumeroTelephone() {
        return this.numeroTelephone;
    }

    public Victime numeroTelephone(Integer numeroTelephone) {
        this.numeroTelephone = numeroTelephone;
        return this;
    }

    public void setNumeroTelephone(Integer numeroTelephone) {
        this.numeroTelephone = numeroTelephone;
    }

    public String getNom() {
        return this.nom;
    }

    public Victime nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public Victime prenom(String prenom) {
        this.prenom = prenom;
        return this;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getLogin() {
        return this.login;
    }

    public Victime login(String login) {
        this.login = login;
        return this;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return this.password;
    }

    public Victime password(String password) {
        this.password = password;
        return this;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Victime)) {
            return false;
        }
        return id != null && id.equals(((Victime) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Victime{" +
            "id=" + getId() +
            ", numContrat='" + getNumContrat() + "'" +
            ", cin=" + getCin() +
            ", situationSocial='" + getSituationSocial() + "'" +
            ", numeroTelephone=" + getNumeroTelephone() +
            ", nom='" + getNom() + "'" +
            ", prenom='" + getPrenom() + "'" +
            ", login='" + getLogin() + "'" +
            ", password='" + getPassword() + "'" +
            "}";
    }
}
