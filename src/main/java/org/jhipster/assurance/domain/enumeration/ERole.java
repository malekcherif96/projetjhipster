package org.jhipster.assurance.domain.enumeration;

/**
 * The ERole enumeration.
 */
public enum ERole {
    ASSUREUR,
    ASSUREE,
    ADMINISTRATEUR,
    VICTIME,
}
