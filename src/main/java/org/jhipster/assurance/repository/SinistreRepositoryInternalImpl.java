package org.jhipster.assurance.repository;

import static org.springframework.data.relational.core.query.Criteria.where;
import static org.springframework.data.relational.core.query.Query.query;

import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiFunction;
import org.jhipster.assurance.domain.Sinistre;
import org.jhipster.assurance.repository.rowmapper.SinistreRowMapper;
import org.jhipster.assurance.service.EntityManager;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Select;
import org.springframework.data.relational.core.sql.SelectBuilder.SelectFromAndJoin;
import org.springframework.data.relational.core.sql.Table;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.r2dbc.core.RowsFetchSpec;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive custom repository implementation for the Sinistre entity.
 */
@SuppressWarnings("unused")
class SinistreRepositoryInternalImpl implements SinistreRepositoryInternal {

    private final DatabaseClient db;
    private final R2dbcEntityTemplate r2dbcEntityTemplate;
    private final EntityManager entityManager;

    private final SinistreRowMapper sinistreMapper;

    private static final Table entityTable = Table.aliased("sinistre", EntityManager.ENTITY_ALIAS);

    public SinistreRepositoryInternalImpl(R2dbcEntityTemplate template, EntityManager entityManager, SinistreRowMapper sinistreMapper) {
        this.db = template.getDatabaseClient();
        this.r2dbcEntityTemplate = template;
        this.entityManager = entityManager;
        this.sinistreMapper = sinistreMapper;
    }

    @Override
    public Flux<Sinistre> findAllBy(Pageable pageable) {
        return findAllBy(pageable, null);
    }

    @Override
    public Flux<Sinistre> findAllBy(Pageable pageable, Criteria criteria) {
        return createQuery(pageable, criteria).all();
    }

    RowsFetchSpec<Sinistre> createQuery(Pageable pageable, Criteria criteria) {
        List<Expression> columns = SinistreSqlHelper.getColumns(entityTable, EntityManager.ENTITY_ALIAS);
        SelectFromAndJoin selectFrom = Select.builder().select(columns).from(entityTable);

        String select = entityManager.createSelect(selectFrom, Sinistre.class, pageable, criteria);
        String alias = entityTable.getReferenceName().getReference();
        String selectWhere = Optional
            .ofNullable(criteria)
            .map(
                crit ->
                    new StringBuilder(select)
                        .append(" ")
                        .append("WHERE")
                        .append(" ")
                        .append(alias)
                        .append(".")
                        .append(crit.toString())
                        .toString()
            )
            .orElse(select); // TODO remove once https://github.com/spring-projects/spring-data-jdbc/issues/907 will be fixed
        return db.sql(selectWhere).map(this::process);
    }

    @Override
    public Flux<Sinistre> findAll() {
        return findAllBy(null, null);
    }

    @Override
    public Mono<Sinistre> findById(Long id) {
        return createQuery(null, where("id").is(id)).one();
    }

    private Sinistre process(Row row, RowMetadata metadata) {
        Sinistre entity = sinistreMapper.apply(row, "e");
        return entity;
    }

    @Override
    public <S extends Sinistre> Mono<S> insert(S entity) {
        return entityManager.insert(entity);
    }

    @Override
    public <S extends Sinistre> Mono<S> save(S entity) {
        if (entity.getId() == null) {
            return insert(entity);
        } else {
            return update(entity)
                .map(
                    numberOfUpdates -> {
                        if (numberOfUpdates.intValue() <= 0) {
                            throw new IllegalStateException("Unable to update Sinistre with id = " + entity.getId());
                        }
                        return entity;
                    }
                );
        }
    }

    @Override
    public Mono<Integer> update(Sinistre entity) {
        //fixme is this the proper way?
        return r2dbcEntityTemplate.update(entity).thenReturn(1);
    }
}

class SinistreSqlHelper {

    static List<Expression> getColumns(Table table, String columnPrefix) {
        List<Expression> columns = new ArrayList<>();
        columns.add(Column.aliased("id", table, columnPrefix + "_id"));
        columns.add(Column.aliased("type_sinistre", table, columnPrefix + "_type_sinistre"));
        columns.add(Column.aliased("date_sinistre", table, columnPrefix + "_date_sinistre"));
        columns.add(Column.aliased("lieu_sinistre", table, columnPrefix + "_lieu_sinistre"));

        return columns;
    }
}
