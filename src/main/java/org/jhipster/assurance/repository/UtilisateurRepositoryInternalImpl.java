package org.jhipster.assurance.repository;

import static org.springframework.data.relational.core.query.Criteria.where;
import static org.springframework.data.relational.core.query.Query.query;

import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiFunction;
import org.jhipster.assurance.domain.Role;
import org.jhipster.assurance.domain.Utilisateur;
import org.jhipster.assurance.repository.rowmapper.UtilisateurRowMapper;
import org.jhipster.assurance.service.EntityManager;
import org.jhipster.assurance.service.EntityManager.LinkTable;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Select;
import org.springframework.data.relational.core.sql.SelectBuilder.SelectFromAndJoin;
import org.springframework.data.relational.core.sql.Table;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.r2dbc.core.RowsFetchSpec;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive custom repository implementation for the Utilisateur entity.
 */
@SuppressWarnings("unused")
class UtilisateurRepositoryInternalImpl implements UtilisateurRepositoryInternal {

    private final DatabaseClient db;
    private final R2dbcEntityTemplate r2dbcEntityTemplate;
    private final EntityManager entityManager;

    private final UtilisateurRowMapper utilisateurMapper;

    private static final Table entityTable = Table.aliased("utilisateur", EntityManager.ENTITY_ALIAS);

    private static final EntityManager.LinkTable rolesLink = new LinkTable("rel_utilisateur__roles", "utilisateur_id", "roles_id");

    public UtilisateurRepositoryInternalImpl(
        R2dbcEntityTemplate template,
        EntityManager entityManager,
        UtilisateurRowMapper utilisateurMapper
    ) {
        this.db = template.getDatabaseClient();
        this.r2dbcEntityTemplate = template;
        this.entityManager = entityManager;
        this.utilisateurMapper = utilisateurMapper;
    }

    @Override
    public Flux<Utilisateur> findAllBy(Pageable pageable) {
        return findAllBy(pageable, null);
    }

    @Override
    public Flux<Utilisateur> findAllBy(Pageable pageable, Criteria criteria) {
        return createQuery(pageable, criteria).all();
    }

    RowsFetchSpec<Utilisateur> createQuery(Pageable pageable, Criteria criteria) {
        List<Expression> columns = UtilisateurSqlHelper.getColumns(entityTable, EntityManager.ENTITY_ALIAS);
        SelectFromAndJoin selectFrom = Select.builder().select(columns).from(entityTable);

        String select = entityManager.createSelect(selectFrom, Utilisateur.class, pageable, criteria);
        String alias = entityTable.getReferenceName().getReference();
        String selectWhere = Optional
            .ofNullable(criteria)
            .map(
                crit ->
                    new StringBuilder(select)
                        .append(" ")
                        .append("WHERE")
                        .append(" ")
                        .append(alias)
                        .append(".")
                        .append(crit.toString())
                        .toString()
            )
            .orElse(select); // TODO remove once https://github.com/spring-projects/spring-data-jdbc/issues/907 will be fixed
        return db.sql(selectWhere).map(this::process);
    }

    @Override
    public Flux<Utilisateur> findAll() {
        return findAllBy(null, null);
    }

    @Override
    public Mono<Utilisateur> findById(Long id) {
        return createQuery(null, where("id").is(id)).one();
    }

    @Override
    public Mono<Utilisateur> findOneWithEagerRelationships(Long id) {
        return findById(id);
    }

    @Override
    public Flux<Utilisateur> findAllWithEagerRelationships() {
        return findAll();
    }

    @Override
    public Flux<Utilisateur> findAllWithEagerRelationships(Pageable page) {
        return findAllBy(page);
    }

    private Utilisateur process(Row row, RowMetadata metadata) {
        Utilisateur entity = utilisateurMapper.apply(row, "e");
        return entity;
    }

    @Override
    public <S extends Utilisateur> Mono<S> insert(S entity) {
        return entityManager.insert(entity);
    }

    @Override
    public <S extends Utilisateur> Mono<S> save(S entity) {
        if (entity.getId() == null) {
            return insert(entity).flatMap(savedEntity -> updateRelations(savedEntity));
        } else {
            return update(entity)
                .map(
                    numberOfUpdates -> {
                        if (numberOfUpdates.intValue() <= 0) {
                            throw new IllegalStateException("Unable to update Utilisateur with id = " + entity.getId());
                        }
                        return entity;
                    }
                )
                .then(updateRelations(entity));
        }
    }

    @Override
    public Mono<Integer> update(Utilisateur entity) {
        //fixme is this the proper way?
        return r2dbcEntityTemplate.update(entity).thenReturn(1);
    }

    @Override
    public Mono<Void> deleteById(Long entityId) {
        return deleteRelations(entityId)
            .then(r2dbcEntityTemplate.delete(Utilisateur.class).matching(query(where("id").is(entityId))).all().then());
    }

    protected <S extends Utilisateur> Mono<S> updateRelations(S entity) {
        Mono<Void> result = entityManager.updateLinkTable(rolesLink, entity.getId(), entity.getRoles().stream().map(Role::getId)).then();
        return result.thenReturn(entity);
    }

    protected Mono<Void> deleteRelations(Long entityId) {
        return entityManager.deleteFromLinkTable(rolesLink, entityId);
    }
}

class UtilisateurSqlHelper {

    static List<Expression> getColumns(Table table, String columnPrefix) {
        List<Expression> columns = new ArrayList<>();
        columns.add(Column.aliased("id", table, columnPrefix + "_id"));
        columns.add(Column.aliased("num_contrat", table, columnPrefix + "_num_contrat"));
        columns.add(Column.aliased("cin", table, columnPrefix + "_cin"));
        columns.add(Column.aliased("situation_social", table, columnPrefix + "_situation_social"));
        columns.add(Column.aliased("numero_telephone", table, columnPrefix + "_numero_telephone"));
        columns.add(Column.aliased("nom", table, columnPrefix + "_nom"));
        columns.add(Column.aliased("prenom", table, columnPrefix + "_prenom"));
        columns.add(Column.aliased("login", table, columnPrefix + "_login"));
        columns.add(Column.aliased("password", table, columnPrefix + "_password"));

        return columns;
    }
}
