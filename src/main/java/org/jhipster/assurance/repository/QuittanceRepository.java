package org.jhipster.assurance.repository;

import org.jhipster.assurance.domain.Quittance;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Quittance entity.
 */
@SuppressWarnings("unused")
@Repository
public interface QuittanceRepository extends R2dbcRepository<Quittance, Long>, QuittanceRepositoryInternal {
    // just to avoid having unambigous methods
    @Override
    Flux<Quittance> findAll();

    @Override
    Mono<Quittance> findById(Long id);

    @Override
    <S extends Quittance> Mono<S> save(S entity);
}

interface QuittanceRepositoryInternal {
    <S extends Quittance> Mono<S> insert(S entity);
    <S extends Quittance> Mono<S> save(S entity);
    Mono<Integer> update(Quittance entity);

    Flux<Quittance> findAll();
    Mono<Quittance> findById(Long id);
    Flux<Quittance> findAllBy(Pageable pageable);
    Flux<Quittance> findAllBy(Pageable pageable, Criteria criteria);
}
