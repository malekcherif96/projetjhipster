package org.jhipster.assurance.repository.rowmapper;

import io.r2dbc.spi.Row;
import java.time.LocalDate;
import java.util.function.BiFunction;
import org.jhipster.assurance.domain.Indemnisation;
import org.jhipster.assurance.service.ColumnConverter;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Indemnisation}, with proper type conversions.
 */
@Service
public class IndemnisationRowMapper implements BiFunction<Row, String, Indemnisation> {

    private final ColumnConverter converter;

    public IndemnisationRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Indemnisation} stored in the database.
     */
    @Override
    public Indemnisation apply(Row row, String prefix) {
        Indemnisation entity = new Indemnisation();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setDate(converter.fromRow(row, prefix + "_date", LocalDate.class));
        return entity;
    }
}
