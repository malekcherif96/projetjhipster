package org.jhipster.assurance.repository.rowmapper;

import io.r2dbc.spi.Row;
import java.util.function.BiFunction;
import org.jhipster.assurance.domain.Victime;
import org.jhipster.assurance.service.ColumnConverter;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Victime}, with proper type conversions.
 */
@Service
public class VictimeRowMapper implements BiFunction<Row, String, Victime> {

    private final ColumnConverter converter;

    public VictimeRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Victime} stored in the database.
     */
    @Override
    public Victime apply(Row row, String prefix) {
        Victime entity = new Victime();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setNumContrat(converter.fromRow(row, prefix + "_num_contrat", String.class));
        entity.setCin(converter.fromRow(row, prefix + "_cin", Integer.class));
        entity.setSituationSocial(converter.fromRow(row, prefix + "_situation_social", String.class));
        entity.setNumeroTelephone(converter.fromRow(row, prefix + "_numero_telephone", Integer.class));
        entity.setNom(converter.fromRow(row, prefix + "_nom", String.class));
        entity.setPrenom(converter.fromRow(row, prefix + "_prenom", String.class));
        entity.setLogin(converter.fromRow(row, prefix + "_login", String.class));
        entity.setPassword(converter.fromRow(row, prefix + "_password", String.class));
        return entity;
    }
}
