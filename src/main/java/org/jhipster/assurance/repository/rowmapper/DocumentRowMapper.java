package org.jhipster.assurance.repository.rowmapper;

import io.r2dbc.spi.Row;
import java.util.function.BiFunction;
import org.jhipster.assurance.domain.Document;
import org.jhipster.assurance.service.ColumnConverter;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Document}, with proper type conversions.
 */
@Service
public class DocumentRowMapper implements BiFunction<Row, String, Document> {

    private final ColumnConverter converter;

    public DocumentRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Document} stored in the database.
     */
    @Override
    public Document apply(Row row, String prefix) {
        Document entity = new Document();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setLibelle(converter.fromRow(row, prefix + "_libelle", String.class));
        entity.setType(converter.fromRow(row, prefix + "_type", String.class));
        return entity;
    }
}
