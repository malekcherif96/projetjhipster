package org.jhipster.assurance.repository.rowmapper;

import io.r2dbc.spi.Row;
import java.util.function.BiFunction;
import org.jhipster.assurance.domain.Quittance;
import org.jhipster.assurance.service.ColumnConverter;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Quittance}, with proper type conversions.
 */
@Service
public class QuittanceRowMapper implements BiFunction<Row, String, Quittance> {

    private final ColumnConverter converter;

    public QuittanceRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Quittance} stored in the database.
     */
    @Override
    public Quittance apply(Row row, String prefix) {
        Quittance entity = new Quittance();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setNumQuittance(converter.fromRow(row, prefix + "_num_quittance", String.class));
        return entity;
    }
}
