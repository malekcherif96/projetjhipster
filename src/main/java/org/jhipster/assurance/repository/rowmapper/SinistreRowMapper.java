package org.jhipster.assurance.repository.rowmapper;

import io.r2dbc.spi.Row;
import java.time.LocalDate;
import java.util.function.BiFunction;
import org.jhipster.assurance.domain.Sinistre;
import org.jhipster.assurance.service.ColumnConverter;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Sinistre}, with proper type conversions.
 */
@Service
public class SinistreRowMapper implements BiFunction<Row, String, Sinistre> {

    private final ColumnConverter converter;

    public SinistreRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Sinistre} stored in the database.
     */
    @Override
    public Sinistre apply(Row row, String prefix) {
        Sinistre entity = new Sinistre();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setTypeSinistre(converter.fromRow(row, prefix + "_type_sinistre", String.class));
        entity.setDateSinistre(converter.fromRow(row, prefix + "_date_sinistre", LocalDate.class));
        entity.setLieuSinistre(converter.fromRow(row, prefix + "_lieu_sinistre", String.class));
        return entity;
    }
}
