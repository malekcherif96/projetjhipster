package org.jhipster.assurance.repository.rowmapper;

import io.r2dbc.spi.Row;
import java.util.function.BiFunction;
import org.jhipster.assurance.domain.Cheque;
import org.jhipster.assurance.service.ColumnConverter;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Cheque}, with proper type conversions.
 */
@Service
public class ChequeRowMapper implements BiFunction<Row, String, Cheque> {

    private final ColumnConverter converter;

    public ChequeRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Cheque} stored in the database.
     */
    @Override
    public Cheque apply(Row row, String prefix) {
        Cheque entity = new Cheque();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setNomCheque(converter.fromRow(row, prefix + "_nom_cheque", String.class));
        entity.setMontnant(converter.fromRow(row, prefix + "_montnant", Double.class));
        return entity;
    }
}
