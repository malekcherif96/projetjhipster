package org.jhipster.assurance.repository;

import org.jhipster.assurance.domain.Role;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Role entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RoleRepository extends R2dbcRepository<Role, Long>, RoleRepositoryInternal {
    Flux<Role> findAllBy(Pageable pageable);

    // just to avoid having unambigous methods
    @Override
    Flux<Role> findAll();

    @Override
    Mono<Role> findById(Long id);

    @Override
    <S extends Role> Mono<S> save(S entity);
}

interface RoleRepositoryInternal {
    <S extends Role> Mono<S> insert(S entity);
    <S extends Role> Mono<S> save(S entity);
    Mono<Integer> update(Role entity);

    Flux<Role> findAll();
    Mono<Role> findById(Long id);
    Flux<Role> findAllBy(Pageable pageable);
    Flux<Role> findAllBy(Pageable pageable, Criteria criteria);
}
