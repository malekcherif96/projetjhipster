package org.jhipster.assurance.repository;

import org.jhipster.assurance.domain.Indemnisation;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Indemnisation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IndemnisationRepository extends R2dbcRepository<Indemnisation, Long>, IndemnisationRepositoryInternal {
    // just to avoid having unambigous methods
    @Override
    Flux<Indemnisation> findAll();

    @Override
    Mono<Indemnisation> findById(Long id);

    @Override
    <S extends Indemnisation> Mono<S> save(S entity);
}

interface IndemnisationRepositoryInternal {
    <S extends Indemnisation> Mono<S> insert(S entity);
    <S extends Indemnisation> Mono<S> save(S entity);
    Mono<Integer> update(Indemnisation entity);

    Flux<Indemnisation> findAll();
    Mono<Indemnisation> findById(Long id);
    Flux<Indemnisation> findAllBy(Pageable pageable);
    Flux<Indemnisation> findAllBy(Pageable pageable, Criteria criteria);
}
