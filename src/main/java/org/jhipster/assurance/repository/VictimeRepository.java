package org.jhipster.assurance.repository;

import org.jhipster.assurance.domain.Victime;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Victime entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VictimeRepository extends R2dbcRepository<Victime, Long>, VictimeRepositoryInternal {
    // just to avoid having unambigous methods
    @Override
    Flux<Victime> findAll();

    @Override
    Mono<Victime> findById(Long id);

    @Override
    <S extends Victime> Mono<S> save(S entity);
}

interface VictimeRepositoryInternal {
    <S extends Victime> Mono<S> insert(S entity);
    <S extends Victime> Mono<S> save(S entity);
    Mono<Integer> update(Victime entity);

    Flux<Victime> findAll();
    Mono<Victime> findById(Long id);
    Flux<Victime> findAllBy(Pageable pageable);
    Flux<Victime> findAllBy(Pageable pageable, Criteria criteria);
}
