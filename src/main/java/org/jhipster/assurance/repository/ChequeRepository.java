package org.jhipster.assurance.repository;

import org.jhipster.assurance.domain.Cheque;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Cheque entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChequeRepository extends R2dbcRepository<Cheque, Long>, ChequeRepositoryInternal {
    // just to avoid having unambigous methods
    @Override
    Flux<Cheque> findAll();

    @Override
    Mono<Cheque> findById(Long id);

    @Override
    <S extends Cheque> Mono<S> save(S entity);
}

interface ChequeRepositoryInternal {
    <S extends Cheque> Mono<S> insert(S entity);
    <S extends Cheque> Mono<S> save(S entity);
    Mono<Integer> update(Cheque entity);

    Flux<Cheque> findAll();
    Mono<Cheque> findById(Long id);
    Flux<Cheque> findAllBy(Pageable pageable);
    Flux<Cheque> findAllBy(Pageable pageable, Criteria criteria);
}
