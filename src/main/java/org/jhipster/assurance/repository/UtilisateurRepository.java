package org.jhipster.assurance.repository;

import org.jhipster.assurance.domain.Utilisateur;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Utilisateur entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UtilisateurRepository extends R2dbcRepository<Utilisateur, Long>, UtilisateurRepositoryInternal {
    Flux<Utilisateur> findAllBy(Pageable pageable);

    @Override
    Mono<Utilisateur> findOneWithEagerRelationships(Long id);

    @Override
    Flux<Utilisateur> findAllWithEagerRelationships();

    @Override
    Flux<Utilisateur> findAllWithEagerRelationships(Pageable page);

    @Override
    Mono<Void> deleteById(Long id);

    @Query(
        "SELECT entity.* FROM utilisateur entity JOIN rel_utilisateur__roles joinTable ON entity.id = joinTable.utilisateur_id WHERE joinTable.roles_id = :id"
    )
    Flux<Utilisateur> findByRoles(Long id);

    // just to avoid having unambigous methods
    @Override
    Flux<Utilisateur> findAll();

    @Override
    Mono<Utilisateur> findById(Long id);

    @Override
    <S extends Utilisateur> Mono<S> save(S entity);
}

interface UtilisateurRepositoryInternal {
    <S extends Utilisateur> Mono<S> insert(S entity);
    <S extends Utilisateur> Mono<S> save(S entity);
    Mono<Integer> update(Utilisateur entity);

    Flux<Utilisateur> findAll();
    Mono<Utilisateur> findById(Long id);
    Flux<Utilisateur> findAllBy(Pageable pageable);
    Flux<Utilisateur> findAllBy(Pageable pageable, Criteria criteria);

    Mono<Utilisateur> findOneWithEagerRelationships(Long id);

    Flux<Utilisateur> findAllWithEagerRelationships();

    Flux<Utilisateur> findAllWithEagerRelationships(Pageable page);

    Mono<Void> deleteById(Long id);
}
