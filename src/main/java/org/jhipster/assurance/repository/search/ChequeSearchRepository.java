package org.jhipster.assurance.repository.search;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import org.jhipster.assurance.domain.Cheque;
import org.springframework.data.elasticsearch.core.ReactiveElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.repository.ReactiveElasticsearchRepository;
import reactor.core.publisher.Flux;

/**
 * Spring Data Elasticsearch repository for the {@link Cheque} entity.
 */
public interface ChequeSearchRepository extends ReactiveElasticsearchRepository<Cheque, Long>, ChequeSearchRepositoryInternal {}

interface ChequeSearchRepositoryInternal {
    Flux<Cheque> search(String query);
}

class ChequeSearchRepositoryInternalImpl implements ChequeSearchRepositoryInternal {

    private final ReactiveElasticsearchTemplate reactiveElasticsearchTemplate;

    ChequeSearchRepositoryInternalImpl(ReactiveElasticsearchTemplate reactiveElasticsearchTemplate) {
        this.reactiveElasticsearchTemplate = reactiveElasticsearchTemplate;
    }

    @Override
    public Flux<Cheque> search(String query) {
        NativeSearchQuery nativeSearchQuery = new NativeSearchQuery(queryStringQuery(query));
        return reactiveElasticsearchTemplate.search(nativeSearchQuery, Cheque.class).map(SearchHit::getContent);
    }
}
