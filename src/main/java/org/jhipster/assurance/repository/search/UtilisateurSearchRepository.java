package org.jhipster.assurance.repository.search;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import org.jhipster.assurance.domain.Utilisateur;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ReactiveElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.repository.ReactiveElasticsearchRepository;
import reactor.core.publisher.Flux;

/**
 * Spring Data Elasticsearch repository for the {@link Utilisateur} entity.
 */
public interface UtilisateurSearchRepository
    extends ReactiveElasticsearchRepository<Utilisateur, Long>, UtilisateurSearchRepositoryInternal {}

interface UtilisateurSearchRepositoryInternal {
    Flux<Utilisateur> search(String query, Pageable pageable);
}

class UtilisateurSearchRepositoryInternalImpl implements UtilisateurSearchRepositoryInternal {

    private final ReactiveElasticsearchTemplate reactiveElasticsearchTemplate;

    UtilisateurSearchRepositoryInternalImpl(ReactiveElasticsearchTemplate reactiveElasticsearchTemplate) {
        this.reactiveElasticsearchTemplate = reactiveElasticsearchTemplate;
    }

    @Override
    public Flux<Utilisateur> search(String query, Pageable pageable) {
        NativeSearchQuery nativeSearchQuery = new NativeSearchQuery(queryStringQuery(query));
        nativeSearchQuery.setPageable(pageable);
        return reactiveElasticsearchTemplate.search(nativeSearchQuery, Utilisateur.class).map(SearchHit::getContent);
    }
}
