package org.jhipster.assurance.repository.search;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import org.jhipster.assurance.domain.Indemnisation;
import org.springframework.data.elasticsearch.core.ReactiveElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.repository.ReactiveElasticsearchRepository;
import reactor.core.publisher.Flux;

/**
 * Spring Data Elasticsearch repository for the {@link Indemnisation} entity.
 */
public interface IndemnisationSearchRepository
    extends ReactiveElasticsearchRepository<Indemnisation, Long>, IndemnisationSearchRepositoryInternal {}

interface IndemnisationSearchRepositoryInternal {
    Flux<Indemnisation> search(String query);
}

class IndemnisationSearchRepositoryInternalImpl implements IndemnisationSearchRepositoryInternal {

    private final ReactiveElasticsearchTemplate reactiveElasticsearchTemplate;

    IndemnisationSearchRepositoryInternalImpl(ReactiveElasticsearchTemplate reactiveElasticsearchTemplate) {
        this.reactiveElasticsearchTemplate = reactiveElasticsearchTemplate;
    }

    @Override
    public Flux<Indemnisation> search(String query) {
        NativeSearchQuery nativeSearchQuery = new NativeSearchQuery(queryStringQuery(query));
        return reactiveElasticsearchTemplate.search(nativeSearchQuery, Indemnisation.class).map(SearchHit::getContent);
    }
}
