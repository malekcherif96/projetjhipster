package org.jhipster.assurance.repository.search;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import org.jhipster.assurance.domain.Sinistre;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ReactiveElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.repository.ReactiveElasticsearchRepository;
import reactor.core.publisher.Flux;

/**
 * Spring Data Elasticsearch repository for the {@link Sinistre} entity.
 */
public interface SinistreSearchRepository extends ReactiveElasticsearchRepository<Sinistre, Long>, SinistreSearchRepositoryInternal {}

interface SinistreSearchRepositoryInternal {
    Flux<Sinistre> search(String query, Pageable pageable);
}

class SinistreSearchRepositoryInternalImpl implements SinistreSearchRepositoryInternal {

    private final ReactiveElasticsearchTemplate reactiveElasticsearchTemplate;

    SinistreSearchRepositoryInternalImpl(ReactiveElasticsearchTemplate reactiveElasticsearchTemplate) {
        this.reactiveElasticsearchTemplate = reactiveElasticsearchTemplate;
    }

    @Override
    public Flux<Sinistre> search(String query, Pageable pageable) {
        NativeSearchQuery nativeSearchQuery = new NativeSearchQuery(queryStringQuery(query));
        nativeSearchQuery.setPageable(pageable);
        return reactiveElasticsearchTemplate.search(nativeSearchQuery, Sinistre.class).map(SearchHit::getContent);
    }
}
