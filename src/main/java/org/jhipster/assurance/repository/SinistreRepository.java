package org.jhipster.assurance.repository;

import org.jhipster.assurance.domain.Sinistre;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Sinistre entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SinistreRepository extends R2dbcRepository<Sinistre, Long>, SinistreRepositoryInternal {
    Flux<Sinistre> findAllBy(Pageable pageable);

    // just to avoid having unambigous methods
    @Override
    Flux<Sinistre> findAll();

    @Override
    Mono<Sinistre> findById(Long id);

    @Override
    <S extends Sinistre> Mono<S> save(S entity);
}

interface SinistreRepositoryInternal {
    <S extends Sinistre> Mono<S> insert(S entity);
    <S extends Sinistre> Mono<S> save(S entity);
    Mono<Integer> update(Sinistre entity);

    Flux<Sinistre> findAll();
    Mono<Sinistre> findById(Long id);
    Flux<Sinistre> findAllBy(Pageable pageable);
    Flux<Sinistre> findAllBy(Pageable pageable, Criteria criteria);
}
