package org.jhipster.assurance.service;

import java.util.List;
import org.jhipster.assurance.domain.Cheque;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Interface for managing {@link Cheque}.
 */
public interface ChequeService {
    /**
     * Save a cheque.
     *
     * @param cheque the entity to save.
     * @return the persisted entity.
     */
    Mono<Cheque> save(Cheque cheque);

    /**
     * Partially updates a cheque.
     *
     * @param cheque the entity to update partially.
     * @return the persisted entity.
     */
    Mono<Cheque> partialUpdate(Cheque cheque);

    /**
     * Get all the cheques.
     *
     * @return the list of entities.
     */
    Flux<Cheque> findAll();

    /**
     * Returns the number of cheques available.
     * @return the number of entities in the database.
     *
     */
    Mono<Long> countAll();

    /**
     * Returns the number of cheques available in search repository.
     *
     */
    Mono<Long> searchCount();

    /**
     * Get the "id" cheque.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Mono<Cheque> findOne(Long id);

    /**
     * Delete the "id" cheque.
     *
     * @param id the id of the entity.
     * @return a Mono to signal the deletion
     */
    Mono<Void> delete(Long id);

    /**
     * Search for the cheque corresponding to the query.
     *
     * @param query the query of the search.
     * @return the list of entities.
     */
    Flux<Cheque> search(String query);
}
