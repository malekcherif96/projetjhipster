package org.jhipster.assurance.service;

import java.util.List;
import org.jhipster.assurance.domain.Document;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Interface for managing {@link Document}.
 */
public interface DocumentService {
    /**
     * Save a document.
     *
     * @param document the entity to save.
     * @return the persisted entity.
     */
    Mono<Document> save(Document document);

    /**
     * Partially updates a document.
     *
     * @param document the entity to update partially.
     * @return the persisted entity.
     */
    Mono<Document> partialUpdate(Document document);

    /**
     * Get all the documents.
     *
     * @return the list of entities.
     */
    Flux<Document> findAll();

    /**
     * Returns the number of documents available.
     * @return the number of entities in the database.
     *
     */
    Mono<Long> countAll();

    /**
     * Returns the number of documents available in search repository.
     *
     */
    Mono<Long> searchCount();

    /**
     * Get the "id" document.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Mono<Document> findOne(Long id);

    /**
     * Delete the "id" document.
     *
     * @param id the id of the entity.
     * @return a Mono to signal the deletion
     */
    Mono<Void> delete(Long id);

    /**
     * Search for the document corresponding to the query.
     *
     * @param query the query of the search.
     * @return the list of entities.
     */
    Flux<Document> search(String query);
}
