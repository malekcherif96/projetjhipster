package org.jhipster.assurance.service;

import org.jhipster.assurance.domain.Sinistre;
import org.springframework.data.domain.Pageable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Interface for managing {@link Sinistre}.
 */
public interface SinistreService {
    /**
     * Save a sinistre.
     *
     * @param sinistre the entity to save.
     * @return the persisted entity.
     */
    Mono<Sinistre> save(Sinistre sinistre);

    /**
     * Partially updates a sinistre.
     *
     * @param sinistre the entity to update partially.
     * @return the persisted entity.
     */
    Mono<Sinistre> partialUpdate(Sinistre sinistre);

    /**
     * Get all the sinistres.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Flux<Sinistre> findAll(Pageable pageable);

    /**
     * Returns the number of sinistres available.
     * @return the number of entities in the database.
     *
     */
    Mono<Long> countAll();

    /**
     * Returns the number of sinistres available in search repository.
     *
     */
    Mono<Long> searchCount();

    /**
     * Get the "id" sinistre.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Mono<Sinistre> findOne(Long id);

    /**
     * Delete the "id" sinistre.
     *
     * @param id the id of the entity.
     * @return a Mono to signal the deletion
     */
    Mono<Void> delete(Long id);

    /**
     * Search for the sinistre corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Flux<Sinistre> search(String query, Pageable pageable);
}
