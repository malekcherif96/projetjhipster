package org.jhipster.assurance.service;

import java.util.List;
import org.jhipster.assurance.domain.Quittance;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Interface for managing {@link Quittance}.
 */
public interface QuittanceService {
    /**
     * Save a quittance.
     *
     * @param quittance the entity to save.
     * @return the persisted entity.
     */
    Mono<Quittance> save(Quittance quittance);

    /**
     * Partially updates a quittance.
     *
     * @param quittance the entity to update partially.
     * @return the persisted entity.
     */
    Mono<Quittance> partialUpdate(Quittance quittance);

    /**
     * Get all the quittances.
     *
     * @return the list of entities.
     */
    Flux<Quittance> findAll();

    /**
     * Returns the number of quittances available.
     * @return the number of entities in the database.
     *
     */
    Mono<Long> countAll();

    /**
     * Returns the number of quittances available in search repository.
     *
     */
    Mono<Long> searchCount();

    /**
     * Get the "id" quittance.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Mono<Quittance> findOne(Long id);

    /**
     * Delete the "id" quittance.
     *
     * @param id the id of the entity.
     * @return a Mono to signal the deletion
     */
    Mono<Void> delete(Long id);

    /**
     * Search for the quittance corresponding to the query.
     *
     * @param query the query of the search.
     * @return the list of entities.
     */
    Flux<Quittance> search(String query);
}
