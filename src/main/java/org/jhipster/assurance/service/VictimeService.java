package org.jhipster.assurance.service;

import java.util.List;
import org.jhipster.assurance.domain.Victime;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Interface for managing {@link Victime}.
 */
public interface VictimeService {
    /**
     * Save a victime.
     *
     * @param victime the entity to save.
     * @return the persisted entity.
     */
    Mono<Victime> save(Victime victime);

    /**
     * Partially updates a victime.
     *
     * @param victime the entity to update partially.
     * @return the persisted entity.
     */
    Mono<Victime> partialUpdate(Victime victime);

    /**
     * Get all the victimes.
     *
     * @return the list of entities.
     */
    Flux<Victime> findAll();

    /**
     * Returns the number of victimes available.
     * @return the number of entities in the database.
     *
     */
    Mono<Long> countAll();

    /**
     * Returns the number of victimes available in search repository.
     *
     */
    Mono<Long> searchCount();

    /**
     * Get the "id" victime.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Mono<Victime> findOne(Long id);

    /**
     * Delete the "id" victime.
     *
     * @param id the id of the entity.
     * @return a Mono to signal the deletion
     */
    Mono<Void> delete(Long id);

    /**
     * Search for the victime corresponding to the query.
     *
     * @param query the query of the search.
     * @return the list of entities.
     */
    Flux<Victime> search(String query);
}
