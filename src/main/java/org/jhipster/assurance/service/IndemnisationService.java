package org.jhipster.assurance.service;

import java.util.List;
import org.jhipster.assurance.domain.Indemnisation;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Interface for managing {@link Indemnisation}.
 */
public interface IndemnisationService {
    /**
     * Save a indemnisation.
     *
     * @param indemnisation the entity to save.
     * @return the persisted entity.
     */
    Mono<Indemnisation> save(Indemnisation indemnisation);

    /**
     * Partially updates a indemnisation.
     *
     * @param indemnisation the entity to update partially.
     * @return the persisted entity.
     */
    Mono<Indemnisation> partialUpdate(Indemnisation indemnisation);

    /**
     * Get all the indemnisations.
     *
     * @return the list of entities.
     */
    Flux<Indemnisation> findAll();

    /**
     * Returns the number of indemnisations available.
     * @return the number of entities in the database.
     *
     */
    Mono<Long> countAll();

    /**
     * Returns the number of indemnisations available in search repository.
     *
     */
    Mono<Long> searchCount();

    /**
     * Get the "id" indemnisation.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Mono<Indemnisation> findOne(Long id);

    /**
     * Delete the "id" indemnisation.
     *
     * @param id the id of the entity.
     * @return a Mono to signal the deletion
     */
    Mono<Void> delete(Long id);

    /**
     * Search for the indemnisation corresponding to the query.
     *
     * @param query the query of the search.
     * @return the list of entities.
     */
    Flux<Indemnisation> search(String query);
}
