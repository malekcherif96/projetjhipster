package org.jhipster.assurance.service.impl;

import static org.elasticsearch.index.query.QueryBuilders.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.jhipster.assurance.domain.Cheque;
import org.jhipster.assurance.repository.ChequeRepository;
import org.jhipster.assurance.repository.search.ChequeSearchRepository;
import org.jhipster.assurance.service.ChequeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Implementation for managing {@link Cheque}.
 */
@Service
@Transactional
public class ChequeServiceImpl implements ChequeService {

    private final Logger log = LoggerFactory.getLogger(ChequeServiceImpl.class);

    private final ChequeRepository chequeRepository;

    private final ChequeSearchRepository chequeSearchRepository;

    public ChequeServiceImpl(ChequeRepository chequeRepository, ChequeSearchRepository chequeSearchRepository) {
        this.chequeRepository = chequeRepository;
        this.chequeSearchRepository = chequeSearchRepository;
    }

    @Override
    public Mono<Cheque> save(Cheque cheque) {
        log.debug("Request to save Cheque : {}", cheque);
        return chequeRepository.save(cheque).flatMap(chequeSearchRepository::save);
    }

    @Override
    public Mono<Cheque> partialUpdate(Cheque cheque) {
        log.debug("Request to partially update Cheque : {}", cheque);

        return chequeRepository
            .findById(cheque.getId())
            .map(
                existingCheque -> {
                    if (cheque.getNomCheque() != null) {
                        existingCheque.setNomCheque(cheque.getNomCheque());
                    }
                    if (cheque.getMontnant() != null) {
                        existingCheque.setMontnant(cheque.getMontnant());
                    }

                    return existingCheque;
                }
            )
            .flatMap(chequeRepository::save)
            .flatMap(
                savedCheque -> {
                    chequeSearchRepository.save(savedCheque);

                    return Mono.just(savedCheque);
                }
            );
    }

    @Override
    @Transactional(readOnly = true)
    public Flux<Cheque> findAll() {
        log.debug("Request to get all Cheques");
        return chequeRepository.findAll();
    }

    public Mono<Long> countAll() {
        return chequeRepository.count();
    }

    public Mono<Long> searchCount() {
        return chequeSearchRepository.count();
    }

    @Override
    @Transactional(readOnly = true)
    public Mono<Cheque> findOne(Long id) {
        log.debug("Request to get Cheque : {}", id);
        return chequeRepository.findById(id);
    }

    @Override
    public Mono<Void> delete(Long id) {
        log.debug("Request to delete Cheque : {}", id);
        return chequeRepository.deleteById(id).then(chequeSearchRepository.deleteById(id));
    }

    @Override
    @Transactional(readOnly = true)
    public Flux<Cheque> search(String query) {
        log.debug("Request to search Cheques for query {}", query);
        return chequeSearchRepository.search(query);
    }
}
