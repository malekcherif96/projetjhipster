package org.jhipster.assurance.service.impl;

import static org.elasticsearch.index.query.QueryBuilders.*;

import org.jhipster.assurance.domain.Role;
import org.jhipster.assurance.repository.RoleRepository;
import org.jhipster.assurance.repository.search.RoleSearchRepository;
import org.jhipster.assurance.service.RoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Implementation for managing {@link Role}.
 */
@Service
@Transactional
public class RoleServiceImpl implements RoleService {

    private final Logger log = LoggerFactory.getLogger(RoleServiceImpl.class);

    private final RoleRepository roleRepository;

    private final RoleSearchRepository roleSearchRepository;

    public RoleServiceImpl(RoleRepository roleRepository, RoleSearchRepository roleSearchRepository) {
        this.roleRepository = roleRepository;
        this.roleSearchRepository = roleSearchRepository;
    }

    @Override
    public Mono<Role> save(Role role) {
        log.debug("Request to save Role : {}", role);
        return roleRepository.save(role).flatMap(roleSearchRepository::save);
    }

    @Override
    public Mono<Role> partialUpdate(Role role) {
        log.debug("Request to partially update Role : {}", role);

        return roleRepository
            .findById(role.getId())
            .map(
                existingRole -> {
                    if (role.getName() != null) {
                        existingRole.setName(role.getName());
                    }

                    return existingRole;
                }
            )
            .flatMap(roleRepository::save)
            .flatMap(
                savedRole -> {
                    roleSearchRepository.save(savedRole);

                    return Mono.just(savedRole);
                }
            );
    }

    @Override
    @Transactional(readOnly = true)
    public Flux<Role> findAll(Pageable pageable) {
        log.debug("Request to get all Roles");
        return roleRepository.findAllBy(pageable);
    }

    public Mono<Long> countAll() {
        return roleRepository.count();
    }

    public Mono<Long> searchCount() {
        return roleSearchRepository.count();
    }

    @Override
    @Transactional(readOnly = true)
    public Mono<Role> findOne(Long id) {
        log.debug("Request to get Role : {}", id);
        return roleRepository.findById(id);
    }

    @Override
    public Mono<Void> delete(Long id) {
        log.debug("Request to delete Role : {}", id);
        return roleRepository.deleteById(id).then(roleSearchRepository.deleteById(id));
    }

    @Override
    @Transactional(readOnly = true)
    public Flux<Role> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Roles for query {}", query);
        return roleSearchRepository.search(query, pageable);
    }
}
