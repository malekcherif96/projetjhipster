package org.jhipster.assurance.service.impl;

import static org.elasticsearch.index.query.QueryBuilders.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.jhipster.assurance.domain.Document;
import org.jhipster.assurance.repository.DocumentRepository;
import org.jhipster.assurance.repository.search.DocumentSearchRepository;
import org.jhipster.assurance.service.DocumentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Implementation for managing {@link Document}.
 */
@Service
@Transactional
public class DocumentServiceImpl implements DocumentService {

    private final Logger log = LoggerFactory.getLogger(DocumentServiceImpl.class);

    private final DocumentRepository documentRepository;

    private final DocumentSearchRepository documentSearchRepository;

    public DocumentServiceImpl(DocumentRepository documentRepository, DocumentSearchRepository documentSearchRepository) {
        this.documentRepository = documentRepository;
        this.documentSearchRepository = documentSearchRepository;
    }

    @Override
    public Mono<Document> save(Document document) {
        log.debug("Request to save Document : {}", document);
        return documentRepository.save(document).flatMap(documentSearchRepository::save);
    }

    @Override
    public Mono<Document> partialUpdate(Document document) {
        log.debug("Request to partially update Document : {}", document);

        return documentRepository
            .findById(document.getId())
            .map(
                existingDocument -> {
                    if (document.getLibelle() != null) {
                        existingDocument.setLibelle(document.getLibelle());
                    }
                    if (document.getType() != null) {
                        existingDocument.setType(document.getType());
                    }

                    return existingDocument;
                }
            )
            .flatMap(documentRepository::save)
            .flatMap(
                savedDocument -> {
                    documentSearchRepository.save(savedDocument);

                    return Mono.just(savedDocument);
                }
            );
    }

    @Override
    @Transactional(readOnly = true)
    public Flux<Document> findAll() {
        log.debug("Request to get all Documents");
        return documentRepository.findAll();
    }

    public Mono<Long> countAll() {
        return documentRepository.count();
    }

    public Mono<Long> searchCount() {
        return documentSearchRepository.count();
    }

    @Override
    @Transactional(readOnly = true)
    public Mono<Document> findOne(Long id) {
        log.debug("Request to get Document : {}", id);
        return documentRepository.findById(id);
    }

    @Override
    public Mono<Void> delete(Long id) {
        log.debug("Request to delete Document : {}", id);
        return documentRepository.deleteById(id).then(documentSearchRepository.deleteById(id));
    }

    @Override
    @Transactional(readOnly = true)
    public Flux<Document> search(String query) {
        log.debug("Request to search Documents for query {}", query);
        return documentSearchRepository.search(query);
    }
}
