package org.jhipster.assurance.service.impl;

import static org.elasticsearch.index.query.QueryBuilders.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.jhipster.assurance.domain.Indemnisation;
import org.jhipster.assurance.repository.IndemnisationRepository;
import org.jhipster.assurance.repository.search.IndemnisationSearchRepository;
import org.jhipster.assurance.service.IndemnisationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Implementation for managing {@link Indemnisation}.
 */
@Service
@Transactional
public class IndemnisationServiceImpl implements IndemnisationService {

    private final Logger log = LoggerFactory.getLogger(IndemnisationServiceImpl.class);

    private final IndemnisationRepository indemnisationRepository;

    private final IndemnisationSearchRepository indemnisationSearchRepository;

    public IndemnisationServiceImpl(
        IndemnisationRepository indemnisationRepository,
        IndemnisationSearchRepository indemnisationSearchRepository
    ) {
        this.indemnisationRepository = indemnisationRepository;
        this.indemnisationSearchRepository = indemnisationSearchRepository;
    }

    @Override
    public Mono<Indemnisation> save(Indemnisation indemnisation) {
        log.debug("Request to save Indemnisation : {}", indemnisation);
        return indemnisationRepository.save(indemnisation).flatMap(indemnisationSearchRepository::save);
    }

    @Override
    public Mono<Indemnisation> partialUpdate(Indemnisation indemnisation) {
        log.debug("Request to partially update Indemnisation : {}", indemnisation);

        return indemnisationRepository
            .findById(indemnisation.getId())
            .map(
                existingIndemnisation -> {
                    if (indemnisation.getDate() != null) {
                        existingIndemnisation.setDate(indemnisation.getDate());
                    }

                    return existingIndemnisation;
                }
            )
            .flatMap(indemnisationRepository::save)
            .flatMap(
                savedIndemnisation -> {
                    indemnisationSearchRepository.save(savedIndemnisation);

                    return Mono.just(savedIndemnisation);
                }
            );
    }

    @Override
    @Transactional(readOnly = true)
    public Flux<Indemnisation> findAll() {
        log.debug("Request to get all Indemnisations");
        return indemnisationRepository.findAll();
    }

    public Mono<Long> countAll() {
        return indemnisationRepository.count();
    }

    public Mono<Long> searchCount() {
        return indemnisationSearchRepository.count();
    }

    @Override
    @Transactional(readOnly = true)
    public Mono<Indemnisation> findOne(Long id) {
        log.debug("Request to get Indemnisation : {}", id);
        return indemnisationRepository.findById(id);
    }

    @Override
    public Mono<Void> delete(Long id) {
        log.debug("Request to delete Indemnisation : {}", id);
        return indemnisationRepository.deleteById(id).then(indemnisationSearchRepository.deleteById(id));
    }

    @Override
    @Transactional(readOnly = true)
    public Flux<Indemnisation> search(String query) {
        log.debug("Request to search Indemnisations for query {}", query);
        return indemnisationSearchRepository.search(query);
    }
}
