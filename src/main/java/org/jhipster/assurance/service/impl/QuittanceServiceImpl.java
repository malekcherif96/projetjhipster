package org.jhipster.assurance.service.impl;

import static org.elasticsearch.index.query.QueryBuilders.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.jhipster.assurance.domain.Quittance;
import org.jhipster.assurance.repository.QuittanceRepository;
import org.jhipster.assurance.repository.search.QuittanceSearchRepository;
import org.jhipster.assurance.service.QuittanceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Implementation for managing {@link Quittance}.
 */
@Service
@Transactional
public class QuittanceServiceImpl implements QuittanceService {

    private final Logger log = LoggerFactory.getLogger(QuittanceServiceImpl.class);

    private final QuittanceRepository quittanceRepository;

    private final QuittanceSearchRepository quittanceSearchRepository;

    public QuittanceServiceImpl(QuittanceRepository quittanceRepository, QuittanceSearchRepository quittanceSearchRepository) {
        this.quittanceRepository = quittanceRepository;
        this.quittanceSearchRepository = quittanceSearchRepository;
    }

    @Override
    public Mono<Quittance> save(Quittance quittance) {
        log.debug("Request to save Quittance : {}", quittance);
        return quittanceRepository.save(quittance).flatMap(quittanceSearchRepository::save);
    }

    @Override
    public Mono<Quittance> partialUpdate(Quittance quittance) {
        log.debug("Request to partially update Quittance : {}", quittance);

        return quittanceRepository
            .findById(quittance.getId())
            .map(
                existingQuittance -> {
                    if (quittance.getNumQuittance() != null) {
                        existingQuittance.setNumQuittance(quittance.getNumQuittance());
                    }

                    return existingQuittance;
                }
            )
            .flatMap(quittanceRepository::save)
            .flatMap(
                savedQuittance -> {
                    quittanceSearchRepository.save(savedQuittance);

                    return Mono.just(savedQuittance);
                }
            );
    }

    @Override
    @Transactional(readOnly = true)
    public Flux<Quittance> findAll() {
        log.debug("Request to get all Quittances");
        return quittanceRepository.findAll();
    }

    public Mono<Long> countAll() {
        return quittanceRepository.count();
    }

    public Mono<Long> searchCount() {
        return quittanceSearchRepository.count();
    }

    @Override
    @Transactional(readOnly = true)
    public Mono<Quittance> findOne(Long id) {
        log.debug("Request to get Quittance : {}", id);
        return quittanceRepository.findById(id);
    }

    @Override
    public Mono<Void> delete(Long id) {
        log.debug("Request to delete Quittance : {}", id);
        return quittanceRepository.deleteById(id).then(quittanceSearchRepository.deleteById(id));
    }

    @Override
    @Transactional(readOnly = true)
    public Flux<Quittance> search(String query) {
        log.debug("Request to search Quittances for query {}", query);
        return quittanceSearchRepository.search(query);
    }
}
