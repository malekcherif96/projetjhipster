package org.jhipster.assurance.service.impl;

import static org.elasticsearch.index.query.QueryBuilders.*;

import org.jhipster.assurance.domain.Utilisateur;
import org.jhipster.assurance.repository.UtilisateurRepository;
import org.jhipster.assurance.repository.search.UtilisateurSearchRepository;
import org.jhipster.assurance.service.UtilisateurService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Implementation for managing {@link Utilisateur}.
 */
@Service
@Transactional
public class UtilisateurServiceImpl implements UtilisateurService {

    private final Logger log = LoggerFactory.getLogger(UtilisateurServiceImpl.class);

    private final UtilisateurRepository utilisateurRepository;

    private final UtilisateurSearchRepository utilisateurSearchRepository;

    public UtilisateurServiceImpl(UtilisateurRepository utilisateurRepository, UtilisateurSearchRepository utilisateurSearchRepository) {
        this.utilisateurRepository = utilisateurRepository;
        this.utilisateurSearchRepository = utilisateurSearchRepository;
    }

    @Override
    public Mono<Utilisateur> save(Utilisateur utilisateur) {
        log.debug("Request to save Utilisateur : {}", utilisateur);
        return utilisateurRepository.save(utilisateur).flatMap(utilisateurSearchRepository::save);
    }

    @Override
    public Mono<Utilisateur> partialUpdate(Utilisateur utilisateur) {
        log.debug("Request to partially update Utilisateur : {}", utilisateur);

        return utilisateurRepository
            .findById(utilisateur.getId())
            .map(
                existingUtilisateur -> {
                    if (utilisateur.getNumContrat() != null) {
                        existingUtilisateur.setNumContrat(utilisateur.getNumContrat());
                    }
                    if (utilisateur.getCin() != null) {
                        existingUtilisateur.setCin(utilisateur.getCin());
                    }
                    if (utilisateur.getSituationSocial() != null) {
                        existingUtilisateur.setSituationSocial(utilisateur.getSituationSocial());
                    }
                    if (utilisateur.getNumeroTelephone() != null) {
                        existingUtilisateur.setNumeroTelephone(utilisateur.getNumeroTelephone());
                    }
                    if (utilisateur.getNom() != null) {
                        existingUtilisateur.setNom(utilisateur.getNom());
                    }
                    if (utilisateur.getPrenom() != null) {
                        existingUtilisateur.setPrenom(utilisateur.getPrenom());
                    }
                    if (utilisateur.getLogin() != null) {
                        existingUtilisateur.setLogin(utilisateur.getLogin());
                    }
                    if (utilisateur.getPassword() != null) {
                        existingUtilisateur.setPassword(utilisateur.getPassword());
                    }

                    return existingUtilisateur;
                }
            )
            .flatMap(utilisateurRepository::save)
            .flatMap(
                savedUtilisateur -> {
                    utilisateurSearchRepository.save(savedUtilisateur);

                    return Mono.just(savedUtilisateur);
                }
            );
    }

    @Override
    @Transactional(readOnly = true)
    public Flux<Utilisateur> findAll(Pageable pageable) {
        log.debug("Request to get all Utilisateurs");
        return utilisateurRepository.findAllBy(pageable);
    }

    public Flux<Utilisateur> findAllWithEagerRelationships(Pageable pageable) {
        return utilisateurRepository.findAllWithEagerRelationships(pageable);
    }

    public Mono<Long> countAll() {
        return utilisateurRepository.count();
    }

    public Mono<Long> searchCount() {
        return utilisateurSearchRepository.count();
    }

    @Override
    @Transactional(readOnly = true)
    public Mono<Utilisateur> findOne(Long id) {
        log.debug("Request to get Utilisateur : {}", id);
        return utilisateurRepository.findOneWithEagerRelationships(id);
    }

    @Override
    public Mono<Void> delete(Long id) {
        log.debug("Request to delete Utilisateur : {}", id);
        return utilisateurRepository.deleteById(id).then(utilisateurSearchRepository.deleteById(id));
    }

    @Override
    @Transactional(readOnly = true)
    public Flux<Utilisateur> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Utilisateurs for query {}", query);
        return utilisateurSearchRepository.search(query, pageable);
    }
}
