package org.jhipster.assurance.service.impl;

import static org.elasticsearch.index.query.QueryBuilders.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.jhipster.assurance.domain.Victime;
import org.jhipster.assurance.repository.VictimeRepository;
import org.jhipster.assurance.repository.search.VictimeSearchRepository;
import org.jhipster.assurance.service.VictimeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Implementation for managing {@link Victime}.
 */
@Service
@Transactional
public class VictimeServiceImpl implements VictimeService {

    private final Logger log = LoggerFactory.getLogger(VictimeServiceImpl.class);

    private final VictimeRepository victimeRepository;

    private final VictimeSearchRepository victimeSearchRepository;

    public VictimeServiceImpl(VictimeRepository victimeRepository, VictimeSearchRepository victimeSearchRepository) {
        this.victimeRepository = victimeRepository;
        this.victimeSearchRepository = victimeSearchRepository;
    }

    @Override
    public Mono<Victime> save(Victime victime) {
        log.debug("Request to save Victime : {}", victime);
        return victimeRepository.save(victime).flatMap(victimeSearchRepository::save);
    }

    @Override
    public Mono<Victime> partialUpdate(Victime victime) {
        log.debug("Request to partially update Victime : {}", victime);

        return victimeRepository
            .findById(victime.getId())
            .map(
                existingVictime -> {
                    if (victime.getNumContrat() != null) {
                        existingVictime.setNumContrat(victime.getNumContrat());
                    }
                    if (victime.getCin() != null) {
                        existingVictime.setCin(victime.getCin());
                    }
                    if (victime.getSituationSocial() != null) {
                        existingVictime.setSituationSocial(victime.getSituationSocial());
                    }
                    if (victime.getNumeroTelephone() != null) {
                        existingVictime.setNumeroTelephone(victime.getNumeroTelephone());
                    }
                    if (victime.getNom() != null) {
                        existingVictime.setNom(victime.getNom());
                    }
                    if (victime.getPrenom() != null) {
                        existingVictime.setPrenom(victime.getPrenom());
                    }
                    if (victime.getLogin() != null) {
                        existingVictime.setLogin(victime.getLogin());
                    }
                    if (victime.getPassword() != null) {
                        existingVictime.setPassword(victime.getPassword());
                    }

                    return existingVictime;
                }
            )
            .flatMap(victimeRepository::save)
            .flatMap(
                savedVictime -> {
                    victimeSearchRepository.save(savedVictime);

                    return Mono.just(savedVictime);
                }
            );
    }

    @Override
    @Transactional(readOnly = true)
    public Flux<Victime> findAll() {
        log.debug("Request to get all Victimes");
        return victimeRepository.findAll();
    }

    public Mono<Long> countAll() {
        return victimeRepository.count();
    }

    public Mono<Long> searchCount() {
        return victimeSearchRepository.count();
    }

    @Override
    @Transactional(readOnly = true)
    public Mono<Victime> findOne(Long id) {
        log.debug("Request to get Victime : {}", id);
        return victimeRepository.findById(id);
    }

    @Override
    public Mono<Void> delete(Long id) {
        log.debug("Request to delete Victime : {}", id);
        return victimeRepository.deleteById(id).then(victimeSearchRepository.deleteById(id));
    }

    @Override
    @Transactional(readOnly = true)
    public Flux<Victime> search(String query) {
        log.debug("Request to search Victimes for query {}", query);
        return victimeSearchRepository.search(query);
    }
}
