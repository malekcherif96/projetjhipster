package org.jhipster.assurance.service.impl;

import static org.elasticsearch.index.query.QueryBuilders.*;

import org.jhipster.assurance.domain.Sinistre;
import org.jhipster.assurance.repository.SinistreRepository;
import org.jhipster.assurance.repository.search.SinistreSearchRepository;
import org.jhipster.assurance.service.SinistreService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Implementation for managing {@link Sinistre}.
 */
@Service
@Transactional
public class SinistreServiceImpl implements SinistreService {

    private final Logger log = LoggerFactory.getLogger(SinistreServiceImpl.class);

    private final SinistreRepository sinistreRepository;

    private final SinistreSearchRepository sinistreSearchRepository;

    public SinistreServiceImpl(SinistreRepository sinistreRepository, SinistreSearchRepository sinistreSearchRepository) {
        this.sinistreRepository = sinistreRepository;
        this.sinistreSearchRepository = sinistreSearchRepository;
    }

    @Override
    public Mono<Sinistre> save(Sinistre sinistre) {
        log.debug("Request to save Sinistre : {}", sinistre);
        return sinistreRepository.save(sinistre).flatMap(sinistreSearchRepository::save);
    }

    @Override
    public Mono<Sinistre> partialUpdate(Sinistre sinistre) {
        log.debug("Request to partially update Sinistre : {}", sinistre);

        return sinistreRepository
            .findById(sinistre.getId())
            .map(
                existingSinistre -> {
                    if (sinistre.getTypeSinistre() != null) {
                        existingSinistre.setTypeSinistre(sinistre.getTypeSinistre());
                    }
                    if (sinistre.getDateSinistre() != null) {
                        existingSinistre.setDateSinistre(sinistre.getDateSinistre());
                    }
                    if (sinistre.getLieuSinistre() != null) {
                        existingSinistre.setLieuSinistre(sinistre.getLieuSinistre());
                    }

                    return existingSinistre;
                }
            )
            .flatMap(sinistreRepository::save)
            .flatMap(
                savedSinistre -> {
                    sinistreSearchRepository.save(savedSinistre);

                    return Mono.just(savedSinistre);
                }
            );
    }

    @Override
    @Transactional(readOnly = true)
    public Flux<Sinistre> findAll(Pageable pageable) {
        log.debug("Request to get all Sinistres");
        return sinistreRepository.findAllBy(pageable);
    }

    public Mono<Long> countAll() {
        return sinistreRepository.count();
    }

    public Mono<Long> searchCount() {
        return sinistreSearchRepository.count();
    }

    @Override
    @Transactional(readOnly = true)
    public Mono<Sinistre> findOne(Long id) {
        log.debug("Request to get Sinistre : {}", id);
        return sinistreRepository.findById(id);
    }

    @Override
    public Mono<Void> delete(Long id) {
        log.debug("Request to delete Sinistre : {}", id);
        return sinistreRepository.deleteById(id).then(sinistreSearchRepository.deleteById(id));
    }

    @Override
    @Transactional(readOnly = true)
    public Flux<Sinistre> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Sinistres for query {}", query);
        return sinistreSearchRepository.search(query, pageable);
    }
}
