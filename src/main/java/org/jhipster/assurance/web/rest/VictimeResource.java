package org.jhipster.assurance.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.jhipster.assurance.domain.Victime;
import org.jhipster.assurance.repository.VictimeRepository;
import org.jhipster.assurance.service.VictimeService;
import org.jhipster.assurance.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link org.jhipster.assurance.domain.Victime}.
 */
@RestController
@RequestMapping("/api")
public class VictimeResource {

    private final Logger log = LoggerFactory.getLogger(VictimeResource.class);

    private static final String ENTITY_NAME = "victime";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final VictimeService victimeService;

    private final VictimeRepository victimeRepository;

    public VictimeResource(VictimeService victimeService, VictimeRepository victimeRepository) {
        this.victimeService = victimeService;
        this.victimeRepository = victimeRepository;
    }

    /**
     * {@code POST  /victimes} : Create a new victime.
     *
     * @param victime the victime to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new victime, or with status {@code 400 (Bad Request)} if the victime has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/victimes")
    public Mono<ResponseEntity<Victime>> createVictime(@RequestBody Victime victime) throws URISyntaxException {
        log.debug("REST request to save Victime : {}", victime);
        if (victime.getId() != null) {
            throw new BadRequestAlertException("A new victime cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return victimeService
            .save(victime)
            .map(
                result -> {
                    try {
                        return ResponseEntity
                            .created(new URI("/api/victimes/" + result.getId()))
                            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result);
                    } catch (URISyntaxException e) {
                        throw new RuntimeException(e);
                    }
                }
            );
    }

    /**
     * {@code PUT  /victimes/:id} : Updates an existing victime.
     *
     * @param id the id of the victime to save.
     * @param victime the victime to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated victime,
     * or with status {@code 400 (Bad Request)} if the victime is not valid,
     * or with status {@code 500 (Internal Server Error)} if the victime couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/victimes/{id}")
    public Mono<ResponseEntity<Victime>> updateVictime(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Victime victime
    ) throws URISyntaxException {
        log.debug("REST request to update Victime : {}, {}", id, victime);
        if (victime.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, victime.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return victimeRepository
            .existsById(id)
            .flatMap(
                exists -> {
                    if (!exists) {
                        return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                    }

                    return victimeService
                        .save(victime)
                        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                        .map(
                            result ->
                                ResponseEntity
                                    .ok()
                                    .headers(
                                        HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString())
                                    )
                                    .body(result)
                        );
                }
            );
    }

    /**
     * {@code PATCH  /victimes/:id} : Partial updates given fields of an existing victime, field will ignore if it is null
     *
     * @param id the id of the victime to save.
     * @param victime the victime to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated victime,
     * or with status {@code 400 (Bad Request)} if the victime is not valid,
     * or with status {@code 404 (Not Found)} if the victime is not found,
     * or with status {@code 500 (Internal Server Error)} if the victime couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/victimes/{id}", consumes = "application/merge-patch+json")
    public Mono<ResponseEntity<Victime>> partialUpdateVictime(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Victime victime
    ) throws URISyntaxException {
        log.debug("REST request to partial update Victime partially : {}, {}", id, victime);
        if (victime.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, victime.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return victimeRepository
            .existsById(id)
            .flatMap(
                exists -> {
                    if (!exists) {
                        return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                    }

                    Mono<Victime> result = victimeService.partialUpdate(victime);

                    return result
                        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                        .map(
                            res ->
                                ResponseEntity
                                    .ok()
                                    .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                                    .body(res)
                        );
                }
            );
    }

    /**
     * {@code GET  /victimes} : get all the victimes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of victimes in body.
     */
    @GetMapping("/victimes")
    public Mono<List<Victime>> getAllVictimes() {
        log.debug("REST request to get all Victimes");
        return victimeService.findAll().collectList();
    }

    /**
     * {@code GET  /victimes} : get all the victimes as a stream.
     * @return the {@link Flux} of victimes.
     */
    @GetMapping(value = "/victimes", produces = MediaType.APPLICATION_NDJSON_VALUE)
    public Flux<Victime> getAllVictimesAsStream() {
        log.debug("REST request to get all Victimes as a stream");
        return victimeService.findAll();
    }

    /**
     * {@code GET  /victimes/:id} : get the "id" victime.
     *
     * @param id the id of the victime to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the victime, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/victimes/{id}")
    public Mono<ResponseEntity<Victime>> getVictime(@PathVariable Long id) {
        log.debug("REST request to get Victime : {}", id);
        Mono<Victime> victime = victimeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(victime);
    }

    /**
     * {@code DELETE  /victimes/:id} : delete the "id" victime.
     *
     * @param id the id of the victime to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/victimes/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteVictime(@PathVariable Long id) {
        log.debug("REST request to delete Victime : {}", id);
        return victimeService
            .delete(id)
            .map(
                result ->
                    ResponseEntity
                        .noContent()
                        .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                        .build()
            );
    }

    /**
     * {@code SEARCH  /_search/victimes?query=:query} : search for the victime corresponding
     * to the query.
     *
     * @param query the query of the victime search.
     * @return the result of the search.
     */
    @GetMapping("/_search/victimes")
    public Mono<List<Victime>> searchVictimes(@RequestParam String query) {
        log.debug("REST request to search Victimes for query {}", query);
        return victimeService.search(query).collectList();
    }
}
