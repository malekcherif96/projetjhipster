package org.jhipster.assurance.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.jhipster.assurance.domain.Cheque;
import org.jhipster.assurance.repository.ChequeRepository;
import org.jhipster.assurance.service.ChequeService;
import org.jhipster.assurance.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link org.jhipster.assurance.domain.Cheque}.
 */
@RestController
@RequestMapping("/api")
public class ChequeResource {

    private final Logger log = LoggerFactory.getLogger(ChequeResource.class);

    private static final String ENTITY_NAME = "cheque";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ChequeService chequeService;

    private final ChequeRepository chequeRepository;

    public ChequeResource(ChequeService chequeService, ChequeRepository chequeRepository) {
        this.chequeService = chequeService;
        this.chequeRepository = chequeRepository;
    }

    /**
     * {@code POST  /cheques} : Create a new cheque.
     *
     * @param cheque the cheque to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new cheque, or with status {@code 400 (Bad Request)} if the cheque has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/cheques")
    public Mono<ResponseEntity<Cheque>> createCheque(@RequestBody Cheque cheque) throws URISyntaxException {
        log.debug("REST request to save Cheque : {}", cheque);
        if (cheque.getId() != null) {
            throw new BadRequestAlertException("A new cheque cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return chequeService
            .save(cheque)
            .map(
                result -> {
                    try {
                        return ResponseEntity
                            .created(new URI("/api/cheques/" + result.getId()))
                            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result);
                    } catch (URISyntaxException e) {
                        throw new RuntimeException(e);
                    }
                }
            );
    }

    /**
     * {@code PUT  /cheques/:id} : Updates an existing cheque.
     *
     * @param id the id of the cheque to save.
     * @param cheque the cheque to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cheque,
     * or with status {@code 400 (Bad Request)} if the cheque is not valid,
     * or with status {@code 500 (Internal Server Error)} if the cheque couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/cheques/{id}")
    public Mono<ResponseEntity<Cheque>> updateCheque(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Cheque cheque
    ) throws URISyntaxException {
        log.debug("REST request to update Cheque : {}, {}", id, cheque);
        if (cheque.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, cheque.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return chequeRepository
            .existsById(id)
            .flatMap(
                exists -> {
                    if (!exists) {
                        return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                    }

                    return chequeService
                        .save(cheque)
                        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                        .map(
                            result ->
                                ResponseEntity
                                    .ok()
                                    .headers(
                                        HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString())
                                    )
                                    .body(result)
                        );
                }
            );
    }

    /**
     * {@code PATCH  /cheques/:id} : Partial updates given fields of an existing cheque, field will ignore if it is null
     *
     * @param id the id of the cheque to save.
     * @param cheque the cheque to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cheque,
     * or with status {@code 400 (Bad Request)} if the cheque is not valid,
     * or with status {@code 404 (Not Found)} if the cheque is not found,
     * or with status {@code 500 (Internal Server Error)} if the cheque couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/cheques/{id}", consumes = "application/merge-patch+json")
    public Mono<ResponseEntity<Cheque>> partialUpdateCheque(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Cheque cheque
    ) throws URISyntaxException {
        log.debug("REST request to partial update Cheque partially : {}, {}", id, cheque);
        if (cheque.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, cheque.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return chequeRepository
            .existsById(id)
            .flatMap(
                exists -> {
                    if (!exists) {
                        return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                    }

                    Mono<Cheque> result = chequeService.partialUpdate(cheque);

                    return result
                        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                        .map(
                            res ->
                                ResponseEntity
                                    .ok()
                                    .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                                    .body(res)
                        );
                }
            );
    }

    /**
     * {@code GET  /cheques} : get all the cheques.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cheques in body.
     */
    @GetMapping("/cheques")
    public Mono<List<Cheque>> getAllCheques() {
        log.debug("REST request to get all Cheques");
        return chequeService.findAll().collectList();
    }

    /**
     * {@code GET  /cheques} : get all the cheques as a stream.
     * @return the {@link Flux} of cheques.
     */
    @GetMapping(value = "/cheques", produces = MediaType.APPLICATION_NDJSON_VALUE)
    public Flux<Cheque> getAllChequesAsStream() {
        log.debug("REST request to get all Cheques as a stream");
        return chequeService.findAll();
    }

    /**
     * {@code GET  /cheques/:id} : get the "id" cheque.
     *
     * @param id the id of the cheque to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the cheque, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/cheques/{id}")
    public Mono<ResponseEntity<Cheque>> getCheque(@PathVariable Long id) {
        log.debug("REST request to get Cheque : {}", id);
        Mono<Cheque> cheque = chequeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(cheque);
    }

    /**
     * {@code DELETE  /cheques/:id} : delete the "id" cheque.
     *
     * @param id the id of the cheque to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/cheques/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteCheque(@PathVariable Long id) {
        log.debug("REST request to delete Cheque : {}", id);
        return chequeService
            .delete(id)
            .map(
                result ->
                    ResponseEntity
                        .noContent()
                        .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                        .build()
            );
    }

    /**
     * {@code SEARCH  /_search/cheques?query=:query} : search for the cheque corresponding
     * to the query.
     *
     * @param query the query of the cheque search.
     * @return the result of the search.
     */
    @GetMapping("/_search/cheques")
    public Mono<List<Cheque>> searchCheques(@RequestParam String query) {
        log.debug("REST request to search Cheques for query {}", query);
        return chequeService.search(query).collectList();
    }
}
