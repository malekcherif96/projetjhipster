package org.jhipster.assurance.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.jhipster.assurance.domain.Sinistre;
import org.jhipster.assurance.repository.SinistreRepository;
import org.jhipster.assurance.service.SinistreService;
import org.jhipster.assurance.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link org.jhipster.assurance.domain.Sinistre}.
 */
@RestController
@RequestMapping("/api")
public class SinistreResource {

    private final Logger log = LoggerFactory.getLogger(SinistreResource.class);

    private static final String ENTITY_NAME = "sinistre";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SinistreService sinistreService;

    private final SinistreRepository sinistreRepository;

    public SinistreResource(SinistreService sinistreService, SinistreRepository sinistreRepository) {
        this.sinistreService = sinistreService;
        this.sinistreRepository = sinistreRepository;
    }

    /**
     * {@code POST  /sinistres} : Create a new sinistre.
     *
     * @param sinistre the sinistre to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new sinistre, or with status {@code 400 (Bad Request)} if the sinistre has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/sinistres")
    public Mono<ResponseEntity<Sinistre>> createSinistre(@RequestBody Sinistre sinistre) throws URISyntaxException {
        log.debug("REST request to save Sinistre : {}", sinistre);
        if (sinistre.getId() != null) {
            throw new BadRequestAlertException("A new sinistre cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return sinistreService
            .save(sinistre)
            .map(
                result -> {
                    try {
                        return ResponseEntity
                            .created(new URI("/api/sinistres/" + result.getId()))
                            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result);
                    } catch (URISyntaxException e) {
                        throw new RuntimeException(e);
                    }
                }
            );
    }

    /**
     * {@code PUT  /sinistres/:id} : Updates an existing sinistre.
     *
     * @param id the id of the sinistre to save.
     * @param sinistre the sinistre to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated sinistre,
     * or with status {@code 400 (Bad Request)} if the sinistre is not valid,
     * or with status {@code 500 (Internal Server Error)} if the sinistre couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/sinistres/{id}")
    public Mono<ResponseEntity<Sinistre>> updateSinistre(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Sinistre sinistre
    ) throws URISyntaxException {
        log.debug("REST request to update Sinistre : {}, {}", id, sinistre);
        if (sinistre.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, sinistre.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return sinistreRepository
            .existsById(id)
            .flatMap(
                exists -> {
                    if (!exists) {
                        return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                    }

                    return sinistreService
                        .save(sinistre)
                        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                        .map(
                            result ->
                                ResponseEntity
                                    .ok()
                                    .headers(
                                        HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString())
                                    )
                                    .body(result)
                        );
                }
            );
    }

    /**
     * {@code PATCH  /sinistres/:id} : Partial updates given fields of an existing sinistre, field will ignore if it is null
     *
     * @param id the id of the sinistre to save.
     * @param sinistre the sinistre to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated sinistre,
     * or with status {@code 400 (Bad Request)} if the sinistre is not valid,
     * or with status {@code 404 (Not Found)} if the sinistre is not found,
     * or with status {@code 500 (Internal Server Error)} if the sinistre couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/sinistres/{id}", consumes = "application/merge-patch+json")
    public Mono<ResponseEntity<Sinistre>> partialUpdateSinistre(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Sinistre sinistre
    ) throws URISyntaxException {
        log.debug("REST request to partial update Sinistre partially : {}, {}", id, sinistre);
        if (sinistre.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, sinistre.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return sinistreRepository
            .existsById(id)
            .flatMap(
                exists -> {
                    if (!exists) {
                        return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                    }

                    Mono<Sinistre> result = sinistreService.partialUpdate(sinistre);

                    return result
                        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                        .map(
                            res ->
                                ResponseEntity
                                    .ok()
                                    .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                                    .body(res)
                        );
                }
            );
    }

    /**
     * {@code GET  /sinistres} : get all the sinistres.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of sinistres in body.
     */
    @GetMapping("/sinistres")
    public Mono<ResponseEntity<List<Sinistre>>> getAllSinistres(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of Sinistres");
        return sinistreService
            .countAll()
            .zipWith(sinistreService.findAll(pageable).collectList())
            .map(
                countWithEntities -> {
                    return ResponseEntity
                        .ok()
                        .headers(
                            PaginationUtil.generatePaginationHttpHeaders(
                                UriComponentsBuilder.fromHttpRequest(request),
                                new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                            )
                        )
                        .body(countWithEntities.getT2());
                }
            );
    }

    /**
     * {@code GET  /sinistres/:id} : get the "id" sinistre.
     *
     * @param id the id of the sinistre to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the sinistre, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/sinistres/{id}")
    public Mono<ResponseEntity<Sinistre>> getSinistre(@PathVariable Long id) {
        log.debug("REST request to get Sinistre : {}", id);
        Mono<Sinistre> sinistre = sinistreService.findOne(id);
        return ResponseUtil.wrapOrNotFound(sinistre);
    }

    /**
     * {@code DELETE  /sinistres/:id} : delete the "id" sinistre.
     *
     * @param id the id of the sinistre to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/sinistres/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteSinistre(@PathVariable Long id) {
        log.debug("REST request to delete Sinistre : {}", id);
        return sinistreService
            .delete(id)
            .map(
                result ->
                    ResponseEntity
                        .noContent()
                        .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                        .build()
            );
    }

    /**
     * {@code SEARCH  /_search/sinistres?query=:query} : search for the sinistre corresponding
     * to the query.
     *
     * @param query the query of the sinistre search.
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the result of the search.
     */
    @GetMapping("/_search/sinistres")
    public Mono<ResponseEntity<Flux<Sinistre>>> searchSinistres(@RequestParam String query, Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to search for a page of Sinistres for query {}", query);
        return sinistreService
            .searchCount()
            .map(total -> new PageImpl<>(new ArrayList<>(), pageable, total))
            .map(page -> PaginationUtil.generatePaginationHttpHeaders(UriComponentsBuilder.fromHttpRequest(request), page))
            .map(headers -> ResponseEntity.ok().headers(headers).body(sinistreService.search(query, pageable)));
    }
}
