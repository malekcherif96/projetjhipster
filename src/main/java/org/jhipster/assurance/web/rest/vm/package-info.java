/**
 * View Models used by Spring MVC REST controllers.
 */
package org.jhipster.assurance.web.rest.vm;
