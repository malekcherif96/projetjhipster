package org.jhipster.assurance.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.jhipster.assurance.domain.Quittance;
import org.jhipster.assurance.repository.QuittanceRepository;
import org.jhipster.assurance.service.QuittanceService;
import org.jhipster.assurance.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link org.jhipster.assurance.domain.Quittance}.
 */
@RestController
@RequestMapping("/api")
public class QuittanceResource {

    private final Logger log = LoggerFactory.getLogger(QuittanceResource.class);

    private static final String ENTITY_NAME = "quittance";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final QuittanceService quittanceService;

    private final QuittanceRepository quittanceRepository;

    public QuittanceResource(QuittanceService quittanceService, QuittanceRepository quittanceRepository) {
        this.quittanceService = quittanceService;
        this.quittanceRepository = quittanceRepository;
    }

    /**
     * {@code POST  /quittances} : Create a new quittance.
     *
     * @param quittance the quittance to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new quittance, or with status {@code 400 (Bad Request)} if the quittance has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/quittances")
    public Mono<ResponseEntity<Quittance>> createQuittance(@RequestBody Quittance quittance) throws URISyntaxException {
        log.debug("REST request to save Quittance : {}", quittance);
        if (quittance.getId() != null) {
            throw new BadRequestAlertException("A new quittance cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return quittanceService
            .save(quittance)
            .map(
                result -> {
                    try {
                        return ResponseEntity
                            .created(new URI("/api/quittances/" + result.getId()))
                            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result);
                    } catch (URISyntaxException e) {
                        throw new RuntimeException(e);
                    }
                }
            );
    }

    /**
     * {@code PUT  /quittances/:id} : Updates an existing quittance.
     *
     * @param id the id of the quittance to save.
     * @param quittance the quittance to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated quittance,
     * or with status {@code 400 (Bad Request)} if the quittance is not valid,
     * or with status {@code 500 (Internal Server Error)} if the quittance couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/quittances/{id}")
    public Mono<ResponseEntity<Quittance>> updateQuittance(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Quittance quittance
    ) throws URISyntaxException {
        log.debug("REST request to update Quittance : {}, {}", id, quittance);
        if (quittance.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, quittance.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return quittanceRepository
            .existsById(id)
            .flatMap(
                exists -> {
                    if (!exists) {
                        return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                    }

                    return quittanceService
                        .save(quittance)
                        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                        .map(
                            result ->
                                ResponseEntity
                                    .ok()
                                    .headers(
                                        HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString())
                                    )
                                    .body(result)
                        );
                }
            );
    }

    /**
     * {@code PATCH  /quittances/:id} : Partial updates given fields of an existing quittance, field will ignore if it is null
     *
     * @param id the id of the quittance to save.
     * @param quittance the quittance to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated quittance,
     * or with status {@code 400 (Bad Request)} if the quittance is not valid,
     * or with status {@code 404 (Not Found)} if the quittance is not found,
     * or with status {@code 500 (Internal Server Error)} if the quittance couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/quittances/{id}", consumes = "application/merge-patch+json")
    public Mono<ResponseEntity<Quittance>> partialUpdateQuittance(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Quittance quittance
    ) throws URISyntaxException {
        log.debug("REST request to partial update Quittance partially : {}, {}", id, quittance);
        if (quittance.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, quittance.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return quittanceRepository
            .existsById(id)
            .flatMap(
                exists -> {
                    if (!exists) {
                        return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                    }

                    Mono<Quittance> result = quittanceService.partialUpdate(quittance);

                    return result
                        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                        .map(
                            res ->
                                ResponseEntity
                                    .ok()
                                    .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                                    .body(res)
                        );
                }
            );
    }

    /**
     * {@code GET  /quittances} : get all the quittances.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of quittances in body.
     */
    @GetMapping("/quittances")
    public Mono<List<Quittance>> getAllQuittances() {
        log.debug("REST request to get all Quittances");
        return quittanceService.findAll().collectList();
    }

    /**
     * {@code GET  /quittances} : get all the quittances as a stream.
     * @return the {@link Flux} of quittances.
     */
    @GetMapping(value = "/quittances", produces = MediaType.APPLICATION_NDJSON_VALUE)
    public Flux<Quittance> getAllQuittancesAsStream() {
        log.debug("REST request to get all Quittances as a stream");
        return quittanceService.findAll();
    }

    /**
     * {@code GET  /quittances/:id} : get the "id" quittance.
     *
     * @param id the id of the quittance to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the quittance, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/quittances/{id}")
    public Mono<ResponseEntity<Quittance>> getQuittance(@PathVariable Long id) {
        log.debug("REST request to get Quittance : {}", id);
        Mono<Quittance> quittance = quittanceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(quittance);
    }

    /**
     * {@code DELETE  /quittances/:id} : delete the "id" quittance.
     *
     * @param id the id of the quittance to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/quittances/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteQuittance(@PathVariable Long id) {
        log.debug("REST request to delete Quittance : {}", id);
        return quittanceService
            .delete(id)
            .map(
                result ->
                    ResponseEntity
                        .noContent()
                        .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                        .build()
            );
    }

    /**
     * {@code SEARCH  /_search/quittances?query=:query} : search for the quittance corresponding
     * to the query.
     *
     * @param query the query of the quittance search.
     * @return the result of the search.
     */
    @GetMapping("/_search/quittances")
    public Mono<List<Quittance>> searchQuittances(@RequestParam String query) {
        log.debug("REST request to search Quittances for query {}", query);
        return quittanceService.search(query).collectList();
    }
}
