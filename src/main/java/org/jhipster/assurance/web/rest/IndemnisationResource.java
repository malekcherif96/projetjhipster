package org.jhipster.assurance.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.jhipster.assurance.domain.Indemnisation;
import org.jhipster.assurance.repository.IndemnisationRepository;
import org.jhipster.assurance.service.IndemnisationService;
import org.jhipster.assurance.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link org.jhipster.assurance.domain.Indemnisation}.
 */
@RestController
@RequestMapping("/api")
public class IndemnisationResource {

    private final Logger log = LoggerFactory.getLogger(IndemnisationResource.class);

    private static final String ENTITY_NAME = "indemnisation";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final IndemnisationService indemnisationService;

    private final IndemnisationRepository indemnisationRepository;

    public IndemnisationResource(IndemnisationService indemnisationService, IndemnisationRepository indemnisationRepository) {
        this.indemnisationService = indemnisationService;
        this.indemnisationRepository = indemnisationRepository;
    }

    /**
     * {@code POST  /indemnisations} : Create a new indemnisation.
     *
     * @param indemnisation the indemnisation to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new indemnisation, or with status {@code 400 (Bad Request)} if the indemnisation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/indemnisations")
    public Mono<ResponseEntity<Indemnisation>> createIndemnisation(@RequestBody Indemnisation indemnisation) throws URISyntaxException {
        log.debug("REST request to save Indemnisation : {}", indemnisation);
        if (indemnisation.getId() != null) {
            throw new BadRequestAlertException("A new indemnisation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return indemnisationService
            .save(indemnisation)
            .map(
                result -> {
                    try {
                        return ResponseEntity
                            .created(new URI("/api/indemnisations/" + result.getId()))
                            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result);
                    } catch (URISyntaxException e) {
                        throw new RuntimeException(e);
                    }
                }
            );
    }

    /**
     * {@code PUT  /indemnisations/:id} : Updates an existing indemnisation.
     *
     * @param id the id of the indemnisation to save.
     * @param indemnisation the indemnisation to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated indemnisation,
     * or with status {@code 400 (Bad Request)} if the indemnisation is not valid,
     * or with status {@code 500 (Internal Server Error)} if the indemnisation couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/indemnisations/{id}")
    public Mono<ResponseEntity<Indemnisation>> updateIndemnisation(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Indemnisation indemnisation
    ) throws URISyntaxException {
        log.debug("REST request to update Indemnisation : {}, {}", id, indemnisation);
        if (indemnisation.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, indemnisation.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return indemnisationRepository
            .existsById(id)
            .flatMap(
                exists -> {
                    if (!exists) {
                        return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                    }

                    return indemnisationService
                        .save(indemnisation)
                        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                        .map(
                            result ->
                                ResponseEntity
                                    .ok()
                                    .headers(
                                        HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString())
                                    )
                                    .body(result)
                        );
                }
            );
    }

    /**
     * {@code PATCH  /indemnisations/:id} : Partial updates given fields of an existing indemnisation, field will ignore if it is null
     *
     * @param id the id of the indemnisation to save.
     * @param indemnisation the indemnisation to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated indemnisation,
     * or with status {@code 400 (Bad Request)} if the indemnisation is not valid,
     * or with status {@code 404 (Not Found)} if the indemnisation is not found,
     * or with status {@code 500 (Internal Server Error)} if the indemnisation couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/indemnisations/{id}", consumes = "application/merge-patch+json")
    public Mono<ResponseEntity<Indemnisation>> partialUpdateIndemnisation(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Indemnisation indemnisation
    ) throws URISyntaxException {
        log.debug("REST request to partial update Indemnisation partially : {}, {}", id, indemnisation);
        if (indemnisation.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, indemnisation.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return indemnisationRepository
            .existsById(id)
            .flatMap(
                exists -> {
                    if (!exists) {
                        return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                    }

                    Mono<Indemnisation> result = indemnisationService.partialUpdate(indemnisation);

                    return result
                        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                        .map(
                            res ->
                                ResponseEntity
                                    .ok()
                                    .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                                    .body(res)
                        );
                }
            );
    }

    /**
     * {@code GET  /indemnisations} : get all the indemnisations.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of indemnisations in body.
     */
    @GetMapping("/indemnisations")
    public Mono<List<Indemnisation>> getAllIndemnisations() {
        log.debug("REST request to get all Indemnisations");
        return indemnisationService.findAll().collectList();
    }

    /**
     * {@code GET  /indemnisations} : get all the indemnisations as a stream.
     * @return the {@link Flux} of indemnisations.
     */
    @GetMapping(value = "/indemnisations", produces = MediaType.APPLICATION_NDJSON_VALUE)
    public Flux<Indemnisation> getAllIndemnisationsAsStream() {
        log.debug("REST request to get all Indemnisations as a stream");
        return indemnisationService.findAll();
    }

    /**
     * {@code GET  /indemnisations/:id} : get the "id" indemnisation.
     *
     * @param id the id of the indemnisation to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the indemnisation, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/indemnisations/{id}")
    public Mono<ResponseEntity<Indemnisation>> getIndemnisation(@PathVariable Long id) {
        log.debug("REST request to get Indemnisation : {}", id);
        Mono<Indemnisation> indemnisation = indemnisationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(indemnisation);
    }

    /**
     * {@code DELETE  /indemnisations/:id} : delete the "id" indemnisation.
     *
     * @param id the id of the indemnisation to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/indemnisations/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteIndemnisation(@PathVariable Long id) {
        log.debug("REST request to delete Indemnisation : {}", id);
        return indemnisationService
            .delete(id)
            .map(
                result ->
                    ResponseEntity
                        .noContent()
                        .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                        .build()
            );
    }

    /**
     * {@code SEARCH  /_search/indemnisations?query=:query} : search for the indemnisation corresponding
     * to the query.
     *
     * @param query the query of the indemnisation search.
     * @return the result of the search.
     */
    @GetMapping("/_search/indemnisations")
    public Mono<List<Indemnisation>> searchIndemnisations(@RequestParam String query) {
        log.debug("REST request to search Indemnisations for query {}", query);
        return indemnisationService.search(query).collectList();
    }
}
