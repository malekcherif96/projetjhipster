import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'utilisateur',
        data: { pageTitle: 'assuranceApp.utilisateur.home.title' },
        loadChildren: () => import('./utilisateur/utilisateur.module').then(m => m.UtilisateurModule),
      },
      {
        path: 'victime',
        data: { pageTitle: 'assuranceApp.victime.home.title' },
        loadChildren: () => import('./victime/victime.module').then(m => m.VictimeModule),
      },
      {
        path: 'sinistre',
        data: { pageTitle: 'assuranceApp.sinistre.home.title' },
        loadChildren: () => import('./sinistre/sinistre.module').then(m => m.SinistreModule),
      },
      {
        path: 'quittance',
        data: { pageTitle: 'assuranceApp.quittance.home.title' },
        loadChildren: () => import('./quittance/quittance.module').then(m => m.QuittanceModule),
      },
      {
        path: 'cheque',
        data: { pageTitle: 'assuranceApp.cheque.home.title' },
        loadChildren: () => import('./cheque/cheque.module').then(m => m.ChequeModule),
      },
      {
        path: 'role',
        data: { pageTitle: 'assuranceApp.role.home.title' },
        loadChildren: () => import('./role/role.module').then(m => m.RoleModule),
      },
      {
        path: 'document',
        data: { pageTitle: 'assuranceApp.document.home.title' },
        loadChildren: () => import('./document/document.module').then(m => m.DocumentModule),
      },
      {
        path: 'indemnisation',
        data: { pageTitle: 'assuranceApp.indemnisation.home.title' },
        loadChildren: () => import('./indemnisation/indemnisation.module').then(m => m.IndemnisationModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
