import { IUtilisateur } from 'app/entities/utilisateur/utilisateur.model';
import { ERole } from 'app/entities/enumerations/e-role.model';

export interface IRole {
  id?: number;
  name?: ERole | null;
  utilisateurs?: IUtilisateur[] | null;
}

export class Role implements IRole {
  constructor(public id?: number, public name?: ERole | null, public utilisateurs?: IUtilisateur[] | null) {}
}

export function getRoleIdentifier(role: IRole): number | undefined {
  return role.id;
}
