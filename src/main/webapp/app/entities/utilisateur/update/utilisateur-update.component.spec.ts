jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { UtilisateurService } from '../service/utilisateur.service';
import { IUtilisateur, Utilisateur } from '../utilisateur.model';
import { IRole } from 'app/entities/role/role.model';
import { RoleService } from 'app/entities/role/service/role.service';

import { UtilisateurUpdateComponent } from './utilisateur-update.component';

describe('Component Tests', () => {
  describe('Utilisateur Management Update Component', () => {
    let comp: UtilisateurUpdateComponent;
    let fixture: ComponentFixture<UtilisateurUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let utilisateurService: UtilisateurService;
    let roleService: RoleService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [UtilisateurUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(UtilisateurUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(UtilisateurUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      utilisateurService = TestBed.inject(UtilisateurService);
      roleService = TestBed.inject(RoleService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call Role query and add missing value', () => {
        const utilisateur: IUtilisateur = { id: 456 };
        const roles: IRole[] = [{ id: 94873 }];
        utilisateur.roles = roles;

        const roleCollection: IRole[] = [{ id: 39821 }];
        jest.spyOn(roleService, 'query').mockReturnValue(of(new HttpResponse({ body: roleCollection })));
        const additionalRoles = [...roles];
        const expectedCollection: IRole[] = [...additionalRoles, ...roleCollection];
        jest.spyOn(roleService, 'addRoleToCollectionIfMissing').mockReturnValue(expectedCollection);

        activatedRoute.data = of({ utilisateur });
        comp.ngOnInit();

        expect(roleService.query).toHaveBeenCalled();
        expect(roleService.addRoleToCollectionIfMissing).toHaveBeenCalledWith(roleCollection, ...additionalRoles);
        expect(comp.rolesSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const utilisateur: IUtilisateur = { id: 456 };
        const roles: IRole = { id: 57826 };
        utilisateur.roles = [roles];

        activatedRoute.data = of({ utilisateur });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(utilisateur));
        expect(comp.rolesSharedCollection).toContain(roles);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Utilisateur>>();
        const utilisateur = { id: 123 };
        jest.spyOn(utilisateurService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ utilisateur });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: utilisateur }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(utilisateurService.update).toHaveBeenCalledWith(utilisateur);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Utilisateur>>();
        const utilisateur = new Utilisateur();
        jest.spyOn(utilisateurService, 'create').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ utilisateur });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: utilisateur }));
        saveSubject.complete();

        // THEN
        expect(utilisateurService.create).toHaveBeenCalledWith(utilisateur);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Utilisateur>>();
        const utilisateur = { id: 123 };
        jest.spyOn(utilisateurService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ utilisateur });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(utilisateurService.update).toHaveBeenCalledWith(utilisateur);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackRoleById', () => {
        it('Should return tracked Role primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackRoleById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });

    describe('Getting selected relationships', () => {
      describe('getSelectedRole', () => {
        it('Should return option if no Role is selected', () => {
          const option = { id: 123 };
          const result = comp.getSelectedRole(option);
          expect(result === option).toEqual(true);
        });

        it('Should return selected Role for according option', () => {
          const option = { id: 123 };
          const selected = { id: 123 };
          const selected2 = { id: 456 };
          const result = comp.getSelectedRole(option, [selected2, selected]);
          expect(result === selected).toEqual(true);
          expect(result === selected2).toEqual(false);
          expect(result === option).toEqual(false);
        });

        it('Should return option if this Role is not selected', () => {
          const option = { id: 123 };
          const selected = { id: 456 };
          const result = comp.getSelectedRole(option, [selected]);
          expect(result === option).toEqual(true);
          expect(result === selected).toEqual(false);
        });
      });
    });
  });
});
