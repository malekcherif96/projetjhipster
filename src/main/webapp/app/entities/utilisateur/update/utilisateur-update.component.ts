import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IUtilisateur, Utilisateur } from '../utilisateur.model';
import { UtilisateurService } from '../service/utilisateur.service';
import { IRole } from 'app/entities/role/role.model';
import { RoleService } from 'app/entities/role/service/role.service';

@Component({
  selector: 'jhi-utilisateur-update',
  templateUrl: './utilisateur-update.component.html',
})
export class UtilisateurUpdateComponent implements OnInit {
  isSaving = false;

  rolesSharedCollection: IRole[] = [];

  editForm = this.fb.group({
    id: [],
    numContrat: [],
    cin: [],
    situationSocial: [],
    numeroTelephone: [],
    nom: [],
    prenom: [],
    login: [],
    password: [],
    roles: [],
  });

  constructor(
    protected utilisateurService: UtilisateurService,
    protected roleService: RoleService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ utilisateur }) => {
      this.updateForm(utilisateur);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const utilisateur = this.createFromForm();
    if (utilisateur.id !== undefined) {
      this.subscribeToSaveResponse(this.utilisateurService.update(utilisateur));
    } else {
      this.subscribeToSaveResponse(this.utilisateurService.create(utilisateur));
    }
  }

  trackRoleById(index: number, item: IRole): number {
    return item.id!;
  }

  getSelectedRole(option: IRole, selectedVals?: IRole[]): IRole {
    if (selectedVals) {
      for (const selectedVal of selectedVals) {
        if (option.id === selectedVal.id) {
          return selectedVal;
        }
      }
    }
    return option;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUtilisateur>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(utilisateur: IUtilisateur): void {
    this.editForm.patchValue({
      id: utilisateur.id,
      numContrat: utilisateur.numContrat,
      cin: utilisateur.cin,
      situationSocial: utilisateur.situationSocial,
      numeroTelephone: utilisateur.numeroTelephone,
      nom: utilisateur.nom,
      prenom: utilisateur.prenom,
      login: utilisateur.login,
      password: utilisateur.password,
      roles: utilisateur.roles,
    });

    this.rolesSharedCollection = this.roleService.addRoleToCollectionIfMissing(this.rolesSharedCollection, ...(utilisateur.roles ?? []));
  }

  protected loadRelationshipsOptions(): void {
    this.roleService
      .query()
      .pipe(map((res: HttpResponse<IRole[]>) => res.body ?? []))
      .pipe(map((roles: IRole[]) => this.roleService.addRoleToCollectionIfMissing(roles, ...(this.editForm.get('roles')!.value ?? []))))
      .subscribe((roles: IRole[]) => (this.rolesSharedCollection = roles));
  }

  protected createFromForm(): IUtilisateur {
    return {
      ...new Utilisateur(),
      id: this.editForm.get(['id'])!.value,
      numContrat: this.editForm.get(['numContrat'])!.value,
      cin: this.editForm.get(['cin'])!.value,
      situationSocial: this.editForm.get(['situationSocial'])!.value,
      numeroTelephone: this.editForm.get(['numeroTelephone'])!.value,
      nom: this.editForm.get(['nom'])!.value,
      prenom: this.editForm.get(['prenom'])!.value,
      login: this.editForm.get(['login'])!.value,
      password: this.editForm.get(['password'])!.value,
      roles: this.editForm.get(['roles'])!.value,
    };
  }
}
