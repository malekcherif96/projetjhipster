import { IRole } from 'app/entities/role/role.model';

export interface IUtilisateur {
  id?: number;
  numContrat?: string | null;
  cin?: number | null;
  situationSocial?: string | null;
  numeroTelephone?: number | null;
  nom?: string | null;
  prenom?: string | null;
  login?: string | null;
  password?: string | null;
  roles?: IRole[] | null;
}

export class Utilisateur implements IUtilisateur {
  constructor(
    public id?: number,
    public numContrat?: string | null,
    public cin?: number | null,
    public situationSocial?: string | null,
    public numeroTelephone?: number | null,
    public nom?: string | null,
    public prenom?: string | null,
    public login?: string | null,
    public password?: string | null,
    public roles?: IRole[] | null
  ) {}
}

export function getUtilisateurIdentifier(utilisateur: IUtilisateur): number | undefined {
  return utilisateur.id;
}
