import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { ISinistre, Sinistre } from '../sinistre.model';
import { SinistreService } from '../service/sinistre.service';

@Component({
  selector: 'jhi-sinistre-update',
  templateUrl: './sinistre-update.component.html',
})
export class SinistreUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    typeSinistre: [],
    dateSinistre: [],
    lieuSinistre: [],
  });

  constructor(protected sinistreService: SinistreService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ sinistre }) => {
      this.updateForm(sinistre);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const sinistre = this.createFromForm();
    if (sinistre.id !== undefined) {
      this.subscribeToSaveResponse(this.sinistreService.update(sinistre));
    } else {
      this.subscribeToSaveResponse(this.sinistreService.create(sinistre));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISinistre>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(sinistre: ISinistre): void {
    this.editForm.patchValue({
      id: sinistre.id,
      typeSinistre: sinistre.typeSinistre,
      dateSinistre: sinistre.dateSinistre,
      lieuSinistre: sinistre.lieuSinistre,
    });
  }

  protected createFromForm(): ISinistre {
    return {
      ...new Sinistre(),
      id: this.editForm.get(['id'])!.value,
      typeSinistre: this.editForm.get(['typeSinistre'])!.value,
      dateSinistre: this.editForm.get(['dateSinistre'])!.value,
      lieuSinistre: this.editForm.get(['lieuSinistre'])!.value,
    };
  }
}
