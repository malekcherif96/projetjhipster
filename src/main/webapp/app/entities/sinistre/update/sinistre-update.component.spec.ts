jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { SinistreService } from '../service/sinistre.service';
import { ISinistre, Sinistre } from '../sinistre.model';

import { SinistreUpdateComponent } from './sinistre-update.component';

describe('Component Tests', () => {
  describe('Sinistre Management Update Component', () => {
    let comp: SinistreUpdateComponent;
    let fixture: ComponentFixture<SinistreUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let sinistreService: SinistreService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [SinistreUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(SinistreUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SinistreUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      sinistreService = TestBed.inject(SinistreService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const sinistre: ISinistre = { id: 456 };

        activatedRoute.data = of({ sinistre });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(sinistre));
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Sinistre>>();
        const sinistre = { id: 123 };
        jest.spyOn(sinistreService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ sinistre });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: sinistre }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(sinistreService.update).toHaveBeenCalledWith(sinistre);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Sinistre>>();
        const sinistre = new Sinistre();
        jest.spyOn(sinistreService, 'create').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ sinistre });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: sinistre }));
        saveSubject.complete();

        // THEN
        expect(sinistreService.create).toHaveBeenCalledWith(sinistre);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Sinistre>>();
        const sinistre = { id: 123 };
        jest.spyOn(sinistreService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ sinistre });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(sinistreService.update).toHaveBeenCalledWith(sinistre);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });
  });
});
