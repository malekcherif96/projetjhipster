import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { SearchWithPagination } from 'app/core/request/request.model';
import { ISinistre, getSinistreIdentifier } from '../sinistre.model';

export type EntityResponseType = HttpResponse<ISinistre>;
export type EntityArrayResponseType = HttpResponse<ISinistre[]>;

@Injectable({ providedIn: 'root' })
export class SinistreService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/sinistres');
  protected resourceSearchUrl = this.applicationConfigService.getEndpointFor('api/_search/sinistres');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(sinistre: ISinistre): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(sinistre);
    return this.http
      .post<ISinistre>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(sinistre: ISinistre): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(sinistre);
    return this.http
      .put<ISinistre>(`${this.resourceUrl}/${getSinistreIdentifier(sinistre) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(sinistre: ISinistre): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(sinistre);
    return this.http
      .patch<ISinistre>(`${this.resourceUrl}/${getSinistreIdentifier(sinistre) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ISinistre>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ISinistre[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: SearchWithPagination): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ISinistre[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  addSinistreToCollectionIfMissing(sinistreCollection: ISinistre[], ...sinistresToCheck: (ISinistre | null | undefined)[]): ISinistre[] {
    const sinistres: ISinistre[] = sinistresToCheck.filter(isPresent);
    if (sinistres.length > 0) {
      const sinistreCollectionIdentifiers = sinistreCollection.map(sinistreItem => getSinistreIdentifier(sinistreItem)!);
      const sinistresToAdd = sinistres.filter(sinistreItem => {
        const sinistreIdentifier = getSinistreIdentifier(sinistreItem);
        if (sinistreIdentifier == null || sinistreCollectionIdentifiers.includes(sinistreIdentifier)) {
          return false;
        }
        sinistreCollectionIdentifiers.push(sinistreIdentifier);
        return true;
      });
      return [...sinistresToAdd, ...sinistreCollection];
    }
    return sinistreCollection;
  }

  protected convertDateFromClient(sinistre: ISinistre): ISinistre {
    return Object.assign({}, sinistre, {
      dateSinistre: sinistre.dateSinistre?.isValid() ? sinistre.dateSinistre.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateSinistre = res.body.dateSinistre ? dayjs(res.body.dateSinistre) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((sinistre: ISinistre) => {
        sinistre.dateSinistre = sinistre.dateSinistre ? dayjs(sinistre.dateSinistre) : undefined;
      });
    }
    return res;
  }
}
