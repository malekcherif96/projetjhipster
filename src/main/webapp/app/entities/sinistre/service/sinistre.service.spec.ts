import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_FORMAT } from 'app/config/input.constants';
import { ISinistre, Sinistre } from '../sinistre.model';

import { SinistreService } from './sinistre.service';

describe('Service Tests', () => {
  describe('Sinistre Service', () => {
    let service: SinistreService;
    let httpMock: HttpTestingController;
    let elemDefault: ISinistre;
    let expectedResult: ISinistre | ISinistre[] | boolean | null;
    let currentDate: dayjs.Dayjs;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(SinistreService);
      httpMock = TestBed.inject(HttpTestingController);
      currentDate = dayjs();

      elemDefault = {
        id: 0,
        typeSinistre: 'AAAAAAA',
        dateSinistre: currentDate,
        lieuSinistre: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            dateSinistre: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Sinistre', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            dateSinistre: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateSinistre: currentDate,
          },
          returnedFromService
        );

        service.create(new Sinistre()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Sinistre', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            typeSinistre: 'BBBBBB',
            dateSinistre: currentDate.format(DATE_FORMAT),
            lieuSinistre: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateSinistre: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a Sinistre', () => {
        const patchObject = Object.assign(
          {
            typeSinistre: 'BBBBBB',
            lieuSinistre: 'BBBBBB',
          },
          new Sinistre()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign(
          {
            dateSinistre: currentDate,
          },
          returnedFromService
        );

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Sinistre', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            typeSinistre: 'BBBBBB',
            dateSinistre: currentDate.format(DATE_FORMAT),
            lieuSinistre: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateSinistre: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Sinistre', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addSinistreToCollectionIfMissing', () => {
        it('should add a Sinistre to an empty array', () => {
          const sinistre: ISinistre = { id: 123 };
          expectedResult = service.addSinistreToCollectionIfMissing([], sinistre);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(sinistre);
        });

        it('should not add a Sinistre to an array that contains it', () => {
          const sinistre: ISinistre = { id: 123 };
          const sinistreCollection: ISinistre[] = [
            {
              ...sinistre,
            },
            { id: 456 },
          ];
          expectedResult = service.addSinistreToCollectionIfMissing(sinistreCollection, sinistre);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a Sinistre to an array that doesn't contain it", () => {
          const sinistre: ISinistre = { id: 123 };
          const sinistreCollection: ISinistre[] = [{ id: 456 }];
          expectedResult = service.addSinistreToCollectionIfMissing(sinistreCollection, sinistre);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(sinistre);
        });

        it('should add only unique Sinistre to an array', () => {
          const sinistreArray: ISinistre[] = [{ id: 123 }, { id: 456 }, { id: 34521 }];
          const sinistreCollection: ISinistre[] = [{ id: 123 }];
          expectedResult = service.addSinistreToCollectionIfMissing(sinistreCollection, ...sinistreArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const sinistre: ISinistre = { id: 123 };
          const sinistre2: ISinistre = { id: 456 };
          expectedResult = service.addSinistreToCollectionIfMissing([], sinistre, sinistre2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(sinistre);
          expect(expectedResult).toContain(sinistre2);
        });

        it('should accept null and undefined values', () => {
          const sinistre: ISinistre = { id: 123 };
          expectedResult = service.addSinistreToCollectionIfMissing([], null, sinistre, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(sinistre);
        });

        it('should return initial array if no Sinistre is added', () => {
          const sinistreCollection: ISinistre[] = [{ id: 123 }];
          expectedResult = service.addSinistreToCollectionIfMissing(sinistreCollection, undefined, null);
          expect(expectedResult).toEqual(sinistreCollection);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
