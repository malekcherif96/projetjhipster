import * as dayjs from 'dayjs';

export interface ISinistre {
  id?: number;
  typeSinistre?: string | null;
  dateSinistre?: dayjs.Dayjs | null;
  lieuSinistre?: string | null;
}

export class Sinistre implements ISinistre {
  constructor(
    public id?: number,
    public typeSinistre?: string | null,
    public dateSinistre?: dayjs.Dayjs | null,
    public lieuSinistre?: string | null
  ) {}
}

export function getSinistreIdentifier(sinistre: ISinistre): number | undefined {
  return sinistre.id;
}
