import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISinistre } from '../sinistre.model';

@Component({
  selector: 'jhi-sinistre-detail',
  templateUrl: './sinistre-detail.component.html',
})
export class SinistreDetailComponent implements OnInit {
  sinistre: ISinistre | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ sinistre }) => {
      this.sinistre = sinistre;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
