import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SinistreDetailComponent } from './sinistre-detail.component';

describe('Component Tests', () => {
  describe('Sinistre Management Detail Component', () => {
    let comp: SinistreDetailComponent;
    let fixture: ComponentFixture<SinistreDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [SinistreDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ sinistre: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(SinistreDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SinistreDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load sinistre on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.sinistre).toEqual(expect.objectContaining({ id: 123 }));
      });
    });
  });
});
