jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { ISinistre, Sinistre } from '../sinistre.model';
import { SinistreService } from '../service/sinistre.service';

import { SinistreRoutingResolveService } from './sinistre-routing-resolve.service';

describe('Service Tests', () => {
  describe('Sinistre routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: SinistreRoutingResolveService;
    let service: SinistreService;
    let resultSinistre: ISinistre | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(SinistreRoutingResolveService);
      service = TestBed.inject(SinistreService);
      resultSinistre = undefined;
    });

    describe('resolve', () => {
      it('should return ISinistre returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultSinistre = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultSinistre).toEqual({ id: 123 });
      });

      it('should return new ISinistre if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultSinistre = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultSinistre).toEqual(new Sinistre());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as Sinistre })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultSinistre = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultSinistre).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
