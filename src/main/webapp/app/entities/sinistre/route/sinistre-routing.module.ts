import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { SinistreComponent } from '../list/sinistre.component';
import { SinistreDetailComponent } from '../detail/sinistre-detail.component';
import { SinistreUpdateComponent } from '../update/sinistre-update.component';
import { SinistreRoutingResolveService } from './sinistre-routing-resolve.service';

const sinistreRoute: Routes = [
  {
    path: '',
    component: SinistreComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: SinistreDetailComponent,
    resolve: {
      sinistre: SinistreRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: SinistreUpdateComponent,
    resolve: {
      sinistre: SinistreRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: SinistreUpdateComponent,
    resolve: {
      sinistre: SinistreRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(sinistreRoute)],
  exports: [RouterModule],
})
export class SinistreRoutingModule {}
