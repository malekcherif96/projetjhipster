import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { SinistreComponent } from './list/sinistre.component';
import { SinistreDetailComponent } from './detail/sinistre-detail.component';
import { SinistreUpdateComponent } from './update/sinistre-update.component';
import { SinistreDeleteDialogComponent } from './delete/sinistre-delete-dialog.component';
import { SinistreRoutingModule } from './route/sinistre-routing.module';

@NgModule({
  imports: [SharedModule, SinistreRoutingModule],
  declarations: [SinistreComponent, SinistreDetailComponent, SinistreUpdateComponent, SinistreDeleteDialogComponent],
  entryComponents: [SinistreDeleteDialogComponent],
})
export class SinistreModule {}
