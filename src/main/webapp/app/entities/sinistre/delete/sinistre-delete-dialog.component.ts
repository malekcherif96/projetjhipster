import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ISinistre } from '../sinistre.model';
import { SinistreService } from '../service/sinistre.service';

@Component({
  templateUrl: './sinistre-delete-dialog.component.html',
})
export class SinistreDeleteDialogComponent {
  sinistre?: ISinistre;

  constructor(protected sinistreService: SinistreService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.sinistreService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
