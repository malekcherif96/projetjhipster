import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IIndemnisation } from '../indemnisation.model';
import { IndemnisationService } from '../service/indemnisation.service';
import { IndemnisationDeleteDialogComponent } from '../delete/indemnisation-delete-dialog.component';

@Component({
  selector: 'jhi-indemnisation',
  templateUrl: './indemnisation.component.html',
})
export class IndemnisationComponent implements OnInit {
  indemnisations?: IIndemnisation[];
  isLoading = false;
  currentSearch: string;

  constructor(
    protected indemnisationService: IndemnisationService,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch = this.activatedRoute.snapshot.queryParams['search'] ?? '';
  }

  loadAll(): void {
    this.isLoading = true;
    if (this.currentSearch) {
      this.indemnisationService
        .search({
          query: this.currentSearch,
        })
        .subscribe(
          (res: HttpResponse<IIndemnisation[]>) => {
            this.isLoading = false;
            this.indemnisations = res.body ?? [];
          },
          () => {
            this.isLoading = false;
          }
        );
      return;
    }

    this.indemnisationService.query().subscribe(
      (res: HttpResponse<IIndemnisation[]>) => {
        this.isLoading = false;
        this.indemnisations = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IIndemnisation): number {
    return item.id!;
  }

  delete(indemnisation: IIndemnisation): void {
    const modalRef = this.modalService.open(IndemnisationDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.indemnisation = indemnisation;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
