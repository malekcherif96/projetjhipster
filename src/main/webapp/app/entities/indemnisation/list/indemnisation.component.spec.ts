jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { IndemnisationService } from '../service/indemnisation.service';

import { IndemnisationComponent } from './indemnisation.component';

describe('Component Tests', () => {
  describe('Indemnisation Management Component', () => {
    let comp: IndemnisationComponent;
    let fixture: ComponentFixture<IndemnisationComponent>;
    let service: IndemnisationService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [IndemnisationComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { snapshot: { queryParams: {} } },
          },
        ],
      })
        .overrideTemplate(IndemnisationComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(IndemnisationComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(IndemnisationService);

      const headers = new HttpHeaders().append('link', 'link;link');
      jest.spyOn(service, 'query').mockReturnValue(
        of(
          new HttpResponse({
            body: [{ id: 123 }],
            headers,
          })
        )
      );
    });

    it('Should call load all on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.indemnisations?.[0]).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
