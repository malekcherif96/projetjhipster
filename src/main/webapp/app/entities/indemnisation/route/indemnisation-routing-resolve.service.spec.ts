jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IIndemnisation, Indemnisation } from '../indemnisation.model';
import { IndemnisationService } from '../service/indemnisation.service';

import { IndemnisationRoutingResolveService } from './indemnisation-routing-resolve.service';

describe('Service Tests', () => {
  describe('Indemnisation routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: IndemnisationRoutingResolveService;
    let service: IndemnisationService;
    let resultIndemnisation: IIndemnisation | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(IndemnisationRoutingResolveService);
      service = TestBed.inject(IndemnisationService);
      resultIndemnisation = undefined;
    });

    describe('resolve', () => {
      it('should return IIndemnisation returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultIndemnisation = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultIndemnisation).toEqual({ id: 123 });
      });

      it('should return new IIndemnisation if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultIndemnisation = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultIndemnisation).toEqual(new Indemnisation());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as Indemnisation })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultIndemnisation = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultIndemnisation).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
