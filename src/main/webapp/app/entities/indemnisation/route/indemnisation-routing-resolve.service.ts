import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IIndemnisation, Indemnisation } from '../indemnisation.model';
import { IndemnisationService } from '../service/indemnisation.service';

@Injectable({ providedIn: 'root' })
export class IndemnisationRoutingResolveService implements Resolve<IIndemnisation> {
  constructor(protected service: IndemnisationService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IIndemnisation> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((indemnisation: HttpResponse<Indemnisation>) => {
          if (indemnisation.body) {
            return of(indemnisation.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Indemnisation());
  }
}
