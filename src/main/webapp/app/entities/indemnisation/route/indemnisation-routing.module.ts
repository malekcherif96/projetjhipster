import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { IndemnisationComponent } from '../list/indemnisation.component';
import { IndemnisationDetailComponent } from '../detail/indemnisation-detail.component';
import { IndemnisationUpdateComponent } from '../update/indemnisation-update.component';
import { IndemnisationRoutingResolveService } from './indemnisation-routing-resolve.service';

const indemnisationRoute: Routes = [
  {
    path: '',
    component: IndemnisationComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: IndemnisationDetailComponent,
    resolve: {
      indemnisation: IndemnisationRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: IndemnisationUpdateComponent,
    resolve: {
      indemnisation: IndemnisationRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: IndemnisationUpdateComponent,
    resolve: {
      indemnisation: IndemnisationRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(indemnisationRoute)],
  exports: [RouterModule],
})
export class IndemnisationRoutingModule {}
