import * as dayjs from 'dayjs';

export interface IIndemnisation {
  id?: number;
  date?: dayjs.Dayjs | null;
}

export class Indemnisation implements IIndemnisation {
  constructor(public id?: number, public date?: dayjs.Dayjs | null) {}
}

export function getIndemnisationIdentifier(indemnisation: IIndemnisation): number | undefined {
  return indemnisation.id;
}
