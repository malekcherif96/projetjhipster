import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IIndemnisation } from '../indemnisation.model';
import { IndemnisationService } from '../service/indemnisation.service';

@Component({
  templateUrl: './indemnisation-delete-dialog.component.html',
})
export class IndemnisationDeleteDialogComponent {
  indemnisation?: IIndemnisation;

  constructor(protected indemnisationService: IndemnisationService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.indemnisationService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
