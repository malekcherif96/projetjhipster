import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { Search } from 'app/core/request/request.model';
import { IIndemnisation, getIndemnisationIdentifier } from '../indemnisation.model';

export type EntityResponseType = HttpResponse<IIndemnisation>;
export type EntityArrayResponseType = HttpResponse<IIndemnisation[]>;

@Injectable({ providedIn: 'root' })
export class IndemnisationService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/indemnisations');
  protected resourceSearchUrl = this.applicationConfigService.getEndpointFor('api/_search/indemnisations');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(indemnisation: IIndemnisation): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(indemnisation);
    return this.http
      .post<IIndemnisation>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(indemnisation: IIndemnisation): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(indemnisation);
    return this.http
      .put<IIndemnisation>(`${this.resourceUrl}/${getIndemnisationIdentifier(indemnisation) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(indemnisation: IIndemnisation): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(indemnisation);
    return this.http
      .patch<IIndemnisation>(`${this.resourceUrl}/${getIndemnisationIdentifier(indemnisation) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IIndemnisation>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IIndemnisation[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IIndemnisation[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  addIndemnisationToCollectionIfMissing(
    indemnisationCollection: IIndemnisation[],
    ...indemnisationsToCheck: (IIndemnisation | null | undefined)[]
  ): IIndemnisation[] {
    const indemnisations: IIndemnisation[] = indemnisationsToCheck.filter(isPresent);
    if (indemnisations.length > 0) {
      const indemnisationCollectionIdentifiers = indemnisationCollection.map(
        indemnisationItem => getIndemnisationIdentifier(indemnisationItem)!
      );
      const indemnisationsToAdd = indemnisations.filter(indemnisationItem => {
        const indemnisationIdentifier = getIndemnisationIdentifier(indemnisationItem);
        if (indemnisationIdentifier == null || indemnisationCollectionIdentifiers.includes(indemnisationIdentifier)) {
          return false;
        }
        indemnisationCollectionIdentifiers.push(indemnisationIdentifier);
        return true;
      });
      return [...indemnisationsToAdd, ...indemnisationCollection];
    }
    return indemnisationCollection;
  }

  protected convertDateFromClient(indemnisation: IIndemnisation): IIndemnisation {
    return Object.assign({}, indemnisation, {
      date: indemnisation.date?.isValid() ? indemnisation.date.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.date = res.body.date ? dayjs(res.body.date) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((indemnisation: IIndemnisation) => {
        indemnisation.date = indemnisation.date ? dayjs(indemnisation.date) : undefined;
      });
    }
    return res;
  }
}
