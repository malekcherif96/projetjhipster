import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IIndemnisation, Indemnisation } from '../indemnisation.model';

import { IndemnisationService } from './indemnisation.service';

describe('Service Tests', () => {
  describe('Indemnisation Service', () => {
    let service: IndemnisationService;
    let httpMock: HttpTestingController;
    let elemDefault: IIndemnisation;
    let expectedResult: IIndemnisation | IIndemnisation[] | boolean | null;
    let currentDate: dayjs.Dayjs;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(IndemnisationService);
      httpMock = TestBed.inject(HttpTestingController);
      currentDate = dayjs();

      elemDefault = {
        id: 0,
        date: currentDate,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            date: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Indemnisation', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            date: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            date: currentDate,
          },
          returnedFromService
        );

        service.create(new Indemnisation()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Indemnisation', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            date: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            date: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a Indemnisation', () => {
        const patchObject = Object.assign({}, new Indemnisation());

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign(
          {
            date: currentDate,
          },
          returnedFromService
        );

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Indemnisation', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            date: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            date: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Indemnisation', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addIndemnisationToCollectionIfMissing', () => {
        it('should add a Indemnisation to an empty array', () => {
          const indemnisation: IIndemnisation = { id: 123 };
          expectedResult = service.addIndemnisationToCollectionIfMissing([], indemnisation);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(indemnisation);
        });

        it('should not add a Indemnisation to an array that contains it', () => {
          const indemnisation: IIndemnisation = { id: 123 };
          const indemnisationCollection: IIndemnisation[] = [
            {
              ...indemnisation,
            },
            { id: 456 },
          ];
          expectedResult = service.addIndemnisationToCollectionIfMissing(indemnisationCollection, indemnisation);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a Indemnisation to an array that doesn't contain it", () => {
          const indemnisation: IIndemnisation = { id: 123 };
          const indemnisationCollection: IIndemnisation[] = [{ id: 456 }];
          expectedResult = service.addIndemnisationToCollectionIfMissing(indemnisationCollection, indemnisation);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(indemnisation);
        });

        it('should add only unique Indemnisation to an array', () => {
          const indemnisationArray: IIndemnisation[] = [{ id: 123 }, { id: 456 }, { id: 10010 }];
          const indemnisationCollection: IIndemnisation[] = [{ id: 123 }];
          expectedResult = service.addIndemnisationToCollectionIfMissing(indemnisationCollection, ...indemnisationArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const indemnisation: IIndemnisation = { id: 123 };
          const indemnisation2: IIndemnisation = { id: 456 };
          expectedResult = service.addIndemnisationToCollectionIfMissing([], indemnisation, indemnisation2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(indemnisation);
          expect(expectedResult).toContain(indemnisation2);
        });

        it('should accept null and undefined values', () => {
          const indemnisation: IIndemnisation = { id: 123 };
          expectedResult = service.addIndemnisationToCollectionIfMissing([], null, indemnisation, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(indemnisation);
        });

        it('should return initial array if no Indemnisation is added', () => {
          const indemnisationCollection: IIndemnisation[] = [{ id: 123 }];
          expectedResult = service.addIndemnisationToCollectionIfMissing(indemnisationCollection, undefined, null);
          expect(expectedResult).toEqual(indemnisationCollection);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
