jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { IndemnisationService } from '../service/indemnisation.service';
import { IIndemnisation, Indemnisation } from '../indemnisation.model';

import { IndemnisationUpdateComponent } from './indemnisation-update.component';

describe('Component Tests', () => {
  describe('Indemnisation Management Update Component', () => {
    let comp: IndemnisationUpdateComponent;
    let fixture: ComponentFixture<IndemnisationUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let indemnisationService: IndemnisationService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [IndemnisationUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(IndemnisationUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(IndemnisationUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      indemnisationService = TestBed.inject(IndemnisationService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const indemnisation: IIndemnisation = { id: 456 };

        activatedRoute.data = of({ indemnisation });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(indemnisation));
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Indemnisation>>();
        const indemnisation = { id: 123 };
        jest.spyOn(indemnisationService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ indemnisation });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: indemnisation }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(indemnisationService.update).toHaveBeenCalledWith(indemnisation);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Indemnisation>>();
        const indemnisation = new Indemnisation();
        jest.spyOn(indemnisationService, 'create').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ indemnisation });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: indemnisation }));
        saveSubject.complete();

        // THEN
        expect(indemnisationService.create).toHaveBeenCalledWith(indemnisation);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Indemnisation>>();
        const indemnisation = { id: 123 };
        jest.spyOn(indemnisationService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ indemnisation });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(indemnisationService.update).toHaveBeenCalledWith(indemnisation);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });
  });
});
