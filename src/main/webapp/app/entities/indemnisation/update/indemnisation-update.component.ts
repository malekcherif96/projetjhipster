import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IIndemnisation, Indemnisation } from '../indemnisation.model';
import { IndemnisationService } from '../service/indemnisation.service';

@Component({
  selector: 'jhi-indemnisation-update',
  templateUrl: './indemnisation-update.component.html',
})
export class IndemnisationUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    date: [],
  });

  constructor(protected indemnisationService: IndemnisationService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ indemnisation }) => {
      this.updateForm(indemnisation);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const indemnisation = this.createFromForm();
    if (indemnisation.id !== undefined) {
      this.subscribeToSaveResponse(this.indemnisationService.update(indemnisation));
    } else {
      this.subscribeToSaveResponse(this.indemnisationService.create(indemnisation));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IIndemnisation>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(indemnisation: IIndemnisation): void {
    this.editForm.patchValue({
      id: indemnisation.id,
      date: indemnisation.date,
    });
  }

  protected createFromForm(): IIndemnisation {
    return {
      ...new Indemnisation(),
      id: this.editForm.get(['id'])!.value,
      date: this.editForm.get(['date'])!.value,
    };
  }
}
