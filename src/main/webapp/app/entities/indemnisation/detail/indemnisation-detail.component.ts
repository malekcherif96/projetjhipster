import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IIndemnisation } from '../indemnisation.model';

@Component({
  selector: 'jhi-indemnisation-detail',
  templateUrl: './indemnisation-detail.component.html',
})
export class IndemnisationDetailComponent implements OnInit {
  indemnisation: IIndemnisation | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ indemnisation }) => {
      this.indemnisation = indemnisation;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
