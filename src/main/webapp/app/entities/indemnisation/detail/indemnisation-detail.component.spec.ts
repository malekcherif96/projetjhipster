import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { IndemnisationDetailComponent } from './indemnisation-detail.component';

describe('Component Tests', () => {
  describe('Indemnisation Management Detail Component', () => {
    let comp: IndemnisationDetailComponent;
    let fixture: ComponentFixture<IndemnisationDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [IndemnisationDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ indemnisation: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(IndemnisationDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(IndemnisationDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load indemnisation on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.indemnisation).toEqual(expect.objectContaining({ id: 123 }));
      });
    });
  });
});
