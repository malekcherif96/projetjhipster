import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { IndemnisationComponent } from './list/indemnisation.component';
import { IndemnisationDetailComponent } from './detail/indemnisation-detail.component';
import { IndemnisationUpdateComponent } from './update/indemnisation-update.component';
import { IndemnisationDeleteDialogComponent } from './delete/indemnisation-delete-dialog.component';
import { IndemnisationRoutingModule } from './route/indemnisation-routing.module';

@NgModule({
  imports: [SharedModule, IndemnisationRoutingModule],
  declarations: [IndemnisationComponent, IndemnisationDetailComponent, IndemnisationUpdateComponent, IndemnisationDeleteDialogComponent],
  entryComponents: [IndemnisationDeleteDialogComponent],
})
export class IndemnisationModule {}
