export interface ICheque {
  id?: number;
  nomCheque?: string | null;
  montnant?: number | null;
}

export class Cheque implements ICheque {
  constructor(public id?: number, public nomCheque?: string | null, public montnant?: number | null) {}
}

export function getChequeIdentifier(cheque: ICheque): number | undefined {
  return cheque.id;
}
