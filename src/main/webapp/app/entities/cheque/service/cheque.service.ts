import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { Search } from 'app/core/request/request.model';
import { ICheque, getChequeIdentifier } from '../cheque.model';

export type EntityResponseType = HttpResponse<ICheque>;
export type EntityArrayResponseType = HttpResponse<ICheque[]>;

@Injectable({ providedIn: 'root' })
export class ChequeService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/cheques');
  protected resourceSearchUrl = this.applicationConfigService.getEndpointFor('api/_search/cheques');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(cheque: ICheque): Observable<EntityResponseType> {
    return this.http.post<ICheque>(this.resourceUrl, cheque, { observe: 'response' });
  }

  update(cheque: ICheque): Observable<EntityResponseType> {
    return this.http.put<ICheque>(`${this.resourceUrl}/${getChequeIdentifier(cheque) as number}`, cheque, { observe: 'response' });
  }

  partialUpdate(cheque: ICheque): Observable<EntityResponseType> {
    return this.http.patch<ICheque>(`${this.resourceUrl}/${getChequeIdentifier(cheque) as number}`, cheque, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICheque>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICheque[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICheque[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }

  addChequeToCollectionIfMissing(chequeCollection: ICheque[], ...chequesToCheck: (ICheque | null | undefined)[]): ICheque[] {
    const cheques: ICheque[] = chequesToCheck.filter(isPresent);
    if (cheques.length > 0) {
      const chequeCollectionIdentifiers = chequeCollection.map(chequeItem => getChequeIdentifier(chequeItem)!);
      const chequesToAdd = cheques.filter(chequeItem => {
        const chequeIdentifier = getChequeIdentifier(chequeItem);
        if (chequeIdentifier == null || chequeCollectionIdentifiers.includes(chequeIdentifier)) {
          return false;
        }
        chequeCollectionIdentifiers.push(chequeIdentifier);
        return true;
      });
      return [...chequesToAdd, ...chequeCollection];
    }
    return chequeCollection;
  }
}
