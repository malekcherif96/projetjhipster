import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ICheque, Cheque } from '../cheque.model';

import { ChequeService } from './cheque.service';

describe('Service Tests', () => {
  describe('Cheque Service', () => {
    let service: ChequeService;
    let httpMock: HttpTestingController;
    let elemDefault: ICheque;
    let expectedResult: ICheque | ICheque[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(ChequeService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        nomCheque: 'AAAAAAA',
        montnant: 0,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Cheque', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new Cheque()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Cheque', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            nomCheque: 'BBBBBB',
            montnant: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a Cheque', () => {
        const patchObject = Object.assign(
          {
            nomCheque: 'BBBBBB',
          },
          new Cheque()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Cheque', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            nomCheque: 'BBBBBB',
            montnant: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Cheque', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addChequeToCollectionIfMissing', () => {
        it('should add a Cheque to an empty array', () => {
          const cheque: ICheque = { id: 123 };
          expectedResult = service.addChequeToCollectionIfMissing([], cheque);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(cheque);
        });

        it('should not add a Cheque to an array that contains it', () => {
          const cheque: ICheque = { id: 123 };
          const chequeCollection: ICheque[] = [
            {
              ...cheque,
            },
            { id: 456 },
          ];
          expectedResult = service.addChequeToCollectionIfMissing(chequeCollection, cheque);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a Cheque to an array that doesn't contain it", () => {
          const cheque: ICheque = { id: 123 };
          const chequeCollection: ICheque[] = [{ id: 456 }];
          expectedResult = service.addChequeToCollectionIfMissing(chequeCollection, cheque);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(cheque);
        });

        it('should add only unique Cheque to an array', () => {
          const chequeArray: ICheque[] = [{ id: 123 }, { id: 456 }, { id: 70855 }];
          const chequeCollection: ICheque[] = [{ id: 123 }];
          expectedResult = service.addChequeToCollectionIfMissing(chequeCollection, ...chequeArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const cheque: ICheque = { id: 123 };
          const cheque2: ICheque = { id: 456 };
          expectedResult = service.addChequeToCollectionIfMissing([], cheque, cheque2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(cheque);
          expect(expectedResult).toContain(cheque2);
        });

        it('should accept null and undefined values', () => {
          const cheque: ICheque = { id: 123 };
          expectedResult = service.addChequeToCollectionIfMissing([], null, cheque, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(cheque);
        });

        it('should return initial array if no Cheque is added', () => {
          const chequeCollection: ICheque[] = [{ id: 123 }];
          expectedResult = service.addChequeToCollectionIfMissing(chequeCollection, undefined, null);
          expect(expectedResult).toEqual(chequeCollection);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
