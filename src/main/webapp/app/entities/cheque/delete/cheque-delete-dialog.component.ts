import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ICheque } from '../cheque.model';
import { ChequeService } from '../service/cheque.service';

@Component({
  templateUrl: './cheque-delete-dialog.component.html',
})
export class ChequeDeleteDialogComponent {
  cheque?: ICheque;

  constructor(protected chequeService: ChequeService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.chequeService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
