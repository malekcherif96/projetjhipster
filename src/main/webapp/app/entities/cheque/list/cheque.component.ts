import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICheque } from '../cheque.model';
import { ChequeService } from '../service/cheque.service';
import { ChequeDeleteDialogComponent } from '../delete/cheque-delete-dialog.component';

@Component({
  selector: 'jhi-cheque',
  templateUrl: './cheque.component.html',
})
export class ChequeComponent implements OnInit {
  cheques?: ICheque[];
  isLoading = false;
  currentSearch: string;

  constructor(protected chequeService: ChequeService, protected modalService: NgbModal, protected activatedRoute: ActivatedRoute) {
    this.currentSearch = this.activatedRoute.snapshot.queryParams['search'] ?? '';
  }

  loadAll(): void {
    this.isLoading = true;
    if (this.currentSearch) {
      this.chequeService
        .search({
          query: this.currentSearch,
        })
        .subscribe(
          (res: HttpResponse<ICheque[]>) => {
            this.isLoading = false;
            this.cheques = res.body ?? [];
          },
          () => {
            this.isLoading = false;
          }
        );
      return;
    }

    this.chequeService.query().subscribe(
      (res: HttpResponse<ICheque[]>) => {
        this.isLoading = false;
        this.cheques = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: ICheque): number {
    return item.id!;
  }

  delete(cheque: ICheque): void {
    const modalRef = this.modalService.open(ChequeDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.cheque = cheque;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
