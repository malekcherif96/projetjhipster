jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ChequeService } from '../service/cheque.service';

import { ChequeComponent } from './cheque.component';

describe('Component Tests', () => {
  describe('Cheque Management Component', () => {
    let comp: ChequeComponent;
    let fixture: ComponentFixture<ChequeComponent>;
    let service: ChequeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [ChequeComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { snapshot: { queryParams: {} } },
          },
        ],
      })
        .overrideTemplate(ChequeComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ChequeComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(ChequeService);

      const headers = new HttpHeaders().append('link', 'link;link');
      jest.spyOn(service, 'query').mockReturnValue(
        of(
          new HttpResponse({
            body: [{ id: 123 }],
            headers,
          })
        )
      );
    });

    it('Should call load all on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.cheques?.[0]).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
