import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ChequeComponent } from './list/cheque.component';
import { ChequeDetailComponent } from './detail/cheque-detail.component';
import { ChequeUpdateComponent } from './update/cheque-update.component';
import { ChequeDeleteDialogComponent } from './delete/cheque-delete-dialog.component';
import { ChequeRoutingModule } from './route/cheque-routing.module';

@NgModule({
  imports: [SharedModule, ChequeRoutingModule],
  declarations: [ChequeComponent, ChequeDetailComponent, ChequeUpdateComponent, ChequeDeleteDialogComponent],
  entryComponents: [ChequeDeleteDialogComponent],
})
export class ChequeModule {}
