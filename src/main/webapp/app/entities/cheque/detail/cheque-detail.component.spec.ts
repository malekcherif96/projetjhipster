import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ChequeDetailComponent } from './cheque-detail.component';

describe('Component Tests', () => {
  describe('Cheque Management Detail Component', () => {
    let comp: ChequeDetailComponent;
    let fixture: ComponentFixture<ChequeDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [ChequeDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ cheque: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(ChequeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ChequeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load cheque on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.cheque).toEqual(expect.objectContaining({ id: 123 }));
      });
    });
  });
});
