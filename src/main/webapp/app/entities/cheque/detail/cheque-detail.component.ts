import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICheque } from '../cheque.model';

@Component({
  selector: 'jhi-cheque-detail',
  templateUrl: './cheque-detail.component.html',
})
export class ChequeDetailComponent implements OnInit {
  cheque: ICheque | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cheque }) => {
      this.cheque = cheque;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
