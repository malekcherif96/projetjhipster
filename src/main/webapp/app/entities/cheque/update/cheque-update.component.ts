import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { ICheque, Cheque } from '../cheque.model';
import { ChequeService } from '../service/cheque.service';

@Component({
  selector: 'jhi-cheque-update',
  templateUrl: './cheque-update.component.html',
})
export class ChequeUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    nomCheque: [],
    montnant: [],
  });

  constructor(protected chequeService: ChequeService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cheque }) => {
      this.updateForm(cheque);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const cheque = this.createFromForm();
    if (cheque.id !== undefined) {
      this.subscribeToSaveResponse(this.chequeService.update(cheque));
    } else {
      this.subscribeToSaveResponse(this.chequeService.create(cheque));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICheque>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(cheque: ICheque): void {
    this.editForm.patchValue({
      id: cheque.id,
      nomCheque: cheque.nomCheque,
      montnant: cheque.montnant,
    });
  }

  protected createFromForm(): ICheque {
    return {
      ...new Cheque(),
      id: this.editForm.get(['id'])!.value,
      nomCheque: this.editForm.get(['nomCheque'])!.value,
      montnant: this.editForm.get(['montnant'])!.value,
    };
  }
}
