jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { ChequeService } from '../service/cheque.service';
import { ICheque, Cheque } from '../cheque.model';

import { ChequeUpdateComponent } from './cheque-update.component';

describe('Component Tests', () => {
  describe('Cheque Management Update Component', () => {
    let comp: ChequeUpdateComponent;
    let fixture: ComponentFixture<ChequeUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let chequeService: ChequeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [ChequeUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(ChequeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ChequeUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      chequeService = TestBed.inject(ChequeService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const cheque: ICheque = { id: 456 };

        activatedRoute.data = of({ cheque });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(cheque));
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Cheque>>();
        const cheque = { id: 123 };
        jest.spyOn(chequeService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ cheque });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: cheque }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(chequeService.update).toHaveBeenCalledWith(cheque);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Cheque>>();
        const cheque = new Cheque();
        jest.spyOn(chequeService, 'create').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ cheque });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: cheque }));
        saveSubject.complete();

        // THEN
        expect(chequeService.create).toHaveBeenCalledWith(cheque);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Cheque>>();
        const cheque = { id: 123 };
        jest.spyOn(chequeService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ cheque });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(chequeService.update).toHaveBeenCalledWith(cheque);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });
  });
});
