import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ChequeComponent } from '../list/cheque.component';
import { ChequeDetailComponent } from '../detail/cheque-detail.component';
import { ChequeUpdateComponent } from '../update/cheque-update.component';
import { ChequeRoutingResolveService } from './cheque-routing-resolve.service';

const chequeRoute: Routes = [
  {
    path: '',
    component: ChequeComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ChequeDetailComponent,
    resolve: {
      cheque: ChequeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ChequeUpdateComponent,
    resolve: {
      cheque: ChequeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ChequeUpdateComponent,
    resolve: {
      cheque: ChequeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(chequeRoute)],
  exports: [RouterModule],
})
export class ChequeRoutingModule {}
