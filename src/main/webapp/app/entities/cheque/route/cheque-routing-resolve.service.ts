import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ICheque, Cheque } from '../cheque.model';
import { ChequeService } from '../service/cheque.service';

@Injectable({ providedIn: 'root' })
export class ChequeRoutingResolveService implements Resolve<ICheque> {
  constructor(protected service: ChequeService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICheque> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((cheque: HttpResponse<Cheque>) => {
          if (cheque.body) {
            return of(cheque.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Cheque());
  }
}
