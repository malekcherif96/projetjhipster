export interface IDocument {
  id?: number;
  libelle?: string | null;
  type?: string | null;
}

export class Document implements IDocument {
  constructor(public id?: number, public libelle?: string | null, public type?: string | null) {}
}

export function getDocumentIdentifier(document: IDocument): number | undefined {
  return document.id;
}
