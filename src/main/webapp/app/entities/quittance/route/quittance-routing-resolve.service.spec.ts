jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IQuittance, Quittance } from '../quittance.model';
import { QuittanceService } from '../service/quittance.service';

import { QuittanceRoutingResolveService } from './quittance-routing-resolve.service';

describe('Service Tests', () => {
  describe('Quittance routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: QuittanceRoutingResolveService;
    let service: QuittanceService;
    let resultQuittance: IQuittance | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(QuittanceRoutingResolveService);
      service = TestBed.inject(QuittanceService);
      resultQuittance = undefined;
    });

    describe('resolve', () => {
      it('should return IQuittance returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultQuittance = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultQuittance).toEqual({ id: 123 });
      });

      it('should return new IQuittance if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultQuittance = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultQuittance).toEqual(new Quittance());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as Quittance })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultQuittance = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultQuittance).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
