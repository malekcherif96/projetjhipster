import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { QuittanceComponent } from '../list/quittance.component';
import { QuittanceDetailComponent } from '../detail/quittance-detail.component';
import { QuittanceUpdateComponent } from '../update/quittance-update.component';
import { QuittanceRoutingResolveService } from './quittance-routing-resolve.service';

const quittanceRoute: Routes = [
  {
    path: '',
    component: QuittanceComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: QuittanceDetailComponent,
    resolve: {
      quittance: QuittanceRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: QuittanceUpdateComponent,
    resolve: {
      quittance: QuittanceRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: QuittanceUpdateComponent,
    resolve: {
      quittance: QuittanceRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(quittanceRoute)],
  exports: [RouterModule],
})
export class QuittanceRoutingModule {}
