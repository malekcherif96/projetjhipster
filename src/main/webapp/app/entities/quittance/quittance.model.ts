export interface IQuittance {
  id?: number;
  numQuittance?: string | null;
}

export class Quittance implements IQuittance {
  constructor(public id?: number, public numQuittance?: string | null) {}
}

export function getQuittanceIdentifier(quittance: IQuittance): number | undefined {
  return quittance.id;
}
