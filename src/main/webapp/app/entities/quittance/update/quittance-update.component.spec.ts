jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { QuittanceService } from '../service/quittance.service';
import { IQuittance, Quittance } from '../quittance.model';

import { QuittanceUpdateComponent } from './quittance-update.component';

describe('Component Tests', () => {
  describe('Quittance Management Update Component', () => {
    let comp: QuittanceUpdateComponent;
    let fixture: ComponentFixture<QuittanceUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let quittanceService: QuittanceService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [QuittanceUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(QuittanceUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(QuittanceUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      quittanceService = TestBed.inject(QuittanceService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const quittance: IQuittance = { id: 456 };

        activatedRoute.data = of({ quittance });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(quittance));
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Quittance>>();
        const quittance = { id: 123 };
        jest.spyOn(quittanceService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ quittance });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: quittance }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(quittanceService.update).toHaveBeenCalledWith(quittance);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Quittance>>();
        const quittance = new Quittance();
        jest.spyOn(quittanceService, 'create').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ quittance });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: quittance }));
        saveSubject.complete();

        // THEN
        expect(quittanceService.create).toHaveBeenCalledWith(quittance);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Quittance>>();
        const quittance = { id: 123 };
        jest.spyOn(quittanceService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ quittance });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(quittanceService.update).toHaveBeenCalledWith(quittance);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });
  });
});
