import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IQuittance, Quittance } from '../quittance.model';
import { QuittanceService } from '../service/quittance.service';

@Component({
  selector: 'jhi-quittance-update',
  templateUrl: './quittance-update.component.html',
})
export class QuittanceUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    numQuittance: [],
  });

  constructor(protected quittanceService: QuittanceService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ quittance }) => {
      this.updateForm(quittance);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const quittance = this.createFromForm();
    if (quittance.id !== undefined) {
      this.subscribeToSaveResponse(this.quittanceService.update(quittance));
    } else {
      this.subscribeToSaveResponse(this.quittanceService.create(quittance));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IQuittance>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(quittance: IQuittance): void {
    this.editForm.patchValue({
      id: quittance.id,
      numQuittance: quittance.numQuittance,
    });
  }

  protected createFromForm(): IQuittance {
    return {
      ...new Quittance(),
      id: this.editForm.get(['id'])!.value,
      numQuittance: this.editForm.get(['numQuittance'])!.value,
    };
  }
}
