import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { QuittanceComponent } from './list/quittance.component';
import { QuittanceDetailComponent } from './detail/quittance-detail.component';
import { QuittanceUpdateComponent } from './update/quittance-update.component';
import { QuittanceDeleteDialogComponent } from './delete/quittance-delete-dialog.component';
import { QuittanceRoutingModule } from './route/quittance-routing.module';

@NgModule({
  imports: [SharedModule, QuittanceRoutingModule],
  declarations: [QuittanceComponent, QuittanceDetailComponent, QuittanceUpdateComponent, QuittanceDeleteDialogComponent],
  entryComponents: [QuittanceDeleteDialogComponent],
})
export class QuittanceModule {}
