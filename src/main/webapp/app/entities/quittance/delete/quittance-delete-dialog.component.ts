import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IQuittance } from '../quittance.model';
import { QuittanceService } from '../service/quittance.service';

@Component({
  templateUrl: './quittance-delete-dialog.component.html',
})
export class QuittanceDeleteDialogComponent {
  quittance?: IQuittance;

  constructor(protected quittanceService: QuittanceService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.quittanceService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
