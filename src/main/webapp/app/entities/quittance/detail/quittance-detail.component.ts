import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IQuittance } from '../quittance.model';

@Component({
  selector: 'jhi-quittance-detail',
  templateUrl: './quittance-detail.component.html',
})
export class QuittanceDetailComponent implements OnInit {
  quittance: IQuittance | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ quittance }) => {
      this.quittance = quittance;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
