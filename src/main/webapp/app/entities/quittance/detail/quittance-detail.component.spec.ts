import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { QuittanceDetailComponent } from './quittance-detail.component';

describe('Component Tests', () => {
  describe('Quittance Management Detail Component', () => {
    let comp: QuittanceDetailComponent;
    let fixture: ComponentFixture<QuittanceDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [QuittanceDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ quittance: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(QuittanceDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(QuittanceDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load quittance on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.quittance).toEqual(expect.objectContaining({ id: 123 }));
      });
    });
  });
});
