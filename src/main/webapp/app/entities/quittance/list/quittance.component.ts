import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IQuittance } from '../quittance.model';
import { QuittanceService } from '../service/quittance.service';
import { QuittanceDeleteDialogComponent } from '../delete/quittance-delete-dialog.component';

@Component({
  selector: 'jhi-quittance',
  templateUrl: './quittance.component.html',
})
export class QuittanceComponent implements OnInit {
  quittances?: IQuittance[];
  isLoading = false;
  currentSearch: string;

  constructor(protected quittanceService: QuittanceService, protected modalService: NgbModal, protected activatedRoute: ActivatedRoute) {
    this.currentSearch = this.activatedRoute.snapshot.queryParams['search'] ?? '';
  }

  loadAll(): void {
    this.isLoading = true;
    if (this.currentSearch) {
      this.quittanceService
        .search({
          query: this.currentSearch,
        })
        .subscribe(
          (res: HttpResponse<IQuittance[]>) => {
            this.isLoading = false;
            this.quittances = res.body ?? [];
          },
          () => {
            this.isLoading = false;
          }
        );
      return;
    }

    this.quittanceService.query().subscribe(
      (res: HttpResponse<IQuittance[]>) => {
        this.isLoading = false;
        this.quittances = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IQuittance): number {
    return item.id!;
  }

  delete(quittance: IQuittance): void {
    const modalRef = this.modalService.open(QuittanceDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.quittance = quittance;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
