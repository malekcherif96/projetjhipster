jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { QuittanceService } from '../service/quittance.service';

import { QuittanceComponent } from './quittance.component';

describe('Component Tests', () => {
  describe('Quittance Management Component', () => {
    let comp: QuittanceComponent;
    let fixture: ComponentFixture<QuittanceComponent>;
    let service: QuittanceService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [QuittanceComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { snapshot: { queryParams: {} } },
          },
        ],
      })
        .overrideTemplate(QuittanceComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(QuittanceComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(QuittanceService);

      const headers = new HttpHeaders().append('link', 'link;link');
      jest.spyOn(service, 'query').mockReturnValue(
        of(
          new HttpResponse({
            body: [{ id: 123 }],
            headers,
          })
        )
      );
    });

    it('Should call load all on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.quittances?.[0]).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
