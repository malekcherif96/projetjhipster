import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { Search } from 'app/core/request/request.model';
import { IQuittance, getQuittanceIdentifier } from '../quittance.model';

export type EntityResponseType = HttpResponse<IQuittance>;
export type EntityArrayResponseType = HttpResponse<IQuittance[]>;

@Injectable({ providedIn: 'root' })
export class QuittanceService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/quittances');
  protected resourceSearchUrl = this.applicationConfigService.getEndpointFor('api/_search/quittances');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(quittance: IQuittance): Observable<EntityResponseType> {
    return this.http.post<IQuittance>(this.resourceUrl, quittance, { observe: 'response' });
  }

  update(quittance: IQuittance): Observable<EntityResponseType> {
    return this.http.put<IQuittance>(`${this.resourceUrl}/${getQuittanceIdentifier(quittance) as number}`, quittance, {
      observe: 'response',
    });
  }

  partialUpdate(quittance: IQuittance): Observable<EntityResponseType> {
    return this.http.patch<IQuittance>(`${this.resourceUrl}/${getQuittanceIdentifier(quittance) as number}`, quittance, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IQuittance>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IQuittance[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IQuittance[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }

  addQuittanceToCollectionIfMissing(
    quittanceCollection: IQuittance[],
    ...quittancesToCheck: (IQuittance | null | undefined)[]
  ): IQuittance[] {
    const quittances: IQuittance[] = quittancesToCheck.filter(isPresent);
    if (quittances.length > 0) {
      const quittanceCollectionIdentifiers = quittanceCollection.map(quittanceItem => getQuittanceIdentifier(quittanceItem)!);
      const quittancesToAdd = quittances.filter(quittanceItem => {
        const quittanceIdentifier = getQuittanceIdentifier(quittanceItem);
        if (quittanceIdentifier == null || quittanceCollectionIdentifiers.includes(quittanceIdentifier)) {
          return false;
        }
        quittanceCollectionIdentifiers.push(quittanceIdentifier);
        return true;
      });
      return [...quittancesToAdd, ...quittanceCollection];
    }
    return quittanceCollection;
  }
}
