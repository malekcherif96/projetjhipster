import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IQuittance, Quittance } from '../quittance.model';

import { QuittanceService } from './quittance.service';

describe('Service Tests', () => {
  describe('Quittance Service', () => {
    let service: QuittanceService;
    let httpMock: HttpTestingController;
    let elemDefault: IQuittance;
    let expectedResult: IQuittance | IQuittance[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(QuittanceService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        numQuittance: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Quittance', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new Quittance()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Quittance', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            numQuittance: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a Quittance', () => {
        const patchObject = Object.assign({}, new Quittance());

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Quittance', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            numQuittance: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Quittance', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addQuittanceToCollectionIfMissing', () => {
        it('should add a Quittance to an empty array', () => {
          const quittance: IQuittance = { id: 123 };
          expectedResult = service.addQuittanceToCollectionIfMissing([], quittance);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(quittance);
        });

        it('should not add a Quittance to an array that contains it', () => {
          const quittance: IQuittance = { id: 123 };
          const quittanceCollection: IQuittance[] = [
            {
              ...quittance,
            },
            { id: 456 },
          ];
          expectedResult = service.addQuittanceToCollectionIfMissing(quittanceCollection, quittance);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a Quittance to an array that doesn't contain it", () => {
          const quittance: IQuittance = { id: 123 };
          const quittanceCollection: IQuittance[] = [{ id: 456 }];
          expectedResult = service.addQuittanceToCollectionIfMissing(quittanceCollection, quittance);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(quittance);
        });

        it('should add only unique Quittance to an array', () => {
          const quittanceArray: IQuittance[] = [{ id: 123 }, { id: 456 }, { id: 98861 }];
          const quittanceCollection: IQuittance[] = [{ id: 123 }];
          expectedResult = service.addQuittanceToCollectionIfMissing(quittanceCollection, ...quittanceArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const quittance: IQuittance = { id: 123 };
          const quittance2: IQuittance = { id: 456 };
          expectedResult = service.addQuittanceToCollectionIfMissing([], quittance, quittance2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(quittance);
          expect(expectedResult).toContain(quittance2);
        });

        it('should accept null and undefined values', () => {
          const quittance: IQuittance = { id: 123 };
          expectedResult = service.addQuittanceToCollectionIfMissing([], null, quittance, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(quittance);
        });

        it('should return initial array if no Quittance is added', () => {
          const quittanceCollection: IQuittance[] = [{ id: 123 }];
          expectedResult = service.addQuittanceToCollectionIfMissing(quittanceCollection, undefined, null);
          expect(expectedResult).toEqual(quittanceCollection);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
