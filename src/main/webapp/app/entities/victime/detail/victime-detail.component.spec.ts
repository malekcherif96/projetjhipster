import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { VictimeDetailComponent } from './victime-detail.component';

describe('Component Tests', () => {
  describe('Victime Management Detail Component', () => {
    let comp: VictimeDetailComponent;
    let fixture: ComponentFixture<VictimeDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [VictimeDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ victime: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(VictimeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(VictimeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load victime on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.victime).toEqual(expect.objectContaining({ id: 123 }));
      });
    });
  });
});
