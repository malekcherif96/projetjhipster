import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IVictime } from '../victime.model';

@Component({
  selector: 'jhi-victime-detail',
  templateUrl: './victime-detail.component.html',
})
export class VictimeDetailComponent implements OnInit {
  victime: IVictime | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ victime }) => {
      this.victime = victime;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
