import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IVictime, Victime } from '../victime.model';
import { VictimeService } from '../service/victime.service';

@Injectable({ providedIn: 'root' })
export class VictimeRoutingResolveService implements Resolve<IVictime> {
  constructor(protected service: VictimeService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IVictime> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((victime: HttpResponse<Victime>) => {
          if (victime.body) {
            return of(victime.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Victime());
  }
}
