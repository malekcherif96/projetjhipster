jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IVictime, Victime } from '../victime.model';
import { VictimeService } from '../service/victime.service';

import { VictimeRoutingResolveService } from './victime-routing-resolve.service';

describe('Service Tests', () => {
  describe('Victime routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: VictimeRoutingResolveService;
    let service: VictimeService;
    let resultVictime: IVictime | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(VictimeRoutingResolveService);
      service = TestBed.inject(VictimeService);
      resultVictime = undefined;
    });

    describe('resolve', () => {
      it('should return IVictime returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultVictime = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultVictime).toEqual({ id: 123 });
      });

      it('should return new IVictime if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultVictime = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultVictime).toEqual(new Victime());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as Victime })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultVictime = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultVictime).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
