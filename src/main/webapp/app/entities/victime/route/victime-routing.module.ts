import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { VictimeComponent } from '../list/victime.component';
import { VictimeDetailComponent } from '../detail/victime-detail.component';
import { VictimeUpdateComponent } from '../update/victime-update.component';
import { VictimeRoutingResolveService } from './victime-routing-resolve.service';

const victimeRoute: Routes = [
  {
    path: '',
    component: VictimeComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: VictimeDetailComponent,
    resolve: {
      victime: VictimeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: VictimeUpdateComponent,
    resolve: {
      victime: VictimeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: VictimeUpdateComponent,
    resolve: {
      victime: VictimeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(victimeRoute)],
  exports: [RouterModule],
})
export class VictimeRoutingModule {}
