import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { VictimeComponent } from './list/victime.component';
import { VictimeDetailComponent } from './detail/victime-detail.component';
import { VictimeUpdateComponent } from './update/victime-update.component';
import { VictimeDeleteDialogComponent } from './delete/victime-delete-dialog.component';
import { VictimeRoutingModule } from './route/victime-routing.module';

@NgModule({
  imports: [SharedModule, VictimeRoutingModule],
  declarations: [VictimeComponent, VictimeDetailComponent, VictimeUpdateComponent, VictimeDeleteDialogComponent],
  entryComponents: [VictimeDeleteDialogComponent],
})
export class VictimeModule {}
