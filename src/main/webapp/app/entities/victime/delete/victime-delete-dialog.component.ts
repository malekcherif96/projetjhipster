import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IVictime } from '../victime.model';
import { VictimeService } from '../service/victime.service';

@Component({
  templateUrl: './victime-delete-dialog.component.html',
})
export class VictimeDeleteDialogComponent {
  victime?: IVictime;

  constructor(protected victimeService: VictimeService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.victimeService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
