export interface IVictime {
  id?: number;
  numContrat?: string | null;
  cin?: number | null;
  situationSocial?: string | null;
  numeroTelephone?: number | null;
  nom?: string | null;
  prenom?: string | null;
  login?: string | null;
  password?: string | null;
}

export class Victime implements IVictime {
  constructor(
    public id?: number,
    public numContrat?: string | null,
    public cin?: number | null,
    public situationSocial?: string | null,
    public numeroTelephone?: number | null,
    public nom?: string | null,
    public prenom?: string | null,
    public login?: string | null,
    public password?: string | null
  ) {}
}

export function getVictimeIdentifier(victime: IVictime): number | undefined {
  return victime.id;
}
