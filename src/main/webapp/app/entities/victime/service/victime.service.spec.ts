import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IVictime, Victime } from '../victime.model';

import { VictimeService } from './victime.service';

describe('Service Tests', () => {
  describe('Victime Service', () => {
    let service: VictimeService;
    let httpMock: HttpTestingController;
    let elemDefault: IVictime;
    let expectedResult: IVictime | IVictime[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(VictimeService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        numContrat: 'AAAAAAA',
        cin: 0,
        situationSocial: 'AAAAAAA',
        numeroTelephone: 0,
        nom: 'AAAAAAA',
        prenom: 'AAAAAAA',
        login: 'AAAAAAA',
        password: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Victime', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new Victime()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Victime', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            numContrat: 'BBBBBB',
            cin: 1,
            situationSocial: 'BBBBBB',
            numeroTelephone: 1,
            nom: 'BBBBBB',
            prenom: 'BBBBBB',
            login: 'BBBBBB',
            password: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a Victime', () => {
        const patchObject = Object.assign(
          {
            numContrat: 'BBBBBB',
            situationSocial: 'BBBBBB',
            numeroTelephone: 1,
            prenom: 'BBBBBB',
            login: 'BBBBBB',
          },
          new Victime()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Victime', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            numContrat: 'BBBBBB',
            cin: 1,
            situationSocial: 'BBBBBB',
            numeroTelephone: 1,
            nom: 'BBBBBB',
            prenom: 'BBBBBB',
            login: 'BBBBBB',
            password: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Victime', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addVictimeToCollectionIfMissing', () => {
        it('should add a Victime to an empty array', () => {
          const victime: IVictime = { id: 123 };
          expectedResult = service.addVictimeToCollectionIfMissing([], victime);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(victime);
        });

        it('should not add a Victime to an array that contains it', () => {
          const victime: IVictime = { id: 123 };
          const victimeCollection: IVictime[] = [
            {
              ...victime,
            },
            { id: 456 },
          ];
          expectedResult = service.addVictimeToCollectionIfMissing(victimeCollection, victime);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a Victime to an array that doesn't contain it", () => {
          const victime: IVictime = { id: 123 };
          const victimeCollection: IVictime[] = [{ id: 456 }];
          expectedResult = service.addVictimeToCollectionIfMissing(victimeCollection, victime);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(victime);
        });

        it('should add only unique Victime to an array', () => {
          const victimeArray: IVictime[] = [{ id: 123 }, { id: 456 }, { id: 21189 }];
          const victimeCollection: IVictime[] = [{ id: 123 }];
          expectedResult = service.addVictimeToCollectionIfMissing(victimeCollection, ...victimeArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const victime: IVictime = { id: 123 };
          const victime2: IVictime = { id: 456 };
          expectedResult = service.addVictimeToCollectionIfMissing([], victime, victime2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(victime);
          expect(expectedResult).toContain(victime2);
        });

        it('should accept null and undefined values', () => {
          const victime: IVictime = { id: 123 };
          expectedResult = service.addVictimeToCollectionIfMissing([], null, victime, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(victime);
        });

        it('should return initial array if no Victime is added', () => {
          const victimeCollection: IVictime[] = [{ id: 123 }];
          expectedResult = service.addVictimeToCollectionIfMissing(victimeCollection, undefined, null);
          expect(expectedResult).toEqual(victimeCollection);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
