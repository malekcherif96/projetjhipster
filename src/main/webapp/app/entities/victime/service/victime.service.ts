import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { Search } from 'app/core/request/request.model';
import { IVictime, getVictimeIdentifier } from '../victime.model';

export type EntityResponseType = HttpResponse<IVictime>;
export type EntityArrayResponseType = HttpResponse<IVictime[]>;

@Injectable({ providedIn: 'root' })
export class VictimeService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/victimes');
  protected resourceSearchUrl = this.applicationConfigService.getEndpointFor('api/_search/victimes');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(victime: IVictime): Observable<EntityResponseType> {
    return this.http.post<IVictime>(this.resourceUrl, victime, { observe: 'response' });
  }

  update(victime: IVictime): Observable<EntityResponseType> {
    return this.http.put<IVictime>(`${this.resourceUrl}/${getVictimeIdentifier(victime) as number}`, victime, { observe: 'response' });
  }

  partialUpdate(victime: IVictime): Observable<EntityResponseType> {
    return this.http.patch<IVictime>(`${this.resourceUrl}/${getVictimeIdentifier(victime) as number}`, victime, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IVictime>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IVictime[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IVictime[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }

  addVictimeToCollectionIfMissing(victimeCollection: IVictime[], ...victimesToCheck: (IVictime | null | undefined)[]): IVictime[] {
    const victimes: IVictime[] = victimesToCheck.filter(isPresent);
    if (victimes.length > 0) {
      const victimeCollectionIdentifiers = victimeCollection.map(victimeItem => getVictimeIdentifier(victimeItem)!);
      const victimesToAdd = victimes.filter(victimeItem => {
        const victimeIdentifier = getVictimeIdentifier(victimeItem);
        if (victimeIdentifier == null || victimeCollectionIdentifiers.includes(victimeIdentifier)) {
          return false;
        }
        victimeCollectionIdentifiers.push(victimeIdentifier);
        return true;
      });
      return [...victimesToAdd, ...victimeCollection];
    }
    return victimeCollection;
  }
}
