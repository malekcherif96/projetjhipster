import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IVictime } from '../victime.model';
import { VictimeService } from '../service/victime.service';
import { VictimeDeleteDialogComponent } from '../delete/victime-delete-dialog.component';

@Component({
  selector: 'jhi-victime',
  templateUrl: './victime.component.html',
})
export class VictimeComponent implements OnInit {
  victimes?: IVictime[];
  isLoading = false;
  currentSearch: string;

  constructor(protected victimeService: VictimeService, protected modalService: NgbModal, protected activatedRoute: ActivatedRoute) {
    this.currentSearch = this.activatedRoute.snapshot.queryParams['search'] ?? '';
  }

  loadAll(): void {
    this.isLoading = true;
    if (this.currentSearch) {
      this.victimeService
        .search({
          query: this.currentSearch,
        })
        .subscribe(
          (res: HttpResponse<IVictime[]>) => {
            this.isLoading = false;
            this.victimes = res.body ?? [];
          },
          () => {
            this.isLoading = false;
          }
        );
      return;
    }

    this.victimeService.query().subscribe(
      (res: HttpResponse<IVictime[]>) => {
        this.isLoading = false;
        this.victimes = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IVictime): number {
    return item.id!;
  }

  delete(victime: IVictime): void {
    const modalRef = this.modalService.open(VictimeDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.victime = victime;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
