jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { VictimeService } from '../service/victime.service';

import { VictimeComponent } from './victime.component';

describe('Component Tests', () => {
  describe('Victime Management Component', () => {
    let comp: VictimeComponent;
    let fixture: ComponentFixture<VictimeComponent>;
    let service: VictimeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [VictimeComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { snapshot: { queryParams: {} } },
          },
        ],
      })
        .overrideTemplate(VictimeComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(VictimeComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(VictimeService);

      const headers = new HttpHeaders().append('link', 'link;link');
      jest.spyOn(service, 'query').mockReturnValue(
        of(
          new HttpResponse({
            body: [{ id: 123 }],
            headers,
          })
        )
      );
    });

    it('Should call load all on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.victimes?.[0]).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
