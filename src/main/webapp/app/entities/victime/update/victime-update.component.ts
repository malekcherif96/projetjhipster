import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IVictime, Victime } from '../victime.model';
import { VictimeService } from '../service/victime.service';

@Component({
  selector: 'jhi-victime-update',
  templateUrl: './victime-update.component.html',
})
export class VictimeUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    numContrat: [],
    cin: [],
    situationSocial: [],
    numeroTelephone: [],
    nom: [],
    prenom: [],
    login: [],
    password: [],
  });

  constructor(protected victimeService: VictimeService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ victime }) => {
      this.updateForm(victime);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const victime = this.createFromForm();
    if (victime.id !== undefined) {
      this.subscribeToSaveResponse(this.victimeService.update(victime));
    } else {
      this.subscribeToSaveResponse(this.victimeService.create(victime));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IVictime>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(victime: IVictime): void {
    this.editForm.patchValue({
      id: victime.id,
      numContrat: victime.numContrat,
      cin: victime.cin,
      situationSocial: victime.situationSocial,
      numeroTelephone: victime.numeroTelephone,
      nom: victime.nom,
      prenom: victime.prenom,
      login: victime.login,
      password: victime.password,
    });
  }

  protected createFromForm(): IVictime {
    return {
      ...new Victime(),
      id: this.editForm.get(['id'])!.value,
      numContrat: this.editForm.get(['numContrat'])!.value,
      cin: this.editForm.get(['cin'])!.value,
      situationSocial: this.editForm.get(['situationSocial'])!.value,
      numeroTelephone: this.editForm.get(['numeroTelephone'])!.value,
      nom: this.editForm.get(['nom'])!.value,
      prenom: this.editForm.get(['prenom'])!.value,
      login: this.editForm.get(['login'])!.value,
      password: this.editForm.get(['password'])!.value,
    };
  }
}
