jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { VictimeService } from '../service/victime.service';
import { IVictime, Victime } from '../victime.model';

import { VictimeUpdateComponent } from './victime-update.component';

describe('Component Tests', () => {
  describe('Victime Management Update Component', () => {
    let comp: VictimeUpdateComponent;
    let fixture: ComponentFixture<VictimeUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let victimeService: VictimeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [VictimeUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(VictimeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(VictimeUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      victimeService = TestBed.inject(VictimeService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const victime: IVictime = { id: 456 };

        activatedRoute.data = of({ victime });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(victime));
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Victime>>();
        const victime = { id: 123 };
        jest.spyOn(victimeService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ victime });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: victime }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(victimeService.update).toHaveBeenCalledWith(victime);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Victime>>();
        const victime = new Victime();
        jest.spyOn(victimeService, 'create').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ victime });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: victime }));
        saveSubject.complete();

        // THEN
        expect(victimeService.create).toHaveBeenCalledWith(victime);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Victime>>();
        const victime = { id: 123 };
        jest.spyOn(victimeService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ victime });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(victimeService.update).toHaveBeenCalledWith(victime);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });
  });
});
