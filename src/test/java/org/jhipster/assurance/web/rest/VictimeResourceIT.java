package org.jhipster.assurance.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.jhipster.assurance.IntegrationTest;
import org.jhipster.assurance.domain.Victime;
import org.jhipster.assurance.repository.VictimeRepository;
import org.jhipster.assurance.repository.search.VictimeSearchRepository;
import org.jhipster.assurance.service.EntityManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Integration tests for the {@link VictimeResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureWebTestClient
@WithMockUser
class VictimeResourceIT {

    private static final String DEFAULT_NUM_CONTRAT = "AAAAAAAAAA";
    private static final String UPDATED_NUM_CONTRAT = "BBBBBBBBBB";

    private static final Integer DEFAULT_CIN = 1;
    private static final Integer UPDATED_CIN = 2;

    private static final String DEFAULT_SITUATION_SOCIAL = "AAAAAAAAAA";
    private static final String UPDATED_SITUATION_SOCIAL = "BBBBBBBBBB";

    private static final Integer DEFAULT_NUMERO_TELEPHONE = 1;
    private static final Integer UPDATED_NUMERO_TELEPHONE = 2;

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_PRENOM = "AAAAAAAAAA";
    private static final String UPDATED_PRENOM = "BBBBBBBBBB";

    private static final String DEFAULT_LOGIN = "AAAAAAAAAA";
    private static final String UPDATED_LOGIN = "BBBBBBBBBB";

    private static final String DEFAULT_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/victimes";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_SEARCH_API_URL = "/api/_search/victimes";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private VictimeRepository victimeRepository;

    /**
     * This repository is mocked in the org.jhipster.assurance.repository.search test package.
     *
     * @see org.jhipster.assurance.repository.search.VictimeSearchRepositoryMockConfiguration
     */
    @Autowired
    private VictimeSearchRepository mockVictimeSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Victime victime;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Victime createEntity(EntityManager em) {
        Victime victime = new Victime()
            .numContrat(DEFAULT_NUM_CONTRAT)
            .cin(DEFAULT_CIN)
            .situationSocial(DEFAULT_SITUATION_SOCIAL)
            .numeroTelephone(DEFAULT_NUMERO_TELEPHONE)
            .nom(DEFAULT_NOM)
            .prenom(DEFAULT_PRENOM)
            .login(DEFAULT_LOGIN)
            .password(DEFAULT_PASSWORD);
        return victime;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Victime createUpdatedEntity(EntityManager em) {
        Victime victime = new Victime()
            .numContrat(UPDATED_NUM_CONTRAT)
            .cin(UPDATED_CIN)
            .situationSocial(UPDATED_SITUATION_SOCIAL)
            .numeroTelephone(UPDATED_NUMERO_TELEPHONE)
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM)
            .login(UPDATED_LOGIN)
            .password(UPDATED_PASSWORD);
        return victime;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Victime.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        victime = createEntity(em);
    }

    @Test
    void createVictime() throws Exception {
        int databaseSizeBeforeCreate = victimeRepository.findAll().collectList().block().size();
        // Configure the mock search repository
        when(mockVictimeSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Create the Victime
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(victime))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Victime in the database
        List<Victime> victimeList = victimeRepository.findAll().collectList().block();
        assertThat(victimeList).hasSize(databaseSizeBeforeCreate + 1);
        Victime testVictime = victimeList.get(victimeList.size() - 1);
        assertThat(testVictime.getNumContrat()).isEqualTo(DEFAULT_NUM_CONTRAT);
        assertThat(testVictime.getCin()).isEqualTo(DEFAULT_CIN);
        assertThat(testVictime.getSituationSocial()).isEqualTo(DEFAULT_SITUATION_SOCIAL);
        assertThat(testVictime.getNumeroTelephone()).isEqualTo(DEFAULT_NUMERO_TELEPHONE);
        assertThat(testVictime.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testVictime.getPrenom()).isEqualTo(DEFAULT_PRENOM);
        assertThat(testVictime.getLogin()).isEqualTo(DEFAULT_LOGIN);
        assertThat(testVictime.getPassword()).isEqualTo(DEFAULT_PASSWORD);

        // Validate the Victime in Elasticsearch
        verify(mockVictimeSearchRepository, times(1)).save(testVictime);
    }

    @Test
    void createVictimeWithExistingId() throws Exception {
        // Create the Victime with an existing ID
        victime.setId(1L);

        int databaseSizeBeforeCreate = victimeRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(victime))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Victime in the database
        List<Victime> victimeList = victimeRepository.findAll().collectList().block();
        assertThat(victimeList).hasSize(databaseSizeBeforeCreate);

        // Validate the Victime in Elasticsearch
        verify(mockVictimeSearchRepository, times(0)).save(victime);
    }

    @Test
    void getAllVictimesAsStream() {
        // Initialize the database
        victimeRepository.save(victime).block();

        List<Victime> victimeList = webTestClient
            .get()
            .uri(ENTITY_API_URL)
            .accept(MediaType.APPLICATION_NDJSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentTypeCompatibleWith(MediaType.APPLICATION_NDJSON)
            .returnResult(Victime.class)
            .getResponseBody()
            .filter(victime::equals)
            .collectList()
            .block(Duration.ofSeconds(5));

        assertThat(victimeList).isNotNull();
        assertThat(victimeList).hasSize(1);
        Victime testVictime = victimeList.get(0);
        assertThat(testVictime.getNumContrat()).isEqualTo(DEFAULT_NUM_CONTRAT);
        assertThat(testVictime.getCin()).isEqualTo(DEFAULT_CIN);
        assertThat(testVictime.getSituationSocial()).isEqualTo(DEFAULT_SITUATION_SOCIAL);
        assertThat(testVictime.getNumeroTelephone()).isEqualTo(DEFAULT_NUMERO_TELEPHONE);
        assertThat(testVictime.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testVictime.getPrenom()).isEqualTo(DEFAULT_PRENOM);
        assertThat(testVictime.getLogin()).isEqualTo(DEFAULT_LOGIN);
        assertThat(testVictime.getPassword()).isEqualTo(DEFAULT_PASSWORD);
    }

    @Test
    void getAllVictimes() {
        // Initialize the database
        victimeRepository.save(victime).block();

        // Get all the victimeList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(victime.getId().intValue()))
            .jsonPath("$.[*].numContrat")
            .value(hasItem(DEFAULT_NUM_CONTRAT))
            .jsonPath("$.[*].cin")
            .value(hasItem(DEFAULT_CIN))
            .jsonPath("$.[*].situationSocial")
            .value(hasItem(DEFAULT_SITUATION_SOCIAL))
            .jsonPath("$.[*].numeroTelephone")
            .value(hasItem(DEFAULT_NUMERO_TELEPHONE))
            .jsonPath("$.[*].nom")
            .value(hasItem(DEFAULT_NOM))
            .jsonPath("$.[*].prenom")
            .value(hasItem(DEFAULT_PRENOM))
            .jsonPath("$.[*].login")
            .value(hasItem(DEFAULT_LOGIN))
            .jsonPath("$.[*].password")
            .value(hasItem(DEFAULT_PASSWORD));
    }

    @Test
    void getVictime() {
        // Initialize the database
        victimeRepository.save(victime).block();

        // Get the victime
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, victime.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(victime.getId().intValue()))
            .jsonPath("$.numContrat")
            .value(is(DEFAULT_NUM_CONTRAT))
            .jsonPath("$.cin")
            .value(is(DEFAULT_CIN))
            .jsonPath("$.situationSocial")
            .value(is(DEFAULT_SITUATION_SOCIAL))
            .jsonPath("$.numeroTelephone")
            .value(is(DEFAULT_NUMERO_TELEPHONE))
            .jsonPath("$.nom")
            .value(is(DEFAULT_NOM))
            .jsonPath("$.prenom")
            .value(is(DEFAULT_PRENOM))
            .jsonPath("$.login")
            .value(is(DEFAULT_LOGIN))
            .jsonPath("$.password")
            .value(is(DEFAULT_PASSWORD));
    }

    @Test
    void getNonExistingVictime() {
        // Get the victime
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewVictime() throws Exception {
        // Configure the mock search repository
        when(mockVictimeSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        victimeRepository.save(victime).block();

        int databaseSizeBeforeUpdate = victimeRepository.findAll().collectList().block().size();

        // Update the victime
        Victime updatedVictime = victimeRepository.findById(victime.getId()).block();
        updatedVictime
            .numContrat(UPDATED_NUM_CONTRAT)
            .cin(UPDATED_CIN)
            .situationSocial(UPDATED_SITUATION_SOCIAL)
            .numeroTelephone(UPDATED_NUMERO_TELEPHONE)
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM)
            .login(UPDATED_LOGIN)
            .password(UPDATED_PASSWORD);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedVictime.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedVictime))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Victime in the database
        List<Victime> victimeList = victimeRepository.findAll().collectList().block();
        assertThat(victimeList).hasSize(databaseSizeBeforeUpdate);
        Victime testVictime = victimeList.get(victimeList.size() - 1);
        assertThat(testVictime.getNumContrat()).isEqualTo(UPDATED_NUM_CONTRAT);
        assertThat(testVictime.getCin()).isEqualTo(UPDATED_CIN);
        assertThat(testVictime.getSituationSocial()).isEqualTo(UPDATED_SITUATION_SOCIAL);
        assertThat(testVictime.getNumeroTelephone()).isEqualTo(UPDATED_NUMERO_TELEPHONE);
        assertThat(testVictime.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testVictime.getPrenom()).isEqualTo(UPDATED_PRENOM);
        assertThat(testVictime.getLogin()).isEqualTo(UPDATED_LOGIN);
        assertThat(testVictime.getPassword()).isEqualTo(UPDATED_PASSWORD);

        // Validate the Victime in Elasticsearch
        verify(mockVictimeSearchRepository).save(testVictime);
    }

    @Test
    void putNonExistingVictime() throws Exception {
        int databaseSizeBeforeUpdate = victimeRepository.findAll().collectList().block().size();
        victime.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, victime.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(victime))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Victime in the database
        List<Victime> victimeList = victimeRepository.findAll().collectList().block();
        assertThat(victimeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Victime in Elasticsearch
        verify(mockVictimeSearchRepository, times(0)).save(victime);
    }

    @Test
    void putWithIdMismatchVictime() throws Exception {
        int databaseSizeBeforeUpdate = victimeRepository.findAll().collectList().block().size();
        victime.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(victime))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Victime in the database
        List<Victime> victimeList = victimeRepository.findAll().collectList().block();
        assertThat(victimeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Victime in Elasticsearch
        verify(mockVictimeSearchRepository, times(0)).save(victime);
    }

    @Test
    void putWithMissingIdPathParamVictime() throws Exception {
        int databaseSizeBeforeUpdate = victimeRepository.findAll().collectList().block().size();
        victime.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(victime))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Victime in the database
        List<Victime> victimeList = victimeRepository.findAll().collectList().block();
        assertThat(victimeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Victime in Elasticsearch
        verify(mockVictimeSearchRepository, times(0)).save(victime);
    }

    @Test
    void partialUpdateVictimeWithPatch() throws Exception {
        // Initialize the database
        victimeRepository.save(victime).block();

        int databaseSizeBeforeUpdate = victimeRepository.findAll().collectList().block().size();

        // Update the victime using partial update
        Victime partialUpdatedVictime = new Victime();
        partialUpdatedVictime.setId(victime.getId());

        partialUpdatedVictime.numContrat(UPDATED_NUM_CONTRAT).prenom(UPDATED_PRENOM).login(UPDATED_LOGIN).password(UPDATED_PASSWORD);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedVictime.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedVictime))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Victime in the database
        List<Victime> victimeList = victimeRepository.findAll().collectList().block();
        assertThat(victimeList).hasSize(databaseSizeBeforeUpdate);
        Victime testVictime = victimeList.get(victimeList.size() - 1);
        assertThat(testVictime.getNumContrat()).isEqualTo(UPDATED_NUM_CONTRAT);
        assertThat(testVictime.getCin()).isEqualTo(DEFAULT_CIN);
        assertThat(testVictime.getSituationSocial()).isEqualTo(DEFAULT_SITUATION_SOCIAL);
        assertThat(testVictime.getNumeroTelephone()).isEqualTo(DEFAULT_NUMERO_TELEPHONE);
        assertThat(testVictime.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testVictime.getPrenom()).isEqualTo(UPDATED_PRENOM);
        assertThat(testVictime.getLogin()).isEqualTo(UPDATED_LOGIN);
        assertThat(testVictime.getPassword()).isEqualTo(UPDATED_PASSWORD);
    }

    @Test
    void fullUpdateVictimeWithPatch() throws Exception {
        // Initialize the database
        victimeRepository.save(victime).block();

        int databaseSizeBeforeUpdate = victimeRepository.findAll().collectList().block().size();

        // Update the victime using partial update
        Victime partialUpdatedVictime = new Victime();
        partialUpdatedVictime.setId(victime.getId());

        partialUpdatedVictime
            .numContrat(UPDATED_NUM_CONTRAT)
            .cin(UPDATED_CIN)
            .situationSocial(UPDATED_SITUATION_SOCIAL)
            .numeroTelephone(UPDATED_NUMERO_TELEPHONE)
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM)
            .login(UPDATED_LOGIN)
            .password(UPDATED_PASSWORD);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedVictime.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedVictime))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Victime in the database
        List<Victime> victimeList = victimeRepository.findAll().collectList().block();
        assertThat(victimeList).hasSize(databaseSizeBeforeUpdate);
        Victime testVictime = victimeList.get(victimeList.size() - 1);
        assertThat(testVictime.getNumContrat()).isEqualTo(UPDATED_NUM_CONTRAT);
        assertThat(testVictime.getCin()).isEqualTo(UPDATED_CIN);
        assertThat(testVictime.getSituationSocial()).isEqualTo(UPDATED_SITUATION_SOCIAL);
        assertThat(testVictime.getNumeroTelephone()).isEqualTo(UPDATED_NUMERO_TELEPHONE);
        assertThat(testVictime.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testVictime.getPrenom()).isEqualTo(UPDATED_PRENOM);
        assertThat(testVictime.getLogin()).isEqualTo(UPDATED_LOGIN);
        assertThat(testVictime.getPassword()).isEqualTo(UPDATED_PASSWORD);
    }

    @Test
    void patchNonExistingVictime() throws Exception {
        int databaseSizeBeforeUpdate = victimeRepository.findAll().collectList().block().size();
        victime.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, victime.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(victime))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Victime in the database
        List<Victime> victimeList = victimeRepository.findAll().collectList().block();
        assertThat(victimeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Victime in Elasticsearch
        verify(mockVictimeSearchRepository, times(0)).save(victime);
    }

    @Test
    void patchWithIdMismatchVictime() throws Exception {
        int databaseSizeBeforeUpdate = victimeRepository.findAll().collectList().block().size();
        victime.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(victime))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Victime in the database
        List<Victime> victimeList = victimeRepository.findAll().collectList().block();
        assertThat(victimeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Victime in Elasticsearch
        verify(mockVictimeSearchRepository, times(0)).save(victime);
    }

    @Test
    void patchWithMissingIdPathParamVictime() throws Exception {
        int databaseSizeBeforeUpdate = victimeRepository.findAll().collectList().block().size();
        victime.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(victime))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Victime in the database
        List<Victime> victimeList = victimeRepository.findAll().collectList().block();
        assertThat(victimeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Victime in Elasticsearch
        verify(mockVictimeSearchRepository, times(0)).save(victime);
    }

    @Test
    void deleteVictime() {
        // Configure the mock search repository
        when(mockVictimeSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        when(mockVictimeSearchRepository.deleteById(anyLong())).thenReturn(Mono.empty());
        // Initialize the database
        victimeRepository.save(victime).block();

        int databaseSizeBeforeDelete = victimeRepository.findAll().collectList().block().size();

        // Delete the victime
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, victime.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Victime> victimeList = victimeRepository.findAll().collectList().block();
        assertThat(victimeList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Victime in Elasticsearch
        verify(mockVictimeSearchRepository, times(1)).deleteById(victime.getId());
    }

    @Test
    void searchVictime() {
        // Configure the mock search repository
        when(mockVictimeSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        victimeRepository.save(victime).block();
        when(mockVictimeSearchRepository.search("id:" + victime.getId())).thenReturn(Flux.just(victime));

        // Search the victime
        webTestClient
            .get()
            .uri(ENTITY_SEARCH_API_URL + "?query=id:" + victime.getId())
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(victime.getId().intValue()))
            .jsonPath("$.[*].numContrat")
            .value(hasItem(DEFAULT_NUM_CONTRAT))
            .jsonPath("$.[*].cin")
            .value(hasItem(DEFAULT_CIN))
            .jsonPath("$.[*].situationSocial")
            .value(hasItem(DEFAULT_SITUATION_SOCIAL))
            .jsonPath("$.[*].numeroTelephone")
            .value(hasItem(DEFAULT_NUMERO_TELEPHONE))
            .jsonPath("$.[*].nom")
            .value(hasItem(DEFAULT_NOM))
            .jsonPath("$.[*].prenom")
            .value(hasItem(DEFAULT_PRENOM))
            .jsonPath("$.[*].login")
            .value(hasItem(DEFAULT_LOGIN))
            .jsonPath("$.[*].password")
            .value(hasItem(DEFAULT_PASSWORD));
    }
}
