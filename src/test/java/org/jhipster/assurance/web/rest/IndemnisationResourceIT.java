package org.jhipster.assurance.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

import java.time.Duration;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.jhipster.assurance.IntegrationTest;
import org.jhipster.assurance.domain.Indemnisation;
import org.jhipster.assurance.repository.IndemnisationRepository;
import org.jhipster.assurance.repository.search.IndemnisationSearchRepository;
import org.jhipster.assurance.service.EntityManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Integration tests for the {@link IndemnisationResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureWebTestClient
@WithMockUser
class IndemnisationResourceIT {

    private static final LocalDate DEFAULT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String ENTITY_API_URL = "/api/indemnisations";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_SEARCH_API_URL = "/api/_search/indemnisations";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private IndemnisationRepository indemnisationRepository;

    /**
     * This repository is mocked in the org.jhipster.assurance.repository.search test package.
     *
     * @see org.jhipster.assurance.repository.search.IndemnisationSearchRepositoryMockConfiguration
     */
    @Autowired
    private IndemnisationSearchRepository mockIndemnisationSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Indemnisation indemnisation;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Indemnisation createEntity(EntityManager em) {
        Indemnisation indemnisation = new Indemnisation().date(DEFAULT_DATE);
        return indemnisation;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Indemnisation createUpdatedEntity(EntityManager em) {
        Indemnisation indemnisation = new Indemnisation().date(UPDATED_DATE);
        return indemnisation;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Indemnisation.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        indemnisation = createEntity(em);
    }

    @Test
    void createIndemnisation() throws Exception {
        int databaseSizeBeforeCreate = indemnisationRepository.findAll().collectList().block().size();
        // Configure the mock search repository
        when(mockIndemnisationSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Create the Indemnisation
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(indemnisation))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Indemnisation in the database
        List<Indemnisation> indemnisationList = indemnisationRepository.findAll().collectList().block();
        assertThat(indemnisationList).hasSize(databaseSizeBeforeCreate + 1);
        Indemnisation testIndemnisation = indemnisationList.get(indemnisationList.size() - 1);
        assertThat(testIndemnisation.getDate()).isEqualTo(DEFAULT_DATE);

        // Validate the Indemnisation in Elasticsearch
        verify(mockIndemnisationSearchRepository, times(1)).save(testIndemnisation);
    }

    @Test
    void createIndemnisationWithExistingId() throws Exception {
        // Create the Indemnisation with an existing ID
        indemnisation.setId(1L);

        int databaseSizeBeforeCreate = indemnisationRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(indemnisation))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Indemnisation in the database
        List<Indemnisation> indemnisationList = indemnisationRepository.findAll().collectList().block();
        assertThat(indemnisationList).hasSize(databaseSizeBeforeCreate);

        // Validate the Indemnisation in Elasticsearch
        verify(mockIndemnisationSearchRepository, times(0)).save(indemnisation);
    }

    @Test
    void getAllIndemnisationsAsStream() {
        // Initialize the database
        indemnisationRepository.save(indemnisation).block();

        List<Indemnisation> indemnisationList = webTestClient
            .get()
            .uri(ENTITY_API_URL)
            .accept(MediaType.APPLICATION_NDJSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentTypeCompatibleWith(MediaType.APPLICATION_NDJSON)
            .returnResult(Indemnisation.class)
            .getResponseBody()
            .filter(indemnisation::equals)
            .collectList()
            .block(Duration.ofSeconds(5));

        assertThat(indemnisationList).isNotNull();
        assertThat(indemnisationList).hasSize(1);
        Indemnisation testIndemnisation = indemnisationList.get(0);
        assertThat(testIndemnisation.getDate()).isEqualTo(DEFAULT_DATE);
    }

    @Test
    void getAllIndemnisations() {
        // Initialize the database
        indemnisationRepository.save(indemnisation).block();

        // Get all the indemnisationList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(indemnisation.getId().intValue()))
            .jsonPath("$.[*].date")
            .value(hasItem(DEFAULT_DATE.toString()));
    }

    @Test
    void getIndemnisation() {
        // Initialize the database
        indemnisationRepository.save(indemnisation).block();

        // Get the indemnisation
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, indemnisation.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(indemnisation.getId().intValue()))
            .jsonPath("$.date")
            .value(is(DEFAULT_DATE.toString()));
    }

    @Test
    void getNonExistingIndemnisation() {
        // Get the indemnisation
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewIndemnisation() throws Exception {
        // Configure the mock search repository
        when(mockIndemnisationSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        indemnisationRepository.save(indemnisation).block();

        int databaseSizeBeforeUpdate = indemnisationRepository.findAll().collectList().block().size();

        // Update the indemnisation
        Indemnisation updatedIndemnisation = indemnisationRepository.findById(indemnisation.getId()).block();
        updatedIndemnisation.date(UPDATED_DATE);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedIndemnisation.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedIndemnisation))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Indemnisation in the database
        List<Indemnisation> indemnisationList = indemnisationRepository.findAll().collectList().block();
        assertThat(indemnisationList).hasSize(databaseSizeBeforeUpdate);
        Indemnisation testIndemnisation = indemnisationList.get(indemnisationList.size() - 1);
        assertThat(testIndemnisation.getDate()).isEqualTo(UPDATED_DATE);

        // Validate the Indemnisation in Elasticsearch
        verify(mockIndemnisationSearchRepository).save(testIndemnisation);
    }

    @Test
    void putNonExistingIndemnisation() throws Exception {
        int databaseSizeBeforeUpdate = indemnisationRepository.findAll().collectList().block().size();
        indemnisation.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, indemnisation.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(indemnisation))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Indemnisation in the database
        List<Indemnisation> indemnisationList = indemnisationRepository.findAll().collectList().block();
        assertThat(indemnisationList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Indemnisation in Elasticsearch
        verify(mockIndemnisationSearchRepository, times(0)).save(indemnisation);
    }

    @Test
    void putWithIdMismatchIndemnisation() throws Exception {
        int databaseSizeBeforeUpdate = indemnisationRepository.findAll().collectList().block().size();
        indemnisation.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(indemnisation))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Indemnisation in the database
        List<Indemnisation> indemnisationList = indemnisationRepository.findAll().collectList().block();
        assertThat(indemnisationList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Indemnisation in Elasticsearch
        verify(mockIndemnisationSearchRepository, times(0)).save(indemnisation);
    }

    @Test
    void putWithMissingIdPathParamIndemnisation() throws Exception {
        int databaseSizeBeforeUpdate = indemnisationRepository.findAll().collectList().block().size();
        indemnisation.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(indemnisation))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Indemnisation in the database
        List<Indemnisation> indemnisationList = indemnisationRepository.findAll().collectList().block();
        assertThat(indemnisationList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Indemnisation in Elasticsearch
        verify(mockIndemnisationSearchRepository, times(0)).save(indemnisation);
    }

    @Test
    void partialUpdateIndemnisationWithPatch() throws Exception {
        // Initialize the database
        indemnisationRepository.save(indemnisation).block();

        int databaseSizeBeforeUpdate = indemnisationRepository.findAll().collectList().block().size();

        // Update the indemnisation using partial update
        Indemnisation partialUpdatedIndemnisation = new Indemnisation();
        partialUpdatedIndemnisation.setId(indemnisation.getId());

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedIndemnisation.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedIndemnisation))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Indemnisation in the database
        List<Indemnisation> indemnisationList = indemnisationRepository.findAll().collectList().block();
        assertThat(indemnisationList).hasSize(databaseSizeBeforeUpdate);
        Indemnisation testIndemnisation = indemnisationList.get(indemnisationList.size() - 1);
        assertThat(testIndemnisation.getDate()).isEqualTo(DEFAULT_DATE);
    }

    @Test
    void fullUpdateIndemnisationWithPatch() throws Exception {
        // Initialize the database
        indemnisationRepository.save(indemnisation).block();

        int databaseSizeBeforeUpdate = indemnisationRepository.findAll().collectList().block().size();

        // Update the indemnisation using partial update
        Indemnisation partialUpdatedIndemnisation = new Indemnisation();
        partialUpdatedIndemnisation.setId(indemnisation.getId());

        partialUpdatedIndemnisation.date(UPDATED_DATE);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedIndemnisation.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedIndemnisation))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Indemnisation in the database
        List<Indemnisation> indemnisationList = indemnisationRepository.findAll().collectList().block();
        assertThat(indemnisationList).hasSize(databaseSizeBeforeUpdate);
        Indemnisation testIndemnisation = indemnisationList.get(indemnisationList.size() - 1);
        assertThat(testIndemnisation.getDate()).isEqualTo(UPDATED_DATE);
    }

    @Test
    void patchNonExistingIndemnisation() throws Exception {
        int databaseSizeBeforeUpdate = indemnisationRepository.findAll().collectList().block().size();
        indemnisation.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, indemnisation.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(indemnisation))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Indemnisation in the database
        List<Indemnisation> indemnisationList = indemnisationRepository.findAll().collectList().block();
        assertThat(indemnisationList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Indemnisation in Elasticsearch
        verify(mockIndemnisationSearchRepository, times(0)).save(indemnisation);
    }

    @Test
    void patchWithIdMismatchIndemnisation() throws Exception {
        int databaseSizeBeforeUpdate = indemnisationRepository.findAll().collectList().block().size();
        indemnisation.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(indemnisation))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Indemnisation in the database
        List<Indemnisation> indemnisationList = indemnisationRepository.findAll().collectList().block();
        assertThat(indemnisationList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Indemnisation in Elasticsearch
        verify(mockIndemnisationSearchRepository, times(0)).save(indemnisation);
    }

    @Test
    void patchWithMissingIdPathParamIndemnisation() throws Exception {
        int databaseSizeBeforeUpdate = indemnisationRepository.findAll().collectList().block().size();
        indemnisation.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(indemnisation))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Indemnisation in the database
        List<Indemnisation> indemnisationList = indemnisationRepository.findAll().collectList().block();
        assertThat(indemnisationList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Indemnisation in Elasticsearch
        verify(mockIndemnisationSearchRepository, times(0)).save(indemnisation);
    }

    @Test
    void deleteIndemnisation() {
        // Configure the mock search repository
        when(mockIndemnisationSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        when(mockIndemnisationSearchRepository.deleteById(anyLong())).thenReturn(Mono.empty());
        // Initialize the database
        indemnisationRepository.save(indemnisation).block();

        int databaseSizeBeforeDelete = indemnisationRepository.findAll().collectList().block().size();

        // Delete the indemnisation
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, indemnisation.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Indemnisation> indemnisationList = indemnisationRepository.findAll().collectList().block();
        assertThat(indemnisationList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Indemnisation in Elasticsearch
        verify(mockIndemnisationSearchRepository, times(1)).deleteById(indemnisation.getId());
    }

    @Test
    void searchIndemnisation() {
        // Configure the mock search repository
        when(mockIndemnisationSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        indemnisationRepository.save(indemnisation).block();
        when(mockIndemnisationSearchRepository.search("id:" + indemnisation.getId())).thenReturn(Flux.just(indemnisation));

        // Search the indemnisation
        webTestClient
            .get()
            .uri(ENTITY_SEARCH_API_URL + "?query=id:" + indemnisation.getId())
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(indemnisation.getId().intValue()))
            .jsonPath("$.[*].date")
            .value(hasItem(DEFAULT_DATE.toString()));
    }
}
