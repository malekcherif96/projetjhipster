package org.jhipster.assurance.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.jhipster.assurance.IntegrationTest;
import org.jhipster.assurance.domain.Cheque;
import org.jhipster.assurance.repository.ChequeRepository;
import org.jhipster.assurance.repository.search.ChequeSearchRepository;
import org.jhipster.assurance.service.EntityManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Integration tests for the {@link ChequeResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureWebTestClient
@WithMockUser
class ChequeResourceIT {

    private static final String DEFAULT_NOM_CHEQUE = "AAAAAAAAAA";
    private static final String UPDATED_NOM_CHEQUE = "BBBBBBBBBB";

    private static final Double DEFAULT_MONTNANT = 1D;
    private static final Double UPDATED_MONTNANT = 2D;

    private static final String ENTITY_API_URL = "/api/cheques";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_SEARCH_API_URL = "/api/_search/cheques";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ChequeRepository chequeRepository;

    /**
     * This repository is mocked in the org.jhipster.assurance.repository.search test package.
     *
     * @see org.jhipster.assurance.repository.search.ChequeSearchRepositoryMockConfiguration
     */
    @Autowired
    private ChequeSearchRepository mockChequeSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Cheque cheque;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cheque createEntity(EntityManager em) {
        Cheque cheque = new Cheque().nomCheque(DEFAULT_NOM_CHEQUE).montnant(DEFAULT_MONTNANT);
        return cheque;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cheque createUpdatedEntity(EntityManager em) {
        Cheque cheque = new Cheque().nomCheque(UPDATED_NOM_CHEQUE).montnant(UPDATED_MONTNANT);
        return cheque;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Cheque.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        cheque = createEntity(em);
    }

    @Test
    void createCheque() throws Exception {
        int databaseSizeBeforeCreate = chequeRepository.findAll().collectList().block().size();
        // Configure the mock search repository
        when(mockChequeSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Create the Cheque
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(cheque))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Cheque in the database
        List<Cheque> chequeList = chequeRepository.findAll().collectList().block();
        assertThat(chequeList).hasSize(databaseSizeBeforeCreate + 1);
        Cheque testCheque = chequeList.get(chequeList.size() - 1);
        assertThat(testCheque.getNomCheque()).isEqualTo(DEFAULT_NOM_CHEQUE);
        assertThat(testCheque.getMontnant()).isEqualTo(DEFAULT_MONTNANT);

        // Validate the Cheque in Elasticsearch
        verify(mockChequeSearchRepository, times(1)).save(testCheque);
    }

    @Test
    void createChequeWithExistingId() throws Exception {
        // Create the Cheque with an existing ID
        cheque.setId(1L);

        int databaseSizeBeforeCreate = chequeRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(cheque))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Cheque in the database
        List<Cheque> chequeList = chequeRepository.findAll().collectList().block();
        assertThat(chequeList).hasSize(databaseSizeBeforeCreate);

        // Validate the Cheque in Elasticsearch
        verify(mockChequeSearchRepository, times(0)).save(cheque);
    }

    @Test
    void getAllChequesAsStream() {
        // Initialize the database
        chequeRepository.save(cheque).block();

        List<Cheque> chequeList = webTestClient
            .get()
            .uri(ENTITY_API_URL)
            .accept(MediaType.APPLICATION_NDJSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentTypeCompatibleWith(MediaType.APPLICATION_NDJSON)
            .returnResult(Cheque.class)
            .getResponseBody()
            .filter(cheque::equals)
            .collectList()
            .block(Duration.ofSeconds(5));

        assertThat(chequeList).isNotNull();
        assertThat(chequeList).hasSize(1);
        Cheque testCheque = chequeList.get(0);
        assertThat(testCheque.getNomCheque()).isEqualTo(DEFAULT_NOM_CHEQUE);
        assertThat(testCheque.getMontnant()).isEqualTo(DEFAULT_MONTNANT);
    }

    @Test
    void getAllCheques() {
        // Initialize the database
        chequeRepository.save(cheque).block();

        // Get all the chequeList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(cheque.getId().intValue()))
            .jsonPath("$.[*].nomCheque")
            .value(hasItem(DEFAULT_NOM_CHEQUE))
            .jsonPath("$.[*].montnant")
            .value(hasItem(DEFAULT_MONTNANT.doubleValue()));
    }

    @Test
    void getCheque() {
        // Initialize the database
        chequeRepository.save(cheque).block();

        // Get the cheque
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, cheque.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(cheque.getId().intValue()))
            .jsonPath("$.nomCheque")
            .value(is(DEFAULT_NOM_CHEQUE))
            .jsonPath("$.montnant")
            .value(is(DEFAULT_MONTNANT.doubleValue()));
    }

    @Test
    void getNonExistingCheque() {
        // Get the cheque
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewCheque() throws Exception {
        // Configure the mock search repository
        when(mockChequeSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        chequeRepository.save(cheque).block();

        int databaseSizeBeforeUpdate = chequeRepository.findAll().collectList().block().size();

        // Update the cheque
        Cheque updatedCheque = chequeRepository.findById(cheque.getId()).block();
        updatedCheque.nomCheque(UPDATED_NOM_CHEQUE).montnant(UPDATED_MONTNANT);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedCheque.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedCheque))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Cheque in the database
        List<Cheque> chequeList = chequeRepository.findAll().collectList().block();
        assertThat(chequeList).hasSize(databaseSizeBeforeUpdate);
        Cheque testCheque = chequeList.get(chequeList.size() - 1);
        assertThat(testCheque.getNomCheque()).isEqualTo(UPDATED_NOM_CHEQUE);
        assertThat(testCheque.getMontnant()).isEqualTo(UPDATED_MONTNANT);

        // Validate the Cheque in Elasticsearch
        verify(mockChequeSearchRepository).save(testCheque);
    }

    @Test
    void putNonExistingCheque() throws Exception {
        int databaseSizeBeforeUpdate = chequeRepository.findAll().collectList().block().size();
        cheque.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, cheque.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(cheque))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Cheque in the database
        List<Cheque> chequeList = chequeRepository.findAll().collectList().block();
        assertThat(chequeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Cheque in Elasticsearch
        verify(mockChequeSearchRepository, times(0)).save(cheque);
    }

    @Test
    void putWithIdMismatchCheque() throws Exception {
        int databaseSizeBeforeUpdate = chequeRepository.findAll().collectList().block().size();
        cheque.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(cheque))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Cheque in the database
        List<Cheque> chequeList = chequeRepository.findAll().collectList().block();
        assertThat(chequeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Cheque in Elasticsearch
        verify(mockChequeSearchRepository, times(0)).save(cheque);
    }

    @Test
    void putWithMissingIdPathParamCheque() throws Exception {
        int databaseSizeBeforeUpdate = chequeRepository.findAll().collectList().block().size();
        cheque.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(cheque))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Cheque in the database
        List<Cheque> chequeList = chequeRepository.findAll().collectList().block();
        assertThat(chequeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Cheque in Elasticsearch
        verify(mockChequeSearchRepository, times(0)).save(cheque);
    }

    @Test
    void partialUpdateChequeWithPatch() throws Exception {
        // Initialize the database
        chequeRepository.save(cheque).block();

        int databaseSizeBeforeUpdate = chequeRepository.findAll().collectList().block().size();

        // Update the cheque using partial update
        Cheque partialUpdatedCheque = new Cheque();
        partialUpdatedCheque.setId(cheque.getId());

        partialUpdatedCheque.nomCheque(UPDATED_NOM_CHEQUE);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedCheque.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedCheque))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Cheque in the database
        List<Cheque> chequeList = chequeRepository.findAll().collectList().block();
        assertThat(chequeList).hasSize(databaseSizeBeforeUpdate);
        Cheque testCheque = chequeList.get(chequeList.size() - 1);
        assertThat(testCheque.getNomCheque()).isEqualTo(UPDATED_NOM_CHEQUE);
        assertThat(testCheque.getMontnant()).isEqualTo(DEFAULT_MONTNANT);
    }

    @Test
    void fullUpdateChequeWithPatch() throws Exception {
        // Initialize the database
        chequeRepository.save(cheque).block();

        int databaseSizeBeforeUpdate = chequeRepository.findAll().collectList().block().size();

        // Update the cheque using partial update
        Cheque partialUpdatedCheque = new Cheque();
        partialUpdatedCheque.setId(cheque.getId());

        partialUpdatedCheque.nomCheque(UPDATED_NOM_CHEQUE).montnant(UPDATED_MONTNANT);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedCheque.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedCheque))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Cheque in the database
        List<Cheque> chequeList = chequeRepository.findAll().collectList().block();
        assertThat(chequeList).hasSize(databaseSizeBeforeUpdate);
        Cheque testCheque = chequeList.get(chequeList.size() - 1);
        assertThat(testCheque.getNomCheque()).isEqualTo(UPDATED_NOM_CHEQUE);
        assertThat(testCheque.getMontnant()).isEqualTo(UPDATED_MONTNANT);
    }

    @Test
    void patchNonExistingCheque() throws Exception {
        int databaseSizeBeforeUpdate = chequeRepository.findAll().collectList().block().size();
        cheque.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, cheque.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(cheque))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Cheque in the database
        List<Cheque> chequeList = chequeRepository.findAll().collectList().block();
        assertThat(chequeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Cheque in Elasticsearch
        verify(mockChequeSearchRepository, times(0)).save(cheque);
    }

    @Test
    void patchWithIdMismatchCheque() throws Exception {
        int databaseSizeBeforeUpdate = chequeRepository.findAll().collectList().block().size();
        cheque.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(cheque))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Cheque in the database
        List<Cheque> chequeList = chequeRepository.findAll().collectList().block();
        assertThat(chequeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Cheque in Elasticsearch
        verify(mockChequeSearchRepository, times(0)).save(cheque);
    }

    @Test
    void patchWithMissingIdPathParamCheque() throws Exception {
        int databaseSizeBeforeUpdate = chequeRepository.findAll().collectList().block().size();
        cheque.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(cheque))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Cheque in the database
        List<Cheque> chequeList = chequeRepository.findAll().collectList().block();
        assertThat(chequeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Cheque in Elasticsearch
        verify(mockChequeSearchRepository, times(0)).save(cheque);
    }

    @Test
    void deleteCheque() {
        // Configure the mock search repository
        when(mockChequeSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        when(mockChequeSearchRepository.deleteById(anyLong())).thenReturn(Mono.empty());
        // Initialize the database
        chequeRepository.save(cheque).block();

        int databaseSizeBeforeDelete = chequeRepository.findAll().collectList().block().size();

        // Delete the cheque
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, cheque.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Cheque> chequeList = chequeRepository.findAll().collectList().block();
        assertThat(chequeList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Cheque in Elasticsearch
        verify(mockChequeSearchRepository, times(1)).deleteById(cheque.getId());
    }

    @Test
    void searchCheque() {
        // Configure the mock search repository
        when(mockChequeSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        chequeRepository.save(cheque).block();
        when(mockChequeSearchRepository.search("id:" + cheque.getId())).thenReturn(Flux.just(cheque));

        // Search the cheque
        webTestClient
            .get()
            .uri(ENTITY_SEARCH_API_URL + "?query=id:" + cheque.getId())
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(cheque.getId().intValue()))
            .jsonPath("$.[*].nomCheque")
            .value(hasItem(DEFAULT_NOM_CHEQUE))
            .jsonPath("$.[*].montnant")
            .value(hasItem(DEFAULT_MONTNANT.doubleValue()));
    }
}
