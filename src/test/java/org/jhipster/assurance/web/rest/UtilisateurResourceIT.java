package org.jhipster.assurance.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.jhipster.assurance.IntegrationTest;
import org.jhipster.assurance.domain.Utilisateur;
import org.jhipster.assurance.repository.UtilisateurRepository;
import org.jhipster.assurance.repository.search.UtilisateurSearchRepository;
import org.jhipster.assurance.service.EntityManager;
import org.jhipster.assurance.service.UtilisateurService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Integration tests for the {@link UtilisateurResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureWebTestClient
@WithMockUser
class UtilisateurResourceIT {

    private static final String DEFAULT_NUM_CONTRAT = "AAAAAAAAAA";
    private static final String UPDATED_NUM_CONTRAT = "BBBBBBBBBB";

    private static final Integer DEFAULT_CIN = 1;
    private static final Integer UPDATED_CIN = 2;

    private static final String DEFAULT_SITUATION_SOCIAL = "AAAAAAAAAA";
    private static final String UPDATED_SITUATION_SOCIAL = "BBBBBBBBBB";

    private static final Integer DEFAULT_NUMERO_TELEPHONE = 1;
    private static final Integer UPDATED_NUMERO_TELEPHONE = 2;

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_PRENOM = "AAAAAAAAAA";
    private static final String UPDATED_PRENOM = "BBBBBBBBBB";

    private static final String DEFAULT_LOGIN = "AAAAAAAAAA";
    private static final String UPDATED_LOGIN = "BBBBBBBBBB";

    private static final String DEFAULT_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/utilisateurs";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_SEARCH_API_URL = "/api/_search/utilisateurs";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Mock
    private UtilisateurRepository utilisateurRepositoryMock;

    @Mock
    private UtilisateurService utilisateurServiceMock;

    /**
     * This repository is mocked in the org.jhipster.assurance.repository.search test package.
     *
     * @see org.jhipster.assurance.repository.search.UtilisateurSearchRepositoryMockConfiguration
     */
    @Autowired
    private UtilisateurSearchRepository mockUtilisateurSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Utilisateur utilisateur;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Utilisateur createEntity(EntityManager em) {
        Utilisateur utilisateur = new Utilisateur()
            .numContrat(DEFAULT_NUM_CONTRAT)
            .cin(DEFAULT_CIN)
            .situationSocial(DEFAULT_SITUATION_SOCIAL)
            .numeroTelephone(DEFAULT_NUMERO_TELEPHONE)
            .nom(DEFAULT_NOM)
            .prenom(DEFAULT_PRENOM)
            .login(DEFAULT_LOGIN)
            .password(DEFAULT_PASSWORD);
        return utilisateur;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Utilisateur createUpdatedEntity(EntityManager em) {
        Utilisateur utilisateur = new Utilisateur()
            .numContrat(UPDATED_NUM_CONTRAT)
            .cin(UPDATED_CIN)
            .situationSocial(UPDATED_SITUATION_SOCIAL)
            .numeroTelephone(UPDATED_NUMERO_TELEPHONE)
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM)
            .login(UPDATED_LOGIN)
            .password(UPDATED_PASSWORD);
        return utilisateur;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll("rel_utilisateur__roles").block();
            em.deleteAll(Utilisateur.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        utilisateur = createEntity(em);
    }

    @Test
    void createUtilisateur() throws Exception {
        int databaseSizeBeforeCreate = utilisateurRepository.findAll().collectList().block().size();
        // Configure the mock search repository
        when(mockUtilisateurSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Create the Utilisateur
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(utilisateur))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Utilisateur in the database
        List<Utilisateur> utilisateurList = utilisateurRepository.findAll().collectList().block();
        assertThat(utilisateurList).hasSize(databaseSizeBeforeCreate + 1);
        Utilisateur testUtilisateur = utilisateurList.get(utilisateurList.size() - 1);
        assertThat(testUtilisateur.getNumContrat()).isEqualTo(DEFAULT_NUM_CONTRAT);
        assertThat(testUtilisateur.getCin()).isEqualTo(DEFAULT_CIN);
        assertThat(testUtilisateur.getSituationSocial()).isEqualTo(DEFAULT_SITUATION_SOCIAL);
        assertThat(testUtilisateur.getNumeroTelephone()).isEqualTo(DEFAULT_NUMERO_TELEPHONE);
        assertThat(testUtilisateur.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testUtilisateur.getPrenom()).isEqualTo(DEFAULT_PRENOM);
        assertThat(testUtilisateur.getLogin()).isEqualTo(DEFAULT_LOGIN);
        assertThat(testUtilisateur.getPassword()).isEqualTo(DEFAULT_PASSWORD);

        // Validate the Utilisateur in Elasticsearch
        verify(mockUtilisateurSearchRepository, times(1)).save(testUtilisateur);
    }

    @Test
    void createUtilisateurWithExistingId() throws Exception {
        // Create the Utilisateur with an existing ID
        utilisateur.setId(1L);

        int databaseSizeBeforeCreate = utilisateurRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(utilisateur))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Utilisateur in the database
        List<Utilisateur> utilisateurList = utilisateurRepository.findAll().collectList().block();
        assertThat(utilisateurList).hasSize(databaseSizeBeforeCreate);

        // Validate the Utilisateur in Elasticsearch
        verify(mockUtilisateurSearchRepository, times(0)).save(utilisateur);
    }

    @Test
    void getAllUtilisateurs() {
        // Initialize the database
        utilisateurRepository.save(utilisateur).block();

        // Get all the utilisateurList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(utilisateur.getId().intValue()))
            .jsonPath("$.[*].numContrat")
            .value(hasItem(DEFAULT_NUM_CONTRAT))
            .jsonPath("$.[*].cin")
            .value(hasItem(DEFAULT_CIN))
            .jsonPath("$.[*].situationSocial")
            .value(hasItem(DEFAULT_SITUATION_SOCIAL))
            .jsonPath("$.[*].numeroTelephone")
            .value(hasItem(DEFAULT_NUMERO_TELEPHONE))
            .jsonPath("$.[*].nom")
            .value(hasItem(DEFAULT_NOM))
            .jsonPath("$.[*].prenom")
            .value(hasItem(DEFAULT_PRENOM))
            .jsonPath("$.[*].login")
            .value(hasItem(DEFAULT_LOGIN))
            .jsonPath("$.[*].password")
            .value(hasItem(DEFAULT_PASSWORD));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllUtilisateursWithEagerRelationshipsIsEnabled() {
        when(utilisateurServiceMock.findAllWithEagerRelationships(any())).thenReturn(Flux.empty());

        webTestClient.get().uri(ENTITY_API_URL + "?eagerload=true").exchange().expectStatus().isOk();

        verify(utilisateurServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllUtilisateursWithEagerRelationshipsIsNotEnabled() {
        when(utilisateurServiceMock.findAllWithEagerRelationships(any())).thenReturn(Flux.empty());

        webTestClient.get().uri(ENTITY_API_URL + "?eagerload=true").exchange().expectStatus().isOk();

        verify(utilisateurServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    void getUtilisateur() {
        // Initialize the database
        utilisateurRepository.save(utilisateur).block();

        // Get the utilisateur
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, utilisateur.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(utilisateur.getId().intValue()))
            .jsonPath("$.numContrat")
            .value(is(DEFAULT_NUM_CONTRAT))
            .jsonPath("$.cin")
            .value(is(DEFAULT_CIN))
            .jsonPath("$.situationSocial")
            .value(is(DEFAULT_SITUATION_SOCIAL))
            .jsonPath("$.numeroTelephone")
            .value(is(DEFAULT_NUMERO_TELEPHONE))
            .jsonPath("$.nom")
            .value(is(DEFAULT_NOM))
            .jsonPath("$.prenom")
            .value(is(DEFAULT_PRENOM))
            .jsonPath("$.login")
            .value(is(DEFAULT_LOGIN))
            .jsonPath("$.password")
            .value(is(DEFAULT_PASSWORD));
    }

    @Test
    void getNonExistingUtilisateur() {
        // Get the utilisateur
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewUtilisateur() throws Exception {
        // Configure the mock search repository
        when(mockUtilisateurSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        utilisateurRepository.save(utilisateur).block();

        int databaseSizeBeforeUpdate = utilisateurRepository.findAll().collectList().block().size();

        // Update the utilisateur
        Utilisateur updatedUtilisateur = utilisateurRepository.findById(utilisateur.getId()).block();
        updatedUtilisateur
            .numContrat(UPDATED_NUM_CONTRAT)
            .cin(UPDATED_CIN)
            .situationSocial(UPDATED_SITUATION_SOCIAL)
            .numeroTelephone(UPDATED_NUMERO_TELEPHONE)
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM)
            .login(UPDATED_LOGIN)
            .password(UPDATED_PASSWORD);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedUtilisateur.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedUtilisateur))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Utilisateur in the database
        List<Utilisateur> utilisateurList = utilisateurRepository.findAll().collectList().block();
        assertThat(utilisateurList).hasSize(databaseSizeBeforeUpdate);
        Utilisateur testUtilisateur = utilisateurList.get(utilisateurList.size() - 1);
        assertThat(testUtilisateur.getNumContrat()).isEqualTo(UPDATED_NUM_CONTRAT);
        assertThat(testUtilisateur.getCin()).isEqualTo(UPDATED_CIN);
        assertThat(testUtilisateur.getSituationSocial()).isEqualTo(UPDATED_SITUATION_SOCIAL);
        assertThat(testUtilisateur.getNumeroTelephone()).isEqualTo(UPDATED_NUMERO_TELEPHONE);
        assertThat(testUtilisateur.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testUtilisateur.getPrenom()).isEqualTo(UPDATED_PRENOM);
        assertThat(testUtilisateur.getLogin()).isEqualTo(UPDATED_LOGIN);
        assertThat(testUtilisateur.getPassword()).isEqualTo(UPDATED_PASSWORD);

        // Validate the Utilisateur in Elasticsearch
        verify(mockUtilisateurSearchRepository).save(testUtilisateur);
    }

    @Test
    void putNonExistingUtilisateur() throws Exception {
        int databaseSizeBeforeUpdate = utilisateurRepository.findAll().collectList().block().size();
        utilisateur.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, utilisateur.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(utilisateur))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Utilisateur in the database
        List<Utilisateur> utilisateurList = utilisateurRepository.findAll().collectList().block();
        assertThat(utilisateurList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Utilisateur in Elasticsearch
        verify(mockUtilisateurSearchRepository, times(0)).save(utilisateur);
    }

    @Test
    void putWithIdMismatchUtilisateur() throws Exception {
        int databaseSizeBeforeUpdate = utilisateurRepository.findAll().collectList().block().size();
        utilisateur.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(utilisateur))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Utilisateur in the database
        List<Utilisateur> utilisateurList = utilisateurRepository.findAll().collectList().block();
        assertThat(utilisateurList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Utilisateur in Elasticsearch
        verify(mockUtilisateurSearchRepository, times(0)).save(utilisateur);
    }

    @Test
    void putWithMissingIdPathParamUtilisateur() throws Exception {
        int databaseSizeBeforeUpdate = utilisateurRepository.findAll().collectList().block().size();
        utilisateur.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(utilisateur))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Utilisateur in the database
        List<Utilisateur> utilisateurList = utilisateurRepository.findAll().collectList().block();
        assertThat(utilisateurList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Utilisateur in Elasticsearch
        verify(mockUtilisateurSearchRepository, times(0)).save(utilisateur);
    }

    @Test
    void partialUpdateUtilisateurWithPatch() throws Exception {
        // Initialize the database
        utilisateurRepository.save(utilisateur).block();

        int databaseSizeBeforeUpdate = utilisateurRepository.findAll().collectList().block().size();

        // Update the utilisateur using partial update
        Utilisateur partialUpdatedUtilisateur = new Utilisateur();
        partialUpdatedUtilisateur.setId(utilisateur.getId());

        partialUpdatedUtilisateur
            .numContrat(UPDATED_NUM_CONTRAT)
            .numeroTelephone(UPDATED_NUMERO_TELEPHONE)
            .login(UPDATED_LOGIN)
            .password(UPDATED_PASSWORD);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedUtilisateur.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedUtilisateur))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Utilisateur in the database
        List<Utilisateur> utilisateurList = utilisateurRepository.findAll().collectList().block();
        assertThat(utilisateurList).hasSize(databaseSizeBeforeUpdate);
        Utilisateur testUtilisateur = utilisateurList.get(utilisateurList.size() - 1);
        assertThat(testUtilisateur.getNumContrat()).isEqualTo(UPDATED_NUM_CONTRAT);
        assertThat(testUtilisateur.getCin()).isEqualTo(DEFAULT_CIN);
        assertThat(testUtilisateur.getSituationSocial()).isEqualTo(DEFAULT_SITUATION_SOCIAL);
        assertThat(testUtilisateur.getNumeroTelephone()).isEqualTo(UPDATED_NUMERO_TELEPHONE);
        assertThat(testUtilisateur.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testUtilisateur.getPrenom()).isEqualTo(DEFAULT_PRENOM);
        assertThat(testUtilisateur.getLogin()).isEqualTo(UPDATED_LOGIN);
        assertThat(testUtilisateur.getPassword()).isEqualTo(UPDATED_PASSWORD);
    }

    @Test
    void fullUpdateUtilisateurWithPatch() throws Exception {
        // Initialize the database
        utilisateurRepository.save(utilisateur).block();

        int databaseSizeBeforeUpdate = utilisateurRepository.findAll().collectList().block().size();

        // Update the utilisateur using partial update
        Utilisateur partialUpdatedUtilisateur = new Utilisateur();
        partialUpdatedUtilisateur.setId(utilisateur.getId());

        partialUpdatedUtilisateur
            .numContrat(UPDATED_NUM_CONTRAT)
            .cin(UPDATED_CIN)
            .situationSocial(UPDATED_SITUATION_SOCIAL)
            .numeroTelephone(UPDATED_NUMERO_TELEPHONE)
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM)
            .login(UPDATED_LOGIN)
            .password(UPDATED_PASSWORD);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedUtilisateur.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedUtilisateur))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Utilisateur in the database
        List<Utilisateur> utilisateurList = utilisateurRepository.findAll().collectList().block();
        assertThat(utilisateurList).hasSize(databaseSizeBeforeUpdate);
        Utilisateur testUtilisateur = utilisateurList.get(utilisateurList.size() - 1);
        assertThat(testUtilisateur.getNumContrat()).isEqualTo(UPDATED_NUM_CONTRAT);
        assertThat(testUtilisateur.getCin()).isEqualTo(UPDATED_CIN);
        assertThat(testUtilisateur.getSituationSocial()).isEqualTo(UPDATED_SITUATION_SOCIAL);
        assertThat(testUtilisateur.getNumeroTelephone()).isEqualTo(UPDATED_NUMERO_TELEPHONE);
        assertThat(testUtilisateur.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testUtilisateur.getPrenom()).isEqualTo(UPDATED_PRENOM);
        assertThat(testUtilisateur.getLogin()).isEqualTo(UPDATED_LOGIN);
        assertThat(testUtilisateur.getPassword()).isEqualTo(UPDATED_PASSWORD);
    }

    @Test
    void patchNonExistingUtilisateur() throws Exception {
        int databaseSizeBeforeUpdate = utilisateurRepository.findAll().collectList().block().size();
        utilisateur.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, utilisateur.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(utilisateur))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Utilisateur in the database
        List<Utilisateur> utilisateurList = utilisateurRepository.findAll().collectList().block();
        assertThat(utilisateurList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Utilisateur in Elasticsearch
        verify(mockUtilisateurSearchRepository, times(0)).save(utilisateur);
    }

    @Test
    void patchWithIdMismatchUtilisateur() throws Exception {
        int databaseSizeBeforeUpdate = utilisateurRepository.findAll().collectList().block().size();
        utilisateur.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(utilisateur))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Utilisateur in the database
        List<Utilisateur> utilisateurList = utilisateurRepository.findAll().collectList().block();
        assertThat(utilisateurList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Utilisateur in Elasticsearch
        verify(mockUtilisateurSearchRepository, times(0)).save(utilisateur);
    }

    @Test
    void patchWithMissingIdPathParamUtilisateur() throws Exception {
        int databaseSizeBeforeUpdate = utilisateurRepository.findAll().collectList().block().size();
        utilisateur.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(utilisateur))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Utilisateur in the database
        List<Utilisateur> utilisateurList = utilisateurRepository.findAll().collectList().block();
        assertThat(utilisateurList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Utilisateur in Elasticsearch
        verify(mockUtilisateurSearchRepository, times(0)).save(utilisateur);
    }

    @Test
    void deleteUtilisateur() {
        // Configure the mock search repository
        when(mockUtilisateurSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        when(mockUtilisateurSearchRepository.deleteById(anyLong())).thenReturn(Mono.empty());
        // Initialize the database
        utilisateurRepository.save(utilisateur).block();

        int databaseSizeBeforeDelete = utilisateurRepository.findAll().collectList().block().size();

        // Delete the utilisateur
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, utilisateur.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Utilisateur> utilisateurList = utilisateurRepository.findAll().collectList().block();
        assertThat(utilisateurList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Utilisateur in Elasticsearch
        verify(mockUtilisateurSearchRepository, times(1)).deleteById(utilisateur.getId());
    }

    @Test
    void searchUtilisateur() {
        // Configure the mock search repository
        when(mockUtilisateurSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        when(mockUtilisateurSearchRepository.count()).thenReturn(Mono.just(1L));
        // Initialize the database
        utilisateurRepository.save(utilisateur).block();
        when(mockUtilisateurSearchRepository.search("id:" + utilisateur.getId(), PageRequest.of(0, 20))).thenReturn(Flux.just(utilisateur));

        // Search the utilisateur
        webTestClient
            .get()
            .uri(ENTITY_SEARCH_API_URL + "?query=id:" + utilisateur.getId())
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(utilisateur.getId().intValue()))
            .jsonPath("$.[*].numContrat")
            .value(hasItem(DEFAULT_NUM_CONTRAT))
            .jsonPath("$.[*].cin")
            .value(hasItem(DEFAULT_CIN))
            .jsonPath("$.[*].situationSocial")
            .value(hasItem(DEFAULT_SITUATION_SOCIAL))
            .jsonPath("$.[*].numeroTelephone")
            .value(hasItem(DEFAULT_NUMERO_TELEPHONE))
            .jsonPath("$.[*].nom")
            .value(hasItem(DEFAULT_NOM))
            .jsonPath("$.[*].prenom")
            .value(hasItem(DEFAULT_PRENOM))
            .jsonPath("$.[*].login")
            .value(hasItem(DEFAULT_LOGIN))
            .jsonPath("$.[*].password")
            .value(hasItem(DEFAULT_PASSWORD));
    }
}
