package org.jhipster.assurance.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

import java.time.Duration;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.jhipster.assurance.IntegrationTest;
import org.jhipster.assurance.domain.Sinistre;
import org.jhipster.assurance.repository.SinistreRepository;
import org.jhipster.assurance.repository.search.SinistreSearchRepository;
import org.jhipster.assurance.service.EntityManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Integration tests for the {@link SinistreResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureWebTestClient
@WithMockUser
class SinistreResourceIT {

    private static final String DEFAULT_TYPE_SINISTRE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_SINISTRE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_SINISTRE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_SINISTRE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_LIEU_SINISTRE = "AAAAAAAAAA";
    private static final String UPDATED_LIEU_SINISTRE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/sinistres";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_SEARCH_API_URL = "/api/_search/sinistres";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private SinistreRepository sinistreRepository;

    /**
     * This repository is mocked in the org.jhipster.assurance.repository.search test package.
     *
     * @see org.jhipster.assurance.repository.search.SinistreSearchRepositoryMockConfiguration
     */
    @Autowired
    private SinistreSearchRepository mockSinistreSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Sinistre sinistre;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sinistre createEntity(EntityManager em) {
        Sinistre sinistre = new Sinistre()
            .typeSinistre(DEFAULT_TYPE_SINISTRE)
            .dateSinistre(DEFAULT_DATE_SINISTRE)
            .lieuSinistre(DEFAULT_LIEU_SINISTRE);
        return sinistre;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sinistre createUpdatedEntity(EntityManager em) {
        Sinistre sinistre = new Sinistre()
            .typeSinistre(UPDATED_TYPE_SINISTRE)
            .dateSinistre(UPDATED_DATE_SINISTRE)
            .lieuSinistre(UPDATED_LIEU_SINISTRE);
        return sinistre;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Sinistre.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        sinistre = createEntity(em);
    }

    @Test
    void createSinistre() throws Exception {
        int databaseSizeBeforeCreate = sinistreRepository.findAll().collectList().block().size();
        // Configure the mock search repository
        when(mockSinistreSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Create the Sinistre
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(sinistre))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Sinistre in the database
        List<Sinistre> sinistreList = sinistreRepository.findAll().collectList().block();
        assertThat(sinistreList).hasSize(databaseSizeBeforeCreate + 1);
        Sinistre testSinistre = sinistreList.get(sinistreList.size() - 1);
        assertThat(testSinistre.getTypeSinistre()).isEqualTo(DEFAULT_TYPE_SINISTRE);
        assertThat(testSinistre.getDateSinistre()).isEqualTo(DEFAULT_DATE_SINISTRE);
        assertThat(testSinistre.getLieuSinistre()).isEqualTo(DEFAULT_LIEU_SINISTRE);

        // Validate the Sinistre in Elasticsearch
        verify(mockSinistreSearchRepository, times(1)).save(testSinistre);
    }

    @Test
    void createSinistreWithExistingId() throws Exception {
        // Create the Sinistre with an existing ID
        sinistre.setId(1L);

        int databaseSizeBeforeCreate = sinistreRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(sinistre))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Sinistre in the database
        List<Sinistre> sinistreList = sinistreRepository.findAll().collectList().block();
        assertThat(sinistreList).hasSize(databaseSizeBeforeCreate);

        // Validate the Sinistre in Elasticsearch
        verify(mockSinistreSearchRepository, times(0)).save(sinistre);
    }

    @Test
    void getAllSinistres() {
        // Initialize the database
        sinistreRepository.save(sinistre).block();

        // Get all the sinistreList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(sinistre.getId().intValue()))
            .jsonPath("$.[*].typeSinistre")
            .value(hasItem(DEFAULT_TYPE_SINISTRE))
            .jsonPath("$.[*].dateSinistre")
            .value(hasItem(DEFAULT_DATE_SINISTRE.toString()))
            .jsonPath("$.[*].lieuSinistre")
            .value(hasItem(DEFAULT_LIEU_SINISTRE));
    }

    @Test
    void getSinistre() {
        // Initialize the database
        sinistreRepository.save(sinistre).block();

        // Get the sinistre
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, sinistre.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(sinistre.getId().intValue()))
            .jsonPath("$.typeSinistre")
            .value(is(DEFAULT_TYPE_SINISTRE))
            .jsonPath("$.dateSinistre")
            .value(is(DEFAULT_DATE_SINISTRE.toString()))
            .jsonPath("$.lieuSinistre")
            .value(is(DEFAULT_LIEU_SINISTRE));
    }

    @Test
    void getNonExistingSinistre() {
        // Get the sinistre
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewSinistre() throws Exception {
        // Configure the mock search repository
        when(mockSinistreSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        sinistreRepository.save(sinistre).block();

        int databaseSizeBeforeUpdate = sinistreRepository.findAll().collectList().block().size();

        // Update the sinistre
        Sinistre updatedSinistre = sinistreRepository.findById(sinistre.getId()).block();
        updatedSinistre.typeSinistre(UPDATED_TYPE_SINISTRE).dateSinistre(UPDATED_DATE_SINISTRE).lieuSinistre(UPDATED_LIEU_SINISTRE);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedSinistre.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedSinistre))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Sinistre in the database
        List<Sinistre> sinistreList = sinistreRepository.findAll().collectList().block();
        assertThat(sinistreList).hasSize(databaseSizeBeforeUpdate);
        Sinistre testSinistre = sinistreList.get(sinistreList.size() - 1);
        assertThat(testSinistre.getTypeSinistre()).isEqualTo(UPDATED_TYPE_SINISTRE);
        assertThat(testSinistre.getDateSinistre()).isEqualTo(UPDATED_DATE_SINISTRE);
        assertThat(testSinistre.getLieuSinistre()).isEqualTo(UPDATED_LIEU_SINISTRE);

        // Validate the Sinistre in Elasticsearch
        verify(mockSinistreSearchRepository).save(testSinistre);
    }

    @Test
    void putNonExistingSinistre() throws Exception {
        int databaseSizeBeforeUpdate = sinistreRepository.findAll().collectList().block().size();
        sinistre.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, sinistre.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(sinistre))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Sinistre in the database
        List<Sinistre> sinistreList = sinistreRepository.findAll().collectList().block();
        assertThat(sinistreList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Sinistre in Elasticsearch
        verify(mockSinistreSearchRepository, times(0)).save(sinistre);
    }

    @Test
    void putWithIdMismatchSinistre() throws Exception {
        int databaseSizeBeforeUpdate = sinistreRepository.findAll().collectList().block().size();
        sinistre.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(sinistre))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Sinistre in the database
        List<Sinistre> sinistreList = sinistreRepository.findAll().collectList().block();
        assertThat(sinistreList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Sinistre in Elasticsearch
        verify(mockSinistreSearchRepository, times(0)).save(sinistre);
    }

    @Test
    void putWithMissingIdPathParamSinistre() throws Exception {
        int databaseSizeBeforeUpdate = sinistreRepository.findAll().collectList().block().size();
        sinistre.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(sinistre))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Sinistre in the database
        List<Sinistre> sinistreList = sinistreRepository.findAll().collectList().block();
        assertThat(sinistreList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Sinistre in Elasticsearch
        verify(mockSinistreSearchRepository, times(0)).save(sinistre);
    }

    @Test
    void partialUpdateSinistreWithPatch() throws Exception {
        // Initialize the database
        sinistreRepository.save(sinistre).block();

        int databaseSizeBeforeUpdate = sinistreRepository.findAll().collectList().block().size();

        // Update the sinistre using partial update
        Sinistre partialUpdatedSinistre = new Sinistre();
        partialUpdatedSinistre.setId(sinistre.getId());

        partialUpdatedSinistre.typeSinistre(UPDATED_TYPE_SINISTRE).lieuSinistre(UPDATED_LIEU_SINISTRE);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedSinistre.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedSinistre))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Sinistre in the database
        List<Sinistre> sinistreList = sinistreRepository.findAll().collectList().block();
        assertThat(sinistreList).hasSize(databaseSizeBeforeUpdate);
        Sinistre testSinistre = sinistreList.get(sinistreList.size() - 1);
        assertThat(testSinistre.getTypeSinistre()).isEqualTo(UPDATED_TYPE_SINISTRE);
        assertThat(testSinistre.getDateSinistre()).isEqualTo(DEFAULT_DATE_SINISTRE);
        assertThat(testSinistre.getLieuSinistre()).isEqualTo(UPDATED_LIEU_SINISTRE);
    }

    @Test
    void fullUpdateSinistreWithPatch() throws Exception {
        // Initialize the database
        sinistreRepository.save(sinistre).block();

        int databaseSizeBeforeUpdate = sinistreRepository.findAll().collectList().block().size();

        // Update the sinistre using partial update
        Sinistre partialUpdatedSinistre = new Sinistre();
        partialUpdatedSinistre.setId(sinistre.getId());

        partialUpdatedSinistre.typeSinistre(UPDATED_TYPE_SINISTRE).dateSinistre(UPDATED_DATE_SINISTRE).lieuSinistre(UPDATED_LIEU_SINISTRE);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedSinistre.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedSinistre))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Sinistre in the database
        List<Sinistre> sinistreList = sinistreRepository.findAll().collectList().block();
        assertThat(sinistreList).hasSize(databaseSizeBeforeUpdate);
        Sinistre testSinistre = sinistreList.get(sinistreList.size() - 1);
        assertThat(testSinistre.getTypeSinistre()).isEqualTo(UPDATED_TYPE_SINISTRE);
        assertThat(testSinistre.getDateSinistre()).isEqualTo(UPDATED_DATE_SINISTRE);
        assertThat(testSinistre.getLieuSinistre()).isEqualTo(UPDATED_LIEU_SINISTRE);
    }

    @Test
    void patchNonExistingSinistre() throws Exception {
        int databaseSizeBeforeUpdate = sinistreRepository.findAll().collectList().block().size();
        sinistre.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, sinistre.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(sinistre))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Sinistre in the database
        List<Sinistre> sinistreList = sinistreRepository.findAll().collectList().block();
        assertThat(sinistreList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Sinistre in Elasticsearch
        verify(mockSinistreSearchRepository, times(0)).save(sinistre);
    }

    @Test
    void patchWithIdMismatchSinistre() throws Exception {
        int databaseSizeBeforeUpdate = sinistreRepository.findAll().collectList().block().size();
        sinistre.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(sinistre))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Sinistre in the database
        List<Sinistre> sinistreList = sinistreRepository.findAll().collectList().block();
        assertThat(sinistreList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Sinistre in Elasticsearch
        verify(mockSinistreSearchRepository, times(0)).save(sinistre);
    }

    @Test
    void patchWithMissingIdPathParamSinistre() throws Exception {
        int databaseSizeBeforeUpdate = sinistreRepository.findAll().collectList().block().size();
        sinistre.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(sinistre))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Sinistre in the database
        List<Sinistre> sinistreList = sinistreRepository.findAll().collectList().block();
        assertThat(sinistreList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Sinistre in Elasticsearch
        verify(mockSinistreSearchRepository, times(0)).save(sinistre);
    }

    @Test
    void deleteSinistre() {
        // Configure the mock search repository
        when(mockSinistreSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        when(mockSinistreSearchRepository.deleteById(anyLong())).thenReturn(Mono.empty());
        // Initialize the database
        sinistreRepository.save(sinistre).block();

        int databaseSizeBeforeDelete = sinistreRepository.findAll().collectList().block().size();

        // Delete the sinistre
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, sinistre.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Sinistre> sinistreList = sinistreRepository.findAll().collectList().block();
        assertThat(sinistreList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Sinistre in Elasticsearch
        verify(mockSinistreSearchRepository, times(1)).deleteById(sinistre.getId());
    }

    @Test
    void searchSinistre() {
        // Configure the mock search repository
        when(mockSinistreSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        when(mockSinistreSearchRepository.count()).thenReturn(Mono.just(1L));
        // Initialize the database
        sinistreRepository.save(sinistre).block();
        when(mockSinistreSearchRepository.search("id:" + sinistre.getId(), PageRequest.of(0, 20))).thenReturn(Flux.just(sinistre));

        // Search the sinistre
        webTestClient
            .get()
            .uri(ENTITY_SEARCH_API_URL + "?query=id:" + sinistre.getId())
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(sinistre.getId().intValue()))
            .jsonPath("$.[*].typeSinistre")
            .value(hasItem(DEFAULT_TYPE_SINISTRE))
            .jsonPath("$.[*].dateSinistre")
            .value(hasItem(DEFAULT_DATE_SINISTRE.toString()))
            .jsonPath("$.[*].lieuSinistre")
            .value(hasItem(DEFAULT_LIEU_SINISTRE));
    }
}
