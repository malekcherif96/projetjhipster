package org.jhipster.assurance.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.jhipster.assurance.IntegrationTest;
import org.jhipster.assurance.domain.Quittance;
import org.jhipster.assurance.repository.QuittanceRepository;
import org.jhipster.assurance.repository.search.QuittanceSearchRepository;
import org.jhipster.assurance.service.EntityManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Integration tests for the {@link QuittanceResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureWebTestClient
@WithMockUser
class QuittanceResourceIT {

    private static final String DEFAULT_NUM_QUITTANCE = "AAAAAAAAAA";
    private static final String UPDATED_NUM_QUITTANCE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/quittances";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_SEARCH_API_URL = "/api/_search/quittances";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private QuittanceRepository quittanceRepository;

    /**
     * This repository is mocked in the org.jhipster.assurance.repository.search test package.
     *
     * @see org.jhipster.assurance.repository.search.QuittanceSearchRepositoryMockConfiguration
     */
    @Autowired
    private QuittanceSearchRepository mockQuittanceSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Quittance quittance;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Quittance createEntity(EntityManager em) {
        Quittance quittance = new Quittance().numQuittance(DEFAULT_NUM_QUITTANCE);
        return quittance;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Quittance createUpdatedEntity(EntityManager em) {
        Quittance quittance = new Quittance().numQuittance(UPDATED_NUM_QUITTANCE);
        return quittance;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Quittance.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        quittance = createEntity(em);
    }

    @Test
    void createQuittance() throws Exception {
        int databaseSizeBeforeCreate = quittanceRepository.findAll().collectList().block().size();
        // Configure the mock search repository
        when(mockQuittanceSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Create the Quittance
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(quittance))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Quittance in the database
        List<Quittance> quittanceList = quittanceRepository.findAll().collectList().block();
        assertThat(quittanceList).hasSize(databaseSizeBeforeCreate + 1);
        Quittance testQuittance = quittanceList.get(quittanceList.size() - 1);
        assertThat(testQuittance.getNumQuittance()).isEqualTo(DEFAULT_NUM_QUITTANCE);

        // Validate the Quittance in Elasticsearch
        verify(mockQuittanceSearchRepository, times(1)).save(testQuittance);
    }

    @Test
    void createQuittanceWithExistingId() throws Exception {
        // Create the Quittance with an existing ID
        quittance.setId(1L);

        int databaseSizeBeforeCreate = quittanceRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(quittance))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Quittance in the database
        List<Quittance> quittanceList = quittanceRepository.findAll().collectList().block();
        assertThat(quittanceList).hasSize(databaseSizeBeforeCreate);

        // Validate the Quittance in Elasticsearch
        verify(mockQuittanceSearchRepository, times(0)).save(quittance);
    }

    @Test
    void getAllQuittancesAsStream() {
        // Initialize the database
        quittanceRepository.save(quittance).block();

        List<Quittance> quittanceList = webTestClient
            .get()
            .uri(ENTITY_API_URL)
            .accept(MediaType.APPLICATION_NDJSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentTypeCompatibleWith(MediaType.APPLICATION_NDJSON)
            .returnResult(Quittance.class)
            .getResponseBody()
            .filter(quittance::equals)
            .collectList()
            .block(Duration.ofSeconds(5));

        assertThat(quittanceList).isNotNull();
        assertThat(quittanceList).hasSize(1);
        Quittance testQuittance = quittanceList.get(0);
        assertThat(testQuittance.getNumQuittance()).isEqualTo(DEFAULT_NUM_QUITTANCE);
    }

    @Test
    void getAllQuittances() {
        // Initialize the database
        quittanceRepository.save(quittance).block();

        // Get all the quittanceList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(quittance.getId().intValue()))
            .jsonPath("$.[*].numQuittance")
            .value(hasItem(DEFAULT_NUM_QUITTANCE));
    }

    @Test
    void getQuittance() {
        // Initialize the database
        quittanceRepository.save(quittance).block();

        // Get the quittance
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, quittance.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(quittance.getId().intValue()))
            .jsonPath("$.numQuittance")
            .value(is(DEFAULT_NUM_QUITTANCE));
    }

    @Test
    void getNonExistingQuittance() {
        // Get the quittance
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewQuittance() throws Exception {
        // Configure the mock search repository
        when(mockQuittanceSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        quittanceRepository.save(quittance).block();

        int databaseSizeBeforeUpdate = quittanceRepository.findAll().collectList().block().size();

        // Update the quittance
        Quittance updatedQuittance = quittanceRepository.findById(quittance.getId()).block();
        updatedQuittance.numQuittance(UPDATED_NUM_QUITTANCE);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedQuittance.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedQuittance))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Quittance in the database
        List<Quittance> quittanceList = quittanceRepository.findAll().collectList().block();
        assertThat(quittanceList).hasSize(databaseSizeBeforeUpdate);
        Quittance testQuittance = quittanceList.get(quittanceList.size() - 1);
        assertThat(testQuittance.getNumQuittance()).isEqualTo(UPDATED_NUM_QUITTANCE);

        // Validate the Quittance in Elasticsearch
        verify(mockQuittanceSearchRepository).save(testQuittance);
    }

    @Test
    void putNonExistingQuittance() throws Exception {
        int databaseSizeBeforeUpdate = quittanceRepository.findAll().collectList().block().size();
        quittance.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, quittance.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(quittance))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Quittance in the database
        List<Quittance> quittanceList = quittanceRepository.findAll().collectList().block();
        assertThat(quittanceList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Quittance in Elasticsearch
        verify(mockQuittanceSearchRepository, times(0)).save(quittance);
    }

    @Test
    void putWithIdMismatchQuittance() throws Exception {
        int databaseSizeBeforeUpdate = quittanceRepository.findAll().collectList().block().size();
        quittance.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(quittance))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Quittance in the database
        List<Quittance> quittanceList = quittanceRepository.findAll().collectList().block();
        assertThat(quittanceList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Quittance in Elasticsearch
        verify(mockQuittanceSearchRepository, times(0)).save(quittance);
    }

    @Test
    void putWithMissingIdPathParamQuittance() throws Exception {
        int databaseSizeBeforeUpdate = quittanceRepository.findAll().collectList().block().size();
        quittance.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(quittance))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Quittance in the database
        List<Quittance> quittanceList = quittanceRepository.findAll().collectList().block();
        assertThat(quittanceList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Quittance in Elasticsearch
        verify(mockQuittanceSearchRepository, times(0)).save(quittance);
    }

    @Test
    void partialUpdateQuittanceWithPatch() throws Exception {
        // Initialize the database
        quittanceRepository.save(quittance).block();

        int databaseSizeBeforeUpdate = quittanceRepository.findAll().collectList().block().size();

        // Update the quittance using partial update
        Quittance partialUpdatedQuittance = new Quittance();
        partialUpdatedQuittance.setId(quittance.getId());

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedQuittance.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedQuittance))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Quittance in the database
        List<Quittance> quittanceList = quittanceRepository.findAll().collectList().block();
        assertThat(quittanceList).hasSize(databaseSizeBeforeUpdate);
        Quittance testQuittance = quittanceList.get(quittanceList.size() - 1);
        assertThat(testQuittance.getNumQuittance()).isEqualTo(DEFAULT_NUM_QUITTANCE);
    }

    @Test
    void fullUpdateQuittanceWithPatch() throws Exception {
        // Initialize the database
        quittanceRepository.save(quittance).block();

        int databaseSizeBeforeUpdate = quittanceRepository.findAll().collectList().block().size();

        // Update the quittance using partial update
        Quittance partialUpdatedQuittance = new Quittance();
        partialUpdatedQuittance.setId(quittance.getId());

        partialUpdatedQuittance.numQuittance(UPDATED_NUM_QUITTANCE);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedQuittance.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedQuittance))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Quittance in the database
        List<Quittance> quittanceList = quittanceRepository.findAll().collectList().block();
        assertThat(quittanceList).hasSize(databaseSizeBeforeUpdate);
        Quittance testQuittance = quittanceList.get(quittanceList.size() - 1);
        assertThat(testQuittance.getNumQuittance()).isEqualTo(UPDATED_NUM_QUITTANCE);
    }

    @Test
    void patchNonExistingQuittance() throws Exception {
        int databaseSizeBeforeUpdate = quittanceRepository.findAll().collectList().block().size();
        quittance.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, quittance.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(quittance))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Quittance in the database
        List<Quittance> quittanceList = quittanceRepository.findAll().collectList().block();
        assertThat(quittanceList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Quittance in Elasticsearch
        verify(mockQuittanceSearchRepository, times(0)).save(quittance);
    }

    @Test
    void patchWithIdMismatchQuittance() throws Exception {
        int databaseSizeBeforeUpdate = quittanceRepository.findAll().collectList().block().size();
        quittance.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(quittance))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Quittance in the database
        List<Quittance> quittanceList = quittanceRepository.findAll().collectList().block();
        assertThat(quittanceList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Quittance in Elasticsearch
        verify(mockQuittanceSearchRepository, times(0)).save(quittance);
    }

    @Test
    void patchWithMissingIdPathParamQuittance() throws Exception {
        int databaseSizeBeforeUpdate = quittanceRepository.findAll().collectList().block().size();
        quittance.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(quittance))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Quittance in the database
        List<Quittance> quittanceList = quittanceRepository.findAll().collectList().block();
        assertThat(quittanceList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Quittance in Elasticsearch
        verify(mockQuittanceSearchRepository, times(0)).save(quittance);
    }

    @Test
    void deleteQuittance() {
        // Configure the mock search repository
        when(mockQuittanceSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        when(mockQuittanceSearchRepository.deleteById(anyLong())).thenReturn(Mono.empty());
        // Initialize the database
        quittanceRepository.save(quittance).block();

        int databaseSizeBeforeDelete = quittanceRepository.findAll().collectList().block().size();

        // Delete the quittance
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, quittance.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Quittance> quittanceList = quittanceRepository.findAll().collectList().block();
        assertThat(quittanceList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Quittance in Elasticsearch
        verify(mockQuittanceSearchRepository, times(1)).deleteById(quittance.getId());
    }

    @Test
    void searchQuittance() {
        // Configure the mock search repository
        when(mockQuittanceSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        quittanceRepository.save(quittance).block();
        when(mockQuittanceSearchRepository.search("id:" + quittance.getId())).thenReturn(Flux.just(quittance));

        // Search the quittance
        webTestClient
            .get()
            .uri(ENTITY_SEARCH_API_URL + "?query=id:" + quittance.getId())
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(quittance.getId().intValue()))
            .jsonPath("$.[*].numQuittance")
            .value(hasItem(DEFAULT_NUM_QUITTANCE));
    }
}
