package org.jhipster.assurance.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.jhipster.assurance.IntegrationTest;
import org.jhipster.assurance.domain.Document;
import org.jhipster.assurance.repository.DocumentRepository;
import org.jhipster.assurance.repository.search.DocumentSearchRepository;
import org.jhipster.assurance.service.EntityManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Integration tests for the {@link DocumentResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureWebTestClient
@WithMockUser
class DocumentResourceIT {

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/documents";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_SEARCH_API_URL = "/api/_search/documents";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private DocumentRepository documentRepository;

    /**
     * This repository is mocked in the org.jhipster.assurance.repository.search test package.
     *
     * @see org.jhipster.assurance.repository.search.DocumentSearchRepositoryMockConfiguration
     */
    @Autowired
    private DocumentSearchRepository mockDocumentSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Document document;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Document createEntity(EntityManager em) {
        Document document = new Document().libelle(DEFAULT_LIBELLE).type(DEFAULT_TYPE);
        return document;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Document createUpdatedEntity(EntityManager em) {
        Document document = new Document().libelle(UPDATED_LIBELLE).type(UPDATED_TYPE);
        return document;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Document.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        document = createEntity(em);
    }

    @Test
    void createDocument() throws Exception {
        int databaseSizeBeforeCreate = documentRepository.findAll().collectList().block().size();
        // Configure the mock search repository
        when(mockDocumentSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Create the Document
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(document))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Document in the database
        List<Document> documentList = documentRepository.findAll().collectList().block();
        assertThat(documentList).hasSize(databaseSizeBeforeCreate + 1);
        Document testDocument = documentList.get(documentList.size() - 1);
        assertThat(testDocument.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
        assertThat(testDocument.getType()).isEqualTo(DEFAULT_TYPE);

        // Validate the Document in Elasticsearch
        verify(mockDocumentSearchRepository, times(1)).save(testDocument);
    }

    @Test
    void createDocumentWithExistingId() throws Exception {
        // Create the Document with an existing ID
        document.setId(1L);

        int databaseSizeBeforeCreate = documentRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(document))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Document in the database
        List<Document> documentList = documentRepository.findAll().collectList().block();
        assertThat(documentList).hasSize(databaseSizeBeforeCreate);

        // Validate the Document in Elasticsearch
        verify(mockDocumentSearchRepository, times(0)).save(document);
    }

    @Test
    void getAllDocumentsAsStream() {
        // Initialize the database
        documentRepository.save(document).block();

        List<Document> documentList = webTestClient
            .get()
            .uri(ENTITY_API_URL)
            .accept(MediaType.APPLICATION_NDJSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentTypeCompatibleWith(MediaType.APPLICATION_NDJSON)
            .returnResult(Document.class)
            .getResponseBody()
            .filter(document::equals)
            .collectList()
            .block(Duration.ofSeconds(5));

        assertThat(documentList).isNotNull();
        assertThat(documentList).hasSize(1);
        Document testDocument = documentList.get(0);
        assertThat(testDocument.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
        assertThat(testDocument.getType()).isEqualTo(DEFAULT_TYPE);
    }

    @Test
    void getAllDocuments() {
        // Initialize the database
        documentRepository.save(document).block();

        // Get all the documentList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(document.getId().intValue()))
            .jsonPath("$.[*].libelle")
            .value(hasItem(DEFAULT_LIBELLE))
            .jsonPath("$.[*].type")
            .value(hasItem(DEFAULT_TYPE));
    }

    @Test
    void getDocument() {
        // Initialize the database
        documentRepository.save(document).block();

        // Get the document
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, document.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(document.getId().intValue()))
            .jsonPath("$.libelle")
            .value(is(DEFAULT_LIBELLE))
            .jsonPath("$.type")
            .value(is(DEFAULT_TYPE));
    }

    @Test
    void getNonExistingDocument() {
        // Get the document
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewDocument() throws Exception {
        // Configure the mock search repository
        when(mockDocumentSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        documentRepository.save(document).block();

        int databaseSizeBeforeUpdate = documentRepository.findAll().collectList().block().size();

        // Update the document
        Document updatedDocument = documentRepository.findById(document.getId()).block();
        updatedDocument.libelle(UPDATED_LIBELLE).type(UPDATED_TYPE);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedDocument.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedDocument))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Document in the database
        List<Document> documentList = documentRepository.findAll().collectList().block();
        assertThat(documentList).hasSize(databaseSizeBeforeUpdate);
        Document testDocument = documentList.get(documentList.size() - 1);
        assertThat(testDocument.getLibelle()).isEqualTo(UPDATED_LIBELLE);
        assertThat(testDocument.getType()).isEqualTo(UPDATED_TYPE);

        // Validate the Document in Elasticsearch
        verify(mockDocumentSearchRepository).save(testDocument);
    }

    @Test
    void putNonExistingDocument() throws Exception {
        int databaseSizeBeforeUpdate = documentRepository.findAll().collectList().block().size();
        document.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, document.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(document))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Document in the database
        List<Document> documentList = documentRepository.findAll().collectList().block();
        assertThat(documentList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Document in Elasticsearch
        verify(mockDocumentSearchRepository, times(0)).save(document);
    }

    @Test
    void putWithIdMismatchDocument() throws Exception {
        int databaseSizeBeforeUpdate = documentRepository.findAll().collectList().block().size();
        document.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(document))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Document in the database
        List<Document> documentList = documentRepository.findAll().collectList().block();
        assertThat(documentList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Document in Elasticsearch
        verify(mockDocumentSearchRepository, times(0)).save(document);
    }

    @Test
    void putWithMissingIdPathParamDocument() throws Exception {
        int databaseSizeBeforeUpdate = documentRepository.findAll().collectList().block().size();
        document.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(document))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Document in the database
        List<Document> documentList = documentRepository.findAll().collectList().block();
        assertThat(documentList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Document in Elasticsearch
        verify(mockDocumentSearchRepository, times(0)).save(document);
    }

    @Test
    void partialUpdateDocumentWithPatch() throws Exception {
        // Initialize the database
        documentRepository.save(document).block();

        int databaseSizeBeforeUpdate = documentRepository.findAll().collectList().block().size();

        // Update the document using partial update
        Document partialUpdatedDocument = new Document();
        partialUpdatedDocument.setId(document.getId());

        partialUpdatedDocument.libelle(UPDATED_LIBELLE).type(UPDATED_TYPE);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedDocument.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedDocument))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Document in the database
        List<Document> documentList = documentRepository.findAll().collectList().block();
        assertThat(documentList).hasSize(databaseSizeBeforeUpdate);
        Document testDocument = documentList.get(documentList.size() - 1);
        assertThat(testDocument.getLibelle()).isEqualTo(UPDATED_LIBELLE);
        assertThat(testDocument.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    void fullUpdateDocumentWithPatch() throws Exception {
        // Initialize the database
        documentRepository.save(document).block();

        int databaseSizeBeforeUpdate = documentRepository.findAll().collectList().block().size();

        // Update the document using partial update
        Document partialUpdatedDocument = new Document();
        partialUpdatedDocument.setId(document.getId());

        partialUpdatedDocument.libelle(UPDATED_LIBELLE).type(UPDATED_TYPE);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedDocument.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedDocument))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Document in the database
        List<Document> documentList = documentRepository.findAll().collectList().block();
        assertThat(documentList).hasSize(databaseSizeBeforeUpdate);
        Document testDocument = documentList.get(documentList.size() - 1);
        assertThat(testDocument.getLibelle()).isEqualTo(UPDATED_LIBELLE);
        assertThat(testDocument.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    void patchNonExistingDocument() throws Exception {
        int databaseSizeBeforeUpdate = documentRepository.findAll().collectList().block().size();
        document.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, document.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(document))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Document in the database
        List<Document> documentList = documentRepository.findAll().collectList().block();
        assertThat(documentList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Document in Elasticsearch
        verify(mockDocumentSearchRepository, times(0)).save(document);
    }

    @Test
    void patchWithIdMismatchDocument() throws Exception {
        int databaseSizeBeforeUpdate = documentRepository.findAll().collectList().block().size();
        document.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(document))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Document in the database
        List<Document> documentList = documentRepository.findAll().collectList().block();
        assertThat(documentList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Document in Elasticsearch
        verify(mockDocumentSearchRepository, times(0)).save(document);
    }

    @Test
    void patchWithMissingIdPathParamDocument() throws Exception {
        int databaseSizeBeforeUpdate = documentRepository.findAll().collectList().block().size();
        document.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(document))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Document in the database
        List<Document> documentList = documentRepository.findAll().collectList().block();
        assertThat(documentList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Document in Elasticsearch
        verify(mockDocumentSearchRepository, times(0)).save(document);
    }

    @Test
    void deleteDocument() {
        // Configure the mock search repository
        when(mockDocumentSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        when(mockDocumentSearchRepository.deleteById(anyLong())).thenReturn(Mono.empty());
        // Initialize the database
        documentRepository.save(document).block();

        int databaseSizeBeforeDelete = documentRepository.findAll().collectList().block().size();

        // Delete the document
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, document.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Document> documentList = documentRepository.findAll().collectList().block();
        assertThat(documentList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Document in Elasticsearch
        verify(mockDocumentSearchRepository, times(1)).deleteById(document.getId());
    }

    @Test
    void searchDocument() {
        // Configure the mock search repository
        when(mockDocumentSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        documentRepository.save(document).block();
        when(mockDocumentSearchRepository.search("id:" + document.getId())).thenReturn(Flux.just(document));

        // Search the document
        webTestClient
            .get()
            .uri(ENTITY_SEARCH_API_URL + "?query=id:" + document.getId())
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(document.getId().intValue()))
            .jsonPath("$.[*].libelle")
            .value(hasItem(DEFAULT_LIBELLE))
            .jsonPath("$.[*].type")
            .value(hasItem(DEFAULT_TYPE));
    }
}
