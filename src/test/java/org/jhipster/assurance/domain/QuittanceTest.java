package org.jhipster.assurance.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.jhipster.assurance.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class QuittanceTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Quittance.class);
        Quittance quittance1 = new Quittance();
        quittance1.setId(1L);
        Quittance quittance2 = new Quittance();
        quittance2.setId(quittance1.getId());
        assertThat(quittance1).isEqualTo(quittance2);
        quittance2.setId(2L);
        assertThat(quittance1).isNotEqualTo(quittance2);
        quittance1.setId(null);
        assertThat(quittance1).isNotEqualTo(quittance2);
    }
}
