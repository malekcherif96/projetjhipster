package org.jhipster.assurance.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.jhipster.assurance.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class SinistreTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Sinistre.class);
        Sinistre sinistre1 = new Sinistre();
        sinistre1.setId(1L);
        Sinistre sinistre2 = new Sinistre();
        sinistre2.setId(sinistre1.getId());
        assertThat(sinistre1).isEqualTo(sinistre2);
        sinistre2.setId(2L);
        assertThat(sinistre1).isNotEqualTo(sinistre2);
        sinistre1.setId(null);
        assertThat(sinistre1).isNotEqualTo(sinistre2);
    }
}
