package org.jhipster.assurance.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.jhipster.assurance.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class VictimeTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Victime.class);
        Victime victime1 = new Victime();
        victime1.setId(1L);
        Victime victime2 = new Victime();
        victime2.setId(victime1.getId());
        assertThat(victime1).isEqualTo(victime2);
        victime2.setId(2L);
        assertThat(victime1).isNotEqualTo(victime2);
        victime1.setId(null);
        assertThat(victime1).isNotEqualTo(victime2);
    }
}
