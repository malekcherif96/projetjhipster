package org.jhipster.assurance.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.jhipster.assurance.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class IndemnisationTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Indemnisation.class);
        Indemnisation indemnisation1 = new Indemnisation();
        indemnisation1.setId(1L);
        Indemnisation indemnisation2 = new Indemnisation();
        indemnisation2.setId(indemnisation1.getId());
        assertThat(indemnisation1).isEqualTo(indemnisation2);
        indemnisation2.setId(2L);
        assertThat(indemnisation1).isNotEqualTo(indemnisation2);
        indemnisation1.setId(null);
        assertThat(indemnisation1).isNotEqualTo(indemnisation2);
    }
}
