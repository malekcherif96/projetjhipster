package org.jhipster.assurance.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.jhipster.assurance.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ChequeTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Cheque.class);
        Cheque cheque1 = new Cheque();
        cheque1.setId(1L);
        Cheque cheque2 = new Cheque();
        cheque2.setId(cheque1.getId());
        assertThat(cheque1).isEqualTo(cheque2);
        cheque2.setId(2L);
        assertThat(cheque1).isNotEqualTo(cheque2);
        cheque1.setId(null);
        assertThat(cheque1).isNotEqualTo(cheque2);
    }
}
