package org.jhipster.assurance.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link ChequeSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class ChequeSearchRepositoryMockConfiguration {

    @MockBean
    private ChequeSearchRepository mockChequeSearchRepository;
}
