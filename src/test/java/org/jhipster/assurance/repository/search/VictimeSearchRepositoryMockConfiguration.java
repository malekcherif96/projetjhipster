package org.jhipster.assurance.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link VictimeSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class VictimeSearchRepositoryMockConfiguration {

    @MockBean
    private VictimeSearchRepository mockVictimeSearchRepository;
}
