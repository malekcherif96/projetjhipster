package org.jhipster.assurance.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link IndemnisationSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class IndemnisationSearchRepositoryMockConfiguration {

    @MockBean
    private IndemnisationSearchRepository mockIndemnisationSearchRepository;
}
