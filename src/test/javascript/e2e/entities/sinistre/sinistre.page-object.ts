import { element, by, ElementFinder } from 'protractor';

export class SinistreComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-sinistre div table .btn-danger'));
  title = element.all(by.css('jhi-sinistre div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class SinistreUpdatePage {
  pageTitle = element(by.id('jhi-sinistre-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  idInput = element(by.id('field_id'));
  typeSinistreInput = element(by.id('field_typeSinistre'));
  dateSinistreInput = element(by.id('field_dateSinistre'));
  lieuSinistreInput = element(by.id('field_lieuSinistre'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setIdInput(id: string): Promise<void> {
    await this.idInput.sendKeys(id);
  }

  async getIdInput(): Promise<string> {
    return await this.idInput.getAttribute('value');
  }

  async setTypeSinistreInput(typeSinistre: string): Promise<void> {
    await this.typeSinistreInput.sendKeys(typeSinistre);
  }

  async getTypeSinistreInput(): Promise<string> {
    return await this.typeSinistreInput.getAttribute('value');
  }

  async setDateSinistreInput(dateSinistre: string): Promise<void> {
    await this.dateSinistreInput.sendKeys(dateSinistre);
  }

  async getDateSinistreInput(): Promise<string> {
    return await this.dateSinistreInput.getAttribute('value');
  }

  async setLieuSinistreInput(lieuSinistre: string): Promise<void> {
    await this.lieuSinistreInput.sendKeys(lieuSinistre);
  }

  async getLieuSinistreInput(): Promise<string> {
    return await this.lieuSinistreInput.getAttribute('value');
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class SinistreDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-sinistre-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-sinistre'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
