import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { SinistreComponentsPage, SinistreDeleteDialog, SinistreUpdatePage } from './sinistre.page-object';

const expect = chai.expect;

describe('Sinistre e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let sinistreComponentsPage: SinistreComponentsPage;
  let sinistreUpdatePage: SinistreUpdatePage;
  let sinistreDeleteDialog: SinistreDeleteDialog;
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing(username, password);
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Sinistres', async () => {
    await navBarPage.goToEntity('sinistre');
    sinistreComponentsPage = new SinistreComponentsPage();
    await browser.wait(ec.visibilityOf(sinistreComponentsPage.title), 5000);
    expect(await sinistreComponentsPage.getTitle()).to.eq('assuranceApp.sinistre.home.title');
    await browser.wait(ec.or(ec.visibilityOf(sinistreComponentsPage.entities), ec.visibilityOf(sinistreComponentsPage.noResult)), 1000);
  });

  it('should load create Sinistre page', async () => {
    await sinistreComponentsPage.clickOnCreateButton();
    sinistreUpdatePage = new SinistreUpdatePage();
    expect(await sinistreUpdatePage.getPageTitle()).to.eq('assuranceApp.sinistre.home.createOrEditLabel');
    await sinistreUpdatePage.cancel();
  });

  it('should create and save Sinistres', async () => {
    const nbButtonsBeforeCreate = await sinistreComponentsPage.countDeleteButtons();

    await sinistreComponentsPage.clickOnCreateButton();

    await promise.all([
      sinistreUpdatePage.setTypeSinistreInput('typeSinistre'),
      sinistreUpdatePage.setDateSinistreInput('2000-12-31'),
      sinistreUpdatePage.setLieuSinistreInput('lieuSinistre'),
    ]);

    await sinistreUpdatePage.save();
    expect(await sinistreUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await sinistreComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Sinistre', async () => {
    const nbButtonsBeforeDelete = await sinistreComponentsPage.countDeleteButtons();
    await sinistreComponentsPage.clickOnLastDeleteButton();

    sinistreDeleteDialog = new SinistreDeleteDialog();
    expect(await sinistreDeleteDialog.getDialogTitle()).to.eq('assuranceApp.sinistre.delete.question');
    await sinistreDeleteDialog.clickOnConfirmButton();
    await browser.wait(ec.visibilityOf(sinistreComponentsPage.title), 5000);

    expect(await sinistreComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
