import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ChequeComponentsPage, ChequeDeleteDialog, ChequeUpdatePage } from './cheque.page-object';

const expect = chai.expect;

describe('Cheque e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let chequeComponentsPage: ChequeComponentsPage;
  let chequeUpdatePage: ChequeUpdatePage;
  let chequeDeleteDialog: ChequeDeleteDialog;
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing(username, password);
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Cheques', async () => {
    await navBarPage.goToEntity('cheque');
    chequeComponentsPage = new ChequeComponentsPage();
    await browser.wait(ec.visibilityOf(chequeComponentsPage.title), 5000);
    expect(await chequeComponentsPage.getTitle()).to.eq('assuranceApp.cheque.home.title');
    await browser.wait(ec.or(ec.visibilityOf(chequeComponentsPage.entities), ec.visibilityOf(chequeComponentsPage.noResult)), 1000);
  });

  it('should load create Cheque page', async () => {
    await chequeComponentsPage.clickOnCreateButton();
    chequeUpdatePage = new ChequeUpdatePage();
    expect(await chequeUpdatePage.getPageTitle()).to.eq('assuranceApp.cheque.home.createOrEditLabel');
    await chequeUpdatePage.cancel();
  });

  it('should create and save Cheques', async () => {
    const nbButtonsBeforeCreate = await chequeComponentsPage.countDeleteButtons();

    await chequeComponentsPage.clickOnCreateButton();

    await promise.all([chequeUpdatePage.setNomChequeInput('nomCheque'), chequeUpdatePage.setMontnantInput('5')]);

    await chequeUpdatePage.save();
    expect(await chequeUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await chequeComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Cheque', async () => {
    const nbButtonsBeforeDelete = await chequeComponentsPage.countDeleteButtons();
    await chequeComponentsPage.clickOnLastDeleteButton();

    chequeDeleteDialog = new ChequeDeleteDialog();
    expect(await chequeDeleteDialog.getDialogTitle()).to.eq('assuranceApp.cheque.delete.question');
    await chequeDeleteDialog.clickOnConfirmButton();
    await browser.wait(ec.visibilityOf(chequeComponentsPage.title), 5000);

    expect(await chequeComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
