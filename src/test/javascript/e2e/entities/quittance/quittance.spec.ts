import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { QuittanceComponentsPage, QuittanceDeleteDialog, QuittanceUpdatePage } from './quittance.page-object';

const expect = chai.expect;

describe('Quittance e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let quittanceComponentsPage: QuittanceComponentsPage;
  let quittanceUpdatePage: QuittanceUpdatePage;
  let quittanceDeleteDialog: QuittanceDeleteDialog;
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing(username, password);
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Quittances', async () => {
    await navBarPage.goToEntity('quittance');
    quittanceComponentsPage = new QuittanceComponentsPage();
    await browser.wait(ec.visibilityOf(quittanceComponentsPage.title), 5000);
    expect(await quittanceComponentsPage.getTitle()).to.eq('assuranceApp.quittance.home.title');
    await browser.wait(ec.or(ec.visibilityOf(quittanceComponentsPage.entities), ec.visibilityOf(quittanceComponentsPage.noResult)), 1000);
  });

  it('should load create Quittance page', async () => {
    await quittanceComponentsPage.clickOnCreateButton();
    quittanceUpdatePage = new QuittanceUpdatePage();
    expect(await quittanceUpdatePage.getPageTitle()).to.eq('assuranceApp.quittance.home.createOrEditLabel');
    await quittanceUpdatePage.cancel();
  });

  it('should create and save Quittances', async () => {
    const nbButtonsBeforeCreate = await quittanceComponentsPage.countDeleteButtons();

    await quittanceComponentsPage.clickOnCreateButton();

    await promise.all([quittanceUpdatePage.setNumQuittanceInput('numQuittance')]);

    await quittanceUpdatePage.save();
    expect(await quittanceUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await quittanceComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Quittance', async () => {
    const nbButtonsBeforeDelete = await quittanceComponentsPage.countDeleteButtons();
    await quittanceComponentsPage.clickOnLastDeleteButton();

    quittanceDeleteDialog = new QuittanceDeleteDialog();
    expect(await quittanceDeleteDialog.getDialogTitle()).to.eq('assuranceApp.quittance.delete.question');
    await quittanceDeleteDialog.clickOnConfirmButton();
    await browser.wait(ec.visibilityOf(quittanceComponentsPage.title), 5000);

    expect(await quittanceComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
