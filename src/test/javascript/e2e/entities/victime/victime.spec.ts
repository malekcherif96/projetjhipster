import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { VictimeComponentsPage, VictimeDeleteDialog, VictimeUpdatePage } from './victime.page-object';

const expect = chai.expect;

describe('Victime e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let victimeComponentsPage: VictimeComponentsPage;
  let victimeUpdatePage: VictimeUpdatePage;
  let victimeDeleteDialog: VictimeDeleteDialog;
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing(username, password);
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Victimes', async () => {
    await navBarPage.goToEntity('victime');
    victimeComponentsPage = new VictimeComponentsPage();
    await browser.wait(ec.visibilityOf(victimeComponentsPage.title), 5000);
    expect(await victimeComponentsPage.getTitle()).to.eq('assuranceApp.victime.home.title');
    await browser.wait(ec.or(ec.visibilityOf(victimeComponentsPage.entities), ec.visibilityOf(victimeComponentsPage.noResult)), 1000);
  });

  it('should load create Victime page', async () => {
    await victimeComponentsPage.clickOnCreateButton();
    victimeUpdatePage = new VictimeUpdatePage();
    expect(await victimeUpdatePage.getPageTitle()).to.eq('assuranceApp.victime.home.createOrEditLabel');
    await victimeUpdatePage.cancel();
  });

  it('should create and save Victimes', async () => {
    const nbButtonsBeforeCreate = await victimeComponentsPage.countDeleteButtons();

    await victimeComponentsPage.clickOnCreateButton();

    await promise.all([
      victimeUpdatePage.setNumContratInput('numContrat'),
      victimeUpdatePage.setCinInput('5'),
      victimeUpdatePage.setSituationSocialInput('situationSocial'),
      victimeUpdatePage.setNumeroTelephoneInput('5'),
      victimeUpdatePage.setNomInput('nom'),
      victimeUpdatePage.setPrenomInput('prenom'),
      victimeUpdatePage.setLoginInput('login'),
      victimeUpdatePage.setPasswordInput('password'),
    ]);

    await victimeUpdatePage.save();
    expect(await victimeUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await victimeComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Victime', async () => {
    const nbButtonsBeforeDelete = await victimeComponentsPage.countDeleteButtons();
    await victimeComponentsPage.clickOnLastDeleteButton();

    victimeDeleteDialog = new VictimeDeleteDialog();
    expect(await victimeDeleteDialog.getDialogTitle()).to.eq('assuranceApp.victime.delete.question');
    await victimeDeleteDialog.clickOnConfirmButton();
    await browser.wait(ec.visibilityOf(victimeComponentsPage.title), 5000);

    expect(await victimeComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
