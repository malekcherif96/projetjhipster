import { element, by, ElementFinder } from 'protractor';

export class VictimeComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-victime div table .btn-danger'));
  title = element.all(by.css('jhi-victime div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class VictimeUpdatePage {
  pageTitle = element(by.id('jhi-victime-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  idInput = element(by.id('field_id'));
  numContratInput = element(by.id('field_numContrat'));
  cinInput = element(by.id('field_cin'));
  situationSocialInput = element(by.id('field_situationSocial'));
  numeroTelephoneInput = element(by.id('field_numeroTelephone'));
  nomInput = element(by.id('field_nom'));
  prenomInput = element(by.id('field_prenom'));
  loginInput = element(by.id('field_login'));
  passwordInput = element(by.id('field_password'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setIdInput(id: string): Promise<void> {
    await this.idInput.sendKeys(id);
  }

  async getIdInput(): Promise<string> {
    return await this.idInput.getAttribute('value');
  }

  async setNumContratInput(numContrat: string): Promise<void> {
    await this.numContratInput.sendKeys(numContrat);
  }

  async getNumContratInput(): Promise<string> {
    return await this.numContratInput.getAttribute('value');
  }

  async setCinInput(cin: string): Promise<void> {
    await this.cinInput.sendKeys(cin);
  }

  async getCinInput(): Promise<string> {
    return await this.cinInput.getAttribute('value');
  }

  async setSituationSocialInput(situationSocial: string): Promise<void> {
    await this.situationSocialInput.sendKeys(situationSocial);
  }

  async getSituationSocialInput(): Promise<string> {
    return await this.situationSocialInput.getAttribute('value');
  }

  async setNumeroTelephoneInput(numeroTelephone: string): Promise<void> {
    await this.numeroTelephoneInput.sendKeys(numeroTelephone);
  }

  async getNumeroTelephoneInput(): Promise<string> {
    return await this.numeroTelephoneInput.getAttribute('value');
  }

  async setNomInput(nom: string): Promise<void> {
    await this.nomInput.sendKeys(nom);
  }

  async getNomInput(): Promise<string> {
    return await this.nomInput.getAttribute('value');
  }

  async setPrenomInput(prenom: string): Promise<void> {
    await this.prenomInput.sendKeys(prenom);
  }

  async getPrenomInput(): Promise<string> {
    return await this.prenomInput.getAttribute('value');
  }

  async setLoginInput(login: string): Promise<void> {
    await this.loginInput.sendKeys(login);
  }

  async getLoginInput(): Promise<string> {
    return await this.loginInput.getAttribute('value');
  }

  async setPasswordInput(password: string): Promise<void> {
    await this.passwordInput.sendKeys(password);
  }

  async getPasswordInput(): Promise<string> {
    return await this.passwordInput.getAttribute('value');
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class VictimeDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-victime-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-victime'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
