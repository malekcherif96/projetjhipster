import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { IndemnisationComponentsPage, IndemnisationDeleteDialog, IndemnisationUpdatePage } from './indemnisation.page-object';

const expect = chai.expect;

describe('Indemnisation e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let indemnisationComponentsPage: IndemnisationComponentsPage;
  let indemnisationUpdatePage: IndemnisationUpdatePage;
  let indemnisationDeleteDialog: IndemnisationDeleteDialog;
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing(username, password);
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Indemnisations', async () => {
    await navBarPage.goToEntity('indemnisation');
    indemnisationComponentsPage = new IndemnisationComponentsPage();
    await browser.wait(ec.visibilityOf(indemnisationComponentsPage.title), 5000);
    expect(await indemnisationComponentsPage.getTitle()).to.eq('assuranceApp.indemnisation.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(indemnisationComponentsPage.entities), ec.visibilityOf(indemnisationComponentsPage.noResult)),
      1000
    );
  });

  it('should load create Indemnisation page', async () => {
    await indemnisationComponentsPage.clickOnCreateButton();
    indemnisationUpdatePage = new IndemnisationUpdatePage();
    expect(await indemnisationUpdatePage.getPageTitle()).to.eq('assuranceApp.indemnisation.home.createOrEditLabel');
    await indemnisationUpdatePage.cancel();
  });

  it('should create and save Indemnisations', async () => {
    const nbButtonsBeforeCreate = await indemnisationComponentsPage.countDeleteButtons();

    await indemnisationComponentsPage.clickOnCreateButton();

    await promise.all([indemnisationUpdatePage.setDateInput('2000-12-31')]);

    await indemnisationUpdatePage.save();
    expect(await indemnisationUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await indemnisationComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Indemnisation', async () => {
    const nbButtonsBeforeDelete = await indemnisationComponentsPage.countDeleteButtons();
    await indemnisationComponentsPage.clickOnLastDeleteButton();

    indemnisationDeleteDialog = new IndemnisationDeleteDialog();
    expect(await indemnisationDeleteDialog.getDialogTitle()).to.eq('assuranceApp.indemnisation.delete.question');
    await indemnisationDeleteDialog.clickOnConfirmButton();
    await browser.wait(ec.visibilityOf(indemnisationComponentsPage.title), 5000);

    expect(await indemnisationComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
